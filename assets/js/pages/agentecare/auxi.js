$(function () {
	//=======================//
	//show datatable presensi aux//
		jQuery.ajax({
			url: base_url+'cs_m_aux/get_m_aux',
			type: 'POST',
			data: {id_presensi : $('#id_presensi').val()},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				var status_aux = $('#status_aux').val();
				var id_m_aux = $('#id_m_aux').val();
				var id_presensi = $('#id_presensi').val();
				var aux_start = new Date($('#aux_start').val());
				var diff = parseInt($('#diff').val());
				var  detik = diff%60;
				var  menit = parseInt((diff%3600)/60);
				var  jam = parseInt(diff/3600);

				var str = '';
				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					str+= '<tr height="40">';
					str+= '<td>'+data['data'][i]['jenis_aux']+'</td>';
					var max_aux = '';
					if(data['data'][i]['max_aux'] == null ){
						str+= '<td class="text-center"> - </td>';
					}else{
						max_aux = data['data'][i]['max_aux'];
						str+= '<td class="text-center">'+data['data'][i]['max_aux']+'</td>';
					}
					if(status_aux == '1' && id_m_aux == data['data'][i]['id']){
						str+= '<td class="text-center">'+addZero(aux_start.getHours())+':'+addZero(aux_start.getMinutes())+':'+addZero(aux_start.getSeconds())+'</td>';
						str+= '<td class="text-center">';
							str += '<span class="waktu_aux" data-jam="'+addZero(jam)+'" data-menit="'+addZero(menit)+'" data-detik="'+addZero(detik)+'" data-max="'+max_aux+'">';
								str+= addZero(jam)+':'+addZero(menit)+':'+addZero(detik)+'</span>';
						str+= '</td>';
						start_time_aux()
					}
					else {
						str+= '<td></td>';
						str+= '<td></td>';
					}

					if(status_aux == 1 && id_presensi !== ''){

						var action='';
						if(id_m_aux == data['data'][i]['id']){
							action = '<div class="switch"><label>OFF<input type="checkbox" data-idaux="'+data['data'][i]['id']+'" id="sw'+data['data'][i]['id']+'" class="switch_aux_end" checked><span class="lever switch-col-red"></span>ON</label></div>&nbsp;';
						}
						str+= '<td class="text-center">'+action+'</td>';
						str+= '</tr>';

					}else if(status_aux == 0 && id_presensi !== ''){
						var action='';
						if(data['data'][i]['status_on'] != '1'){
							action = '<div class="switch"><label>OFF<input type="checkbox" data-idaux="'+data['data'][i]['id']+'" id="sw'+data['data'][i]['id']+'" class="switch_aux_start"><span class="lever switch-col-red"></span>ON</label></div>&nbsp;';							
						}
						str+= '<td class="text-center">'+action+'</td>';
						str+= '</tr>';
					}
					else{

						str+= '<td class="text-center"></td>';
						str+= '</tr>';
					}
				}
				
				$('#tbodyy').html('');
				$('#tbodyy').append(str);
				 
				switch_aux();

				// $('#tbody').html(str);
				// table = $('#data_agent').DataTable({
				// 	"order": [],
				// 	// "bDestroy": true
				// });

			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

	//Get
	//var bla = $('#csdm').val();

	//Set
	//$('#name').val(bla);

	function switch_aux(){
		$('.switch_aux_start').change(function(){
			//console.log($(this).data("idaux"));
			jQuery.ajax({
				url: base_url+'agentecare/do_aux_start',
				type: 'POST',
				data: {id_presensi:$('#id_presensi').val(), id_m_aux:$(this).data("idaux") },
	            dataType : 'json',
				success: function(data, textStatus, xhr) {
					console.log(data['hasil']);
					if (data['hasil'] == 'success') {
						window.location = base_url+data['url'];
					}
					else{ alert(data['hasil']); }
				},
				error: function(xhr, textStatus, errorThrown) {
					console.log(textStatus.reponseText);
				}
			});
		});

		$('.switch_aux_end').change(function(){
			//console.log($(this).data("idaux"));
			jQuery.ajax({
			url: base_url+'agentecare/do_aux_end',
			type: 'POST',
			data: {id_presensi:$('#id_presensi').val(), id_presensi_aux:$('#id_presensi_aux').val(), aux_start:$('#aux_start').val() },
            dataType : 'json',
			success: function(data, textStatus, xhr) {
				console.log(data['hasil']);
				if (data['hasil'] == 'success') {
					window.location = base_url+data['url'];
				}
				else{ alert(data['hasil']); }
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
		});
	}

	//=======================//
	//Create select aux//
		// jQuery.ajax({
		// 	url: base_url+'cs_m_aux/get_m_aux',
		// 	type: 'POST',
		// 	data: {},
		// 	dataType : 'json',
		// 	beforeSend: function(){
		// 			var o = '<option value="0" selected>-- Choose AUX --</option>';
		// 			o += '<option value="search" disabled>Search.. </option>';
		// 			$("#jenis_aux").html(o).selectpicker('refresh');
		// 		},
		// 	success: function(data, textStatus, xhr) {
		// 		$("#jenis_aux option[value='search']").remove();
		// 		for (var i = 0; i < Object.keys(data['data']).length; i++) {

		// 			var o = '<option value="'+data['data'][i]['id']+'" >'+data['data'][i]['jenis_aux']+'</option>';
		// 			$("#jenis_aux").append(o).selectpicker('refresh');
		// 		}
		// 	},
		// 	error: function(xhr, textStatus, errorThrown) {
		// 		console.log(textStatus.reponseText);
		// 	}
		// });
	//=======================//

	//=======================//
	//show datatable history presensi aux//
		jQuery.ajax({
			url: base_url+'cs_presensi_aux/get_presensi_aux',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				var str = '';
				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					str+= '<tr height="40">';
					str+= '<td class="text-center">'+data['data'][i]['aux_start']+'</td>';
					if(data['data'][i]['aux_end'] == null ){
						str+= '<td class="text-center"> - </td>';
					}else{
						str+= '<td class="text-center">'+data['data'][i]['aux_end']+'</td>';
					}
					if(data['data'][i]['duration'] == null ){
						str+= '<td class="text-center"> - </td>';
					}else{
						str+= '<td class="text-center">'+data['data'][i]['duration']+'</td>';
					}
					str+= '<td class="text-center">'+data['data'][i]['jenis_aux']+'</td>';
					str+= '</tr>';
				}

				$('#tbody').html(str);
				table = $('#data_history_aux').DataTable({
					"order": [],
					// "bDestroy": true
				});

			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//
	function start_time_aux(){

		setInterval(function(){
			$('.waktu_aux').each(function(index){
				var jam = parseInt($(this).data('jam'));
				var menit = parseInt($(this).data('menit'));
				var detik = parseInt($(this).data('detik'));

				detik += 1;
				$(this).data('detik', detik);
				if(detik > 60){
					detik = 0;
					menit += 1;
					$(this).data('detik', '0');
					$(this).data('menit', menit);
					if(menit > 60 ){
						menit = 0;
						jam += 1;
						$(this).data('menit', '0');
						$(this).data('jam', jam);
					}
				}

				var text = '';
				if(jam < 10 ){
					text += "0"+jam;
				}else{text += jam;}
				if(menit < 10 ){
					text += ":0"+menit;
				}else{text += ":"+menit;}
				if(detik < 10 ){
					text += ":0"+detik;
				}else{text += ":"+detik;}

				$(this).html(text);
			});
		}, 1000);
	}
});

function addZero(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}