$(function () {
	var table =  $('#table-jadwal_report').DataTable({
		"order": []
	});

	//=======================//
	//Create select unit//
		jQuery.ajax({
			url: base_url+'cs_unit/get_unit',
			type: 'POST',
			data: {},
			dataType : 'json',
			beforeSend: function(){
					var o = '<option value="0" selected>-- All --</option>';
					o += '<option value="search" disabled>Search.. </option>';
					$("#unit").html(o).selectpicker('refresh');
				},
			success: function(data, textStatus, xhr) {
				$("#unit option[value='search']").remove();
				for (var i = 0; i < Object.keys(data['data']).length; i++) {

					var o = '<option value="'+data['data'][i]['name']+'" >'+data['data'][i]['name']+'</option>';
					$("#unit").append(o).selectpicker('refresh');
				}
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

	//=======================//
	//Get view sdm
	$('#form-filter_report').click(function(){
		var unit = $('#unit').val();
		var status_validate = 1;

		// if(unit == null){
		// 	status_validate = 0;
		// 	$('#unit').closest('.form-line').addClass('error');
		// }

		if(status_validate == 1 ){
			
			jQuery.ajax({
				url: base_url+'cs_sdm/get_all_sdm',
				type: 'POST',
				data: {unit:unit},
				dataType : 'json',
				success: function(data, textStatus, xhr) {
					$('#unit').closest('.form-line').removeClass('error');

					var str = '';
					for (var i = 0; i < Object.keys(data['data']).length; i++) {
						str+= '<tr>';
						str+= '<td class="text-center" nowrap>'+data['data'][i]['name']+'</td>';
						str+= '<td class="text-left" nowrap>'+data['data'][i]['csdm']+'</td>';
						str+= '<td class="text-center" nowrap>'+data['data'][i]['nik_hris']+'</td>';
						str+= '<td class="text-center" nowrap>'+data['data'][i]['gender']+'</td>';
						str+= '<td class="text-center" nowrap>'+data['data'][i]['perner']+'</td>';
						str+= '<td class="text-center">'+data['data'][i]['jabatan']+'</td>';
						str+= '<td class="text-center">'+data['data'][i]['jabatan_level']+'</td>';
						str+= '<td class="text-center" nowrap>'+data['data'][i]['unit']+'</td>';
						str+= '<td class="text-center" nowrap>'+data['data'][i]['tl']+'</td>';
						str+= '<td class="text-center" nowrap>'+data['data'][i]['spv']+'</td>';
						str+= '<td class="text-center" nowrap><a href="'+data['data'][i]['photo_out']+'" target="_blank"><img src="../../../fania/images/no-photo.gif"" width="100px"></a></td>';
						str+= '</tr>';
					}

					table.destroy();
					$('#tbody_report').html(str);
					table = $('#table-jadwal_report').DataTable({
						"order": [],
						// "bDestroy": true
					});
				
				},
				error: function(xhr, textStatus, errorThrown) {
					console.log(textStatus.reponseText);
				}
			});
		}
	});
	//=======================//

	//=======================//
	//export excel
		// $('#export_excel').click(function(){
		// 	var date_start = $('#date_start').val(); 
		// 	var date_end = $('#date_end').val(); 
		// 	var unit = $('#unit').val();
		// 	var name = $('#name').val();
		// 	var status_validate = 1;

		// 	if(date_start == ''){
		// 		status_validate = 0;
		// 		$('#date_start').closest('.form-line').addClass('focused error');
		// 	}
		// 	else { $('#date_start').closest('.form-line').removeClass('error'); }

		// 	if(date_end == ''){
		// 		status_validate = 0;
		// 		$('#date_end').closest('.form-line').addClass('focused error');
		// 	}
		// 	else { $('#date_end').closest('.form-line').removeClass('error'); }

		// 	if(unit == null){
		// 		status_validate = 0;
		// 		$('#unit').closest('.form-line').addClass('error');
		// 	}
		// 	else { $('#unit').closest('.form-line').removeClass('error'); }

		// 	if(status_validate === 1){
		// 		if(name == '0'){
		// 			var url = base_url+'ca_presensi/get_xls_presensi/'+date_start+'/'+date_end+'/'+unit;
		// 		}else{
		// 			var url = base_url+'ca_presensi/get_xls_presensi_personal/'+date_start+'/'+date_end+'/'+unit+'/'+name;
		// 		}
				
		// 		window.open(url);
		// 	}
		// });
	//=======================//
})