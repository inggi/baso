$(function () {
	var table =  $('#table-jadwal_report').DataTable({
		"order": []
	});

	//=======================//
	//Create select spv unit//
		jQuery.ajax({
			url: base_url+'cs_sdm/get_unit_spv',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				$("#spv option[value='search']").remove();
				for (var i = 0; i < Object.keys(data['data']).length; i++) {

					var o = '<option value="'+data['data'][i]['id']+'" data-subtext="'+data['data'][i]['name']+'">'+data['data'][i]['jabatan']+'</option>';
					$("#spv").append(o).selectpicker('refresh');
				}
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

	//=======================//
	//Create select nama agent//
	/*
		$("#spv").change(function(){
			jQuery.ajax({
				url: base_url+'cs_sdm/get_underspv',
				type: 'POST',
				data: {id_spv: $("#spv").val() },
				dataType : 'json',
				beforeSend: function(){
					var o = '<option value="0" selected>-- All --</option>';
					o += '<option value="search" disabled>Search.. </option>';
					$("#unit").html(o).selectpicker('refresh');
				},
				success: function(data, textStatus, xhr) {
					$("#unit option[value='search']").remove();
					for (var i = 0; i < Object.keys(data['data']).length; i++) {

						var o = '<option value="'+data['data'][i]['id']+'" data-subtext="'+data['data'][i]['csdm']+'">'+data['data'][i]['name']+'</option>';
						$("#unit").append(o).selectpicker('refresh');
					}
				},
				error: function(xhr, textStatus, errorThrown) {
					console.log(textStatus.reponseText);
				}
			});
		});
		*/
	//=======================//

	//=======================//
	//Setting datepicker
	$('#bs_datepicker_range_container').datepicker({
		autoclose: true,
		container: '#bs_datepicker_range_container'
	});
	//=======================//

	//=======================//
	//Get info keterlambatan
	$('#form-filter_report').click(function(){
		var date_start = $('#date_start').val(); 
		var date_end = $('#date_end').val(); 
		var id_spv = $('#spv').val(); 
		var unit = $('#unit').val();
		var status_validate = 1;

		if(date_start == ''){
			status_validate = 0;
			$('#date_start').closest('.form-line').addClass('focused error');
		}

		if(date_end == ''){
			status_validate = 0;
			$('#date_end').closest('.form-line').addClass('focused error');
		}

		if(id_spv == null){
			status_validate = 0;
			$('#spv').closest('.form-line').addClass('error');
		}

		if(status_validate == 1 ){
			jQuery.ajax({
				url: base_url+'ca_presensi/get_keterlambatan',
				type: 'POST',
				data: { date_start:date_start, date_end:date_end, id_spv:id_spv, unit:unit},
				dataType : 'json',
				success: function(data, textStatus, xhr) {
					$('#date_start').closest('.form-line').removeClass('error');
					$('#date_end').closest('.form-line').removeClass('error');
					$('#spv').closest('.form-line').removeClass('error');

					var str = '';
					for (var i = 0; i < Object.keys(data['data']).length; i++) {
						str+= '<tr>';
						str+= '<td>'+data['data'][i]['csdm']+'</td>';
						str+= '<td nowrap>'+data['data'][i]['name']+'</td>';
						str+= '<td>'+data['data'][i]['jabatan']+'</td>';
						str+= '<td class="text-right">'+data['data'][i]['hari_kerja']+'</td>';
						str+= '<td class="text-right">'+data['data'][i]['hari_terlambat']+'</td>';
						str+= '<td class="text-right">'+data['data'][i]['lama_terlambat']+'</td>';
						str+= '<td class="text-right">'+data['data'][i]['presentasi'].toFixed(2)+' %</td>';
						str+= '</tr>';
					}

					table.destroy();
					$('#tbody_report').html(str);
					table = $('#table-jadwal_report').DataTable({
						"order": [],
						// "bDestroy": true
					});
				
				},
				error: function(xhr, textStatus, errorThrown) {
					console.log(textStatus.reponseText);
				}
			});
		}
	});
	//=======================//
})