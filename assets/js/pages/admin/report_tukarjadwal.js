$(function () {
	var d = new Date();
	var table = $('#table-jadwal_report').DataTable({
        responsive: true,
    });

	//=======================//
	//Create select unit unit//
		jQuery.ajax({
			url: base_url+'cs_unit/get_unit',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				$("#unit option[value='search']").remove();
				for (var i = 0; i < Object.keys(data['data']).length; i++) {

					var o = '<option value="'+data['data'][i]['name']+'" >'+data['data'][i]['name']+'</option>';
					$("#unit").append(o).selectpicker('refresh');
				}
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

	//=======================//
	//Setting datepicker
	$('#bs_datepicker_range_container').datepicker({
		autoclose: true,
		container: '#bs_datepicker_range_container'
	});
	//=======================//
	
	//=======================//
	//export excel
		$('#export_excel').click(function(){
			var date_start = $('#date_start').val(); 
			var date_end = $('#date_end').val(); 
			var unit = $('#unit').val();
			var status_validate = 1;

			if(date_start == ''){
				status_validate = 0;
				$('#date_start').closest('.form-line').addClass('focused error');
			}
			else { $('#date_start').closest('.form-line').removeClass('error'); }

			if(date_end == ''){
				status_validate = 0;
				$('#date_end').closest('.form-line').addClass('focused error');
			}
			else { $('#date_end').closest('.form-line').removeClass('error'); }

			if(unit == null){
				status_validate = 0;
				$('#unit').closest('.form-line').addClass('error');
			}
			else { $('#unit').closest('.form-line').removeClass('error'); }

			if(status_validate === 1){	
				var url = base_url+'cs_history_tukarjadwal/get_xls_history_tukarjadwal_byadmin/'+date_start+'/'+date_end+'/'+unit;
				window.open(url);
			}
		});
	//=======================//

	//=======================//
	//Setting table jadwal agent

		$('#form-filter_report').click(function(){
			var date_start = $('#date_start').val(); 
			var date_end = $('#date_end').val(); 
			var unit = $('#unit').val();
			var status_validate = 1;

			if(date_start == ''){
				status_validate = 0;
				$('#date_start').closest('.form-line').addClass('focused error');
			}
			else { $('#date_start').closest('.form-line').removeClass('error'); }

			if(date_end == ''){
				status_validate = 0;
				$('#date_end').closest('.form-line').addClass('focused error');
			}
			else { $('#date_end').closest('.form-line').removeClass('error'); }

			if(unit == null){
				status_validate = 0;
				$('#unit').closest('.form-line').addClass('error');
			}
			else { $('#unit').closest('.form-line').removeClass('error'); }

			if(status_validate === 1){
				jQuery.ajax({
					url: base_url+'cs_history_tukarjadwal/get_history_tukarjadwal_byadmin',
					type: 'POST',
					data: {date_start: date_start, date_end: date_end, unit:unit},
					dataType : 'json',
					success: function(data, textStatus, xhr) {
						var str = '';
						for (var i = 0; i < Object.keys(data['data']).length; i++) {
							str += '<tr>';
							str += '<td >'+data['data'][i]['csdm']+'</td>';
							str += '<td >'+data['data'][i]['csdm_pengganti']+'</td>';
							str += '<td scope="row" nowrap>'+data['data'][i]['nama_peminta']+'</td>';
							str += '<td nowrap>'+data['data'][i]['nama_pengganti']+'</td>';
							str += '<td >'+data['data'][i]['tgl_awal']+'</td>';
							str += '<td >'+data['data'][i]['tgl_pengganti']+'</td>';
							str += '<td >'+data['data'][i]['pola_awal']+'</td>';
							str += '<td >'+data['data'][i]['pola_pengganti']+'</td>';
							str += '<td >'+data['data'][i]['type']+'</td>';
							str += '<td >'+data['data'][i]['alasan']+'</td>';
							str += '<td ><a target="_blank" rel="noopener noreferrer" href="'+base_url+'uploads/'+data['data'][i]['uploads']+'">Download</a></td>';

							str += '</tr>';
						}
						
						table.destroy();
						$('#tbody_report').html('');
						$('#tbody_report').append(str);
						table = $('#table-jadwal_report').DataTable({
							"order": [],
							// "bDestroy": true
						});
					},
					error: function(xhr, textStatus, errorThrown) {
						console.log(textStatus.reponseText);
					}
				});
			}
			
			// alert('makan');
		});
	//=======================//

})