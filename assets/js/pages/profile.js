$(function () {
	//=======================//
	//Import pola jadwal//
		$('#do_update_password').submit(function(e){
			e.preventDefault();
			if(typeof $('#password').val() != 'undefined' && $('#password').val() === $('#retype_password').val()){
				var formdata = new FormData();
				formdata.append('password', $('#password').val());

				$.ajax({
					url:base_url+'cs_sdm/do_update_password',
					type:"post",
					data:formdata,
					processData:false,
					contentType:false,
					cache:false,
					async:false,
					dataType : 'json',
					success: function(data){
						if (data['hasil'] == 'success') {
							alert('Password berhasil di update.');
							$('#password').val('');
							$('#retype_password').val('');
						}
						else{
							alert(data['hasil']);
						}
					}
				});
			}
			else{
				alert('Password tidak sama.');
			}
		});
	//=======================//
})