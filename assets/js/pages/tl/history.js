$(function () {

	//=======================//
	//Setting datepicker range
	$('#bs_datepicker_range_container').datepicker({
		autoclose: true,
		container: '#bs_datepicker_range_container'
	});
	//Setting datepicker
	// $('#bs_datepicker_container input').datepicker({
 //        autoclose: true,
 //        container: '#bs_datepicker_container'
 //    });
    //=======================//

	//=======================//
	//Hiden form filter and  upload
		$('#show_upload').click(function(){
			$('#form-upload').removeClass('hidden');
		});
		$('#close_upload').click(function(){
			$('#form-upload').addClass('hidden');
		});

		$('#show_filter').click(function(){
			$('#form-filter').removeClass('hidden');
		});
		$('#close_filter').click(function(){
			$('#form-filter').addClass('hidden');
		});
	//=======================//

	//=======================//
	//Create select tl/
		jQuery.ajax({
			url: base_url+'cs_sdm/get_tl',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					var o = '<option value="'+data['data'][i]['id']+'">'+data['data'][i]['name']+'</option>';
					$("#id_tl").append(o).selectpicker('refresh');
				}

				var temp = $("#temp_tl").val();
				$("#id_tl").val(temp).selectpicker('refresh');
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//
	//Filter karyawan//
	var table =  $('#table-pegawai').DataTable({
		"order": []
	});

		$('#form-filter_report').click(function(){
			// var date_start = $('#date_start').val(); 
			// var date_end = $('#date_end').val(); 
			var date_start0 = $('#date_start').val();
			let date_start1 = new Date(date_start0);
			let date_start = date_start1.getFullYear() + "-" + (date_start1.getMonth() + 1) + "-" + date_start1.getDate();
			var date_end0 = $('#date_end').val(); 
			let date_end1 = new Date(date_end0);
			let date_end = date_end1.getFullYear() + "-" + (date_end1.getMonth() + 1) + "-" + date_end1.getDate();
			var unit = $('#unit').val();
			var reason_update = $('#reason_update').val();
			var id_tl = $('#id_tl').val();
			var id_spv = $('#id_spv').val();
			var status_validate = 1;

			if(id_tl == null){
				status_validate = 0;
				$('#id_tl').closest('.form-line').addClass('error');
			}
			
			if(status_validate == 1 ){
				jQuery.ajax({
					url: base_url+'cs_history/get_history_usertl',
					type: 'POST',
					data: {date_start:date_start, date_end:date_end, reason_update, id_tl:id_tl},
					dataType : 'json',
					success: function(data, textStatus, xhr) {
						$('#unit').closest('.form-line').removeClass('error');

						var str = '';
						for (var i = 0; i < Object.keys(data['data']).length; i++) {
							str+= '<tr>';
							str+= '<td>'+data['data'][i]['csdm']+'</td>';
							str+= '<td nowrap>'+data['data'][i]['name']+'</td>';
							str+= '<td>'+data['data'][i]['name_tl']+'</td>';
							str+= '<td>'+data['data'][i]['reason_update']+'</td>';
							str+= '<td>'+data['data'][i]['createdby']+'</td>';
							str+= '<td>'+'<a href="'+base_url+'spv/detail_history/'+data['data'][i]['id']+'" class="btn btn-primary m-t-15 waves-effect">Detail</a>'+'</td>';
							str+= '</tr>';
						}

						table.destroy();
						$('#tbody').html(str);
						table = $('#table-pegawai').DataTable({
							"order": [],
							// "bDestroy": true
						});
					
					},
					error: function(xhr, textStatus, errorThrown) {
						console.log(textStatus.reponseText);
					}
				});
			}
		});
		$('#form-filter_report').trigger('click');
	//=======================//

	//=======================//
	//datatable ajax limit query//
	// var table_pegawai = $('#table-pegawai').DataTable({
	// 	"responsive": true,
	// 	"bLengthChange": false,
	// 	"bDestroy": true,
	// 	"bFilter": false,
	// 	"bProcessing": true,
	// 	"serverSide": true,
	// 	"columns": [
	// 		{ "data": "csdm" },
	// 		{ "data": "name" },
	// 		{ "data": "unit" },
	// 		{ "data": "reason_update" },
	// 		{ "data": "notes" },
	// 		{ "data": "createdby" },
	// 		{ 
	//             "data": "id",
	//             render : function(data, type, row, meta) { return "<button data-id_kip='"+data+"' class='edit btn btn-primary m-t-15 waves-effect'>Detail</button>"}
	//         },
	// 	],
	// 	"ajax":{
	// 		url :base_url+'cs_history/get_tb_history_update_sdm', // json datasource
	// 		type: "post",
	// 		data: function(d){
	// 			d.date_start = $('#date_start').val();
	// 			d.date_end = $('#date_end').val();
	// 			d.unit = $('#unit').val();
	// 			d.reason_update = $('#reason_update').val();
	// 		},
	// 		// success: function(data, textStatus, xhr) {
	// 		//  console.log(data);
	// 		// },
	// 		error: function(){  // error handling code
	// 			$('#table-element').css("display","none");
	// 		}
	// 	},
	// 	"pageLength": 50,
	// 	"order": [[ 0, "desc" ]]
	// });


	// $('#form-filter_report').click(function(){
 //        table_pegawai.ajax.reload();
 //    });
	//=======================//

	//=======================//
	//export excel
		$('#export').click(function(){
			var date_start0 = $('#date_start').val();
			let date_start1 = new Date(date_start0);
			let date_start = date_start1.getFullYear() + "-" + (date_start1.getMonth() + 1) + "-" + date_start1.getDate();
			var date_end0 = $('#date_end').val(); 
			let date_end1 = new Date(date_end0);
			let date_end = date_end1.getFullYear() + "-" + (date_end1.getMonth() + 1) + "-" + date_end1.getDate();
			
			var reason_update = $('#reason_update').val();
			
			if(typeof date_start == 'undefined' || date_start =='NaN-NaN-NaN'  || date_start =='' || date_start == null) { date_start = '-' }  
			if(typeof date_end == 'undefined' || date_end =='NaN-NaN-NaN' || date_start =='' || date_end == null) { date_end = '-' }  
			if(typeof unit == 'undefined' || unit =='' || unit == null) { unit = '-' }  
			if(typeof reason_update == 'undefined' || reason_update =='' || reason_update == null) { reason_update = '-' }  
			var url = base_url+'cs_history/get_xls/'+date_start+'/'+date_end+'/'+unit+'/'+reason_update;

			window.open(url);
		});
	//=======================//
})