$(function () {
	//=======================//
	//datatable//
		jQuery.ajax({
			url: base_url+'cs_sdm/get_sdm',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				var str = '';
				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					str+= '<tr height="25px">';
					str+= '<td>'+data['data'][i]['csdm']+'</td>';
					str+= '<td nowrap>'+data['data'][i]['name']+'</td>';
					str+= '<td>'+data['data'][i]['jabatan']+'</td>';
					str+= '<td>'+data['data'][i]['tgl_awal_kontrak']+'</td>';
					str+= '<td>'+data['data'][i]['tgl_akhir_kontrak']+'</td>';
					// str+= '<td>'+'<a href="'+base_url+'spv/view_sdm/'+data['data'][i]['id']+'" class="btn bg-cyan btn-xs"><i class="material-icons">search</i></a>'+'<button type="button" class="btn btn-primary btn-xs modalupload1" data-toggle="modal" data-target="#modalupload1" data-id_sdm="'+data['data'][i]['id']+'" data-name="'+data['data'][i]['name']+'" data-csdm="'+data['data'][i]['csdm']+'"><i class="material-icons">file_upload</i></button></td>';

					var action='<a href="'+base_url+'tl/view_sdm/'+data['data'][i]['id']+'" class="btn bg-cyan btn-xs"><i class="material-icons">search</i></a>';
					
					var action2='';
					if(data['data'][i]['status'] == null ){
						action2= '<button type="button" data-cek="button_upload" class="btn btn-primary btn-xs modalupload1" data-toggle="modal" data-target="#modalupload1" data-id_sdm="'+data['data'][i]['id']+'" data-name="'+data['data'][i]['name']+'" data-csdm="'+data['data'][i]['csdm']+'"><i class="material-icons">file_upload</i></button>';
					}else if(data['data'][i]['kategori_file'] == 'CONTRACT' ){
						action2= '';
					}else{
						action2= '<button type="button" data-cek="button_reupload" class="btn btn-success btn-xs modaledit1" data-toggle="modal" data-target="#modaledit1" data-id_sdm2="'+data['data'][i]['id']+'" data-name2="'+data['data'][i]['name']+'" data-csdm2="'+data['data'][i]['csdm']+'" data-notes="'+data['data'][i]['notes2']+'" data-namefile="'+data['data'][i]['namefile']+'" data-pathfile="'+data['data'][i]['pathfile']+'" data-namefile2="'+data['data'][i]['namefile2']+'" data-pathfile2="'+data['data'][i]['pathfile2']+'" data-namefile3="'+data['data'][i]['namefile3']+'" data-pathfile3="'+data['data'][i]['pathfile3']+'" data-id_doc="'+data['data'][i]['id_doc']+'" data-kategori_file="'+data['data'][i]['kategori_file']+'"><i class="material-icons">edit</i></button>';
					}
					// str+= '<td nowrap>'+action+'</td>';
					str+= '<td nowrap>'+action+''+action2+'</td>';
					str+= '</tr>';
				}

				$('#tbody').html(str);
				table = $('#table_pegawai').DataTable({
					"pageLength": 50,
					"order": [],
					// "bDestroy": true
				});
			
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

	//=======================//
	//value on modal//
	$('#table_pegawai tbody').on('click', 'button', function(){
		var id_sdm = $(this).data('id_sdm');
		var name = $(this).data('name');
		var csdm = $(this).data('csdm');
		var id_sdm2 = $(this).data('id_sdm2');
		var name2 = $(this).data('name2');
		var csdm2 = $(this).data('csdm2');
		// var notes = $(this).data('notes');
		var notes = $(this).data('notes');
		var namefile = $(this).data('namefile');
		var pathfile = $(this).data('pathfile');
		var namefile2 = $(this).data('namefile2');
		var pathfile2 = $(this).data('pathfile2');
		var namefile3 = $(this).data('namefile3');
		var pathfile3 = $(this).data('pathfile3');
		var id_doc = $(this).data('id_doc');
		var kategori_file = $(this).data('kategori_file');
		var cek = $(this).data('cek');
		// alert(name+id_sdm);
		jQuery.ajax({
			success: function() {
				if(cek == "button_upload"){
					$('#name').val(name);
					$('#id_sdm').val(id_sdm);
					$('#csdm').val(csdm);
				}else if(cek == "button_reupload"){
					$('#name2').val(name2);
					$('#id_sdm2').val(id_sdm2);
					$('#csdm2').val(csdm2);
					$('#notes2').val(notes);
					$('#id_doc').val(id_doc);
					$('#kategori_file2').val(kategori_file);
					document.getElementById("namefile").innerHTML ="<a href=' "+base_url+"document/"+csdm2+"/"+pathfile+" '> "+namefile+" </a>";
					document.getElementById("namefile2").innerHTML ="<a href=' "+base_url+"document/"+csdm2+"/"+pathfile2+" '> "+namefile2+" </a>";
					document.getElementById("namefile3").innerHTML ="<a href=' "+base_url+"document/"+csdm2+"/"+pathfile3+" '> "+namefile3+" </a>";

				}
			}
		});
	})
	//=======================//

	$('#button_submit').click(function(){
        table_pegawai.ajax.reload();
    });

	//=======================//
	//upload file
	$('#do_upload').submit(function(e){
		e.preventDefault();
		jQuery.ajax({
			url:base_url+'cs_document/do_upload_document',
			type:"post",
			data:new FormData(this),
			processData:false,
			contentType:false,
			cache:false,
			async:false,
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				console.log(data['hasil']);
				if (data['hasil'] == 'success') {
					$('#do_upload').trigger("reset");
					$('#modalupload1').modal('hide');
					alert('Data Berhasil di Simpan.');
					window.location = base_url+data['url'];
					window.location = base_url+'tl/content/data_agent';
					// table_pegawai.ajax.reload();
				}
				else{ alert(data['hasil']); }
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	});
	//reupload file
	$('#do_reupload').submit(function(e){
		e.preventDefault();
		jQuery.ajax({
			url:base_url+'cs_document/do_reupload_document',
			type:"post",
			data:new FormData(this),
			processData:false,
			contentType:false,
			cache:false,
			async:false,
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				console.log(data['hasil']);
				if (data['hasil'] == 'success') {
					$('#do_reupload').trigger("reset");
					$('#modaledit1').modal('hide');
					alert('Data Berhasil di Update.');
					// table_pegawai.ajax.reload();
					// window.location = base_url+data['url'];
					window.location = base_url+'tl/content/data_agent';
				}
				else{ alert(data['hasil']); }
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	});
	//=======================//
})