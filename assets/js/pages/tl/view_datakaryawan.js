$(function () {
	var table =  $('#table-pegawai').DataTable({
		"order": []
	});

	//=======================//
	//Setting datepicker range
	$('#bs_datepicker_range_container').datepicker({
		autoclose: true,
		container: '#bs_datepicker_range_container'
	});
	//Setting datepicker
	// $('#bs_datepicker_container input').datepicker({
 //        autoclose: true,
 //        container: '#bs_datepicker_container'
 //    });
    //=======================//

	//=======================//
	//Hiden form filter and  upload
		$('#show_upload').click(function(){
			$('#form-upload').removeClass('hidden');
		});
		$('#close_upload').click(function(){
			$('#form-upload').addClass('hidden');
		});

		$('#show_filter').click(function(){
			$('#form-filter').removeClass('hidden');
		});
		$('#close_filter').click(function(){
			$('#form-filter').addClass('hidden');
		});
	//=======================//

	//=======================//
	//Create select jabatan//
		jQuery.ajax({
			url: base_url+'cs_sdm/get_jabatan',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					var o = '<option value="'+data['data'][i]['jabatan']+'">'+data['data'][i]['jabatan']+'</option>';
					$("#jabatan").append(o).selectpicker('refresh');
				}
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

	//=======================//
	//Create select unit//
		jQuery.ajax({
			url: base_url+'cs_unit/get_unit',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					var o = '<option value="'+data['data'][i]['name']+'">'+data['data'][i]['name']+'</option>';
					$("#unit").append(o).selectpicker('refresh');
				}
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

	//=======================//
	//Create select ctp//
		jQuery.ajax({
			url: base_url+'cs_unit/get_ctp',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					var o = '<option value="'+data['data'][i]['ctp']+'">'+data['data'][i]['ctp']+'</option>';
					$("#ctp").append(o).selectpicker('refresh');
				}
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

	//=======================//
	//Create select tl and spv//
		jQuery.ajax({
			url: base_url+'cs_sdm/get_tl_all',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					var o = '<option value="'+data['data'][i]['id']+'">'+data['data'][i]['name']+'</option>';
					$("#id_tl").append(o).selectpicker('refresh');
				}

				var temp = $("#temp_tl").val();
				$("#id_tl").val(temp).selectpicker('refresh');
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
		
		jQuery.ajax({
			url: base_url+'cs_sdm/get_spv',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					var o = '<option value="'+data['data'][i]['id']+'">'+data['data'][i]['name']+'</option>';
					$("#id_spv").append(o).selectpicker('refresh');
				}
				var temp = $("#temp_spv").val();
				$("#id_spv").val(temp).selectpicker('refresh');
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});

		//Create select responsive by TL//
		function get_selectspvbytl(){
			var id_tl = $('#id_tl').val();
			jQuery.ajax({
				url: base_url+'cs_sdm/get_selectspvbytl',
				type: 'POST',
				data: {id_tl:id_tl},
				dataType : 'json',
				success: function(data, textStatus, xhr) {
					var o;
					if (id_tl == 'All') {
						var o = '<option value="All" selected>All</option>';
						for (var i = 0; i < Object.keys(data['data']).length; i++) {
							o += '<option value="'+data['data'][i]['id']+'">'+data['data'][i]['name']+'</option>';
						}
							$("#id_spv").html(o).selectpicker('refresh');
						// var temp = $("#temp_spv").val();
						// $("#id_spv").val(temp).selectpicker('refresh');
					}
					else{
						for (var i = 0; i < Object.keys(data['data']).length; i++) {
							o += '<option value="'+data['data'][i]['id_spv']+'">'+data['data'][i]['name_spv']+'</option>';
						}
						$("#id_spv").html(o).selectpicker('refresh');
					}
				},
				error: function(xhr, textStatus, errorThrown) {
					console.log(textStatus.reponseText);
				}
			});
		}
		
		$('#id_tl').change(function(){
			get_selectspvbytl();
		});
	//=======================//

	//=======================//
	//Filter karyawan//
		$('#form-filter_report').click(function(){
			var date_start = $('#date_start').val(); 
			var date_end = $('#date_end').val(); 
			var status_sdm = $('#status_sdm').val();
			var ctp = $('#ctp').val();
			var jabatan = $('#jabatan').val();
			var unit = $('#unit').val();
			var id_tl = $('#id_tl').val();
			var id_spv = $('#id_spv').val();
			var status_validate = 1;

			if(status_sdm == null){
				status_validate = 0;
				$('#status_sdm').closest('.form-line').addClass('error');
			}
			if(ctp == null){
				status_validate = 0;
				$('#ctp').closest('.form-line').addClass('error');
			}
			if(jabatan == null){
				status_validate = 0;
				$('#jabatan').closest('.form-line').addClass('error');
			}
			if(unit == null){
				status_validate = 0;
				$('#unit').closest('.form-line').addClass('error');
			}
			if(id_tl == null){
				status_validate = 0;
				$('#id_tl').closest('.form-line').addClass('error');
			}
			if(id_spv == null){
				status_validate = 0;
				$('#id_spv').closest('.form-line').addClass('error');
			}

			if(status_validate == 1 ){
				jQuery.ajax({
					url: base_url+'cs_sdm/get_viewsdm',
					type: 'POST',
					data: {status_sdm:status_sdm, unit:unit, jabatan:jabatan, date_start:date_start, date_end:date_end, ctp:ctp, id_tl:id_tl, id_spv:id_spv},
					dataType : 'json',
					success: function(data, textStatus, xhr) {
						$('#unit').closest('.form-line').removeClass('error');

						var str = '';
						for (var i = 0; i < Object.keys(data['data']).length; i++) {
							str+= '<tr>';
							str+= '<td>'+data['data'][i]['csdm']+'</td>';
							str+= '<td nowrap>'+data['data'][i]['name']+'</td>';
							str+= '<td>'+data['data'][i]['unit']+'</td>';
							str+= '<td>'+data['data'][i]['name_tl']+'</td>';
							str+= '<td>'+data['data'][i]['name_spv']+'</td>';
							str+= '<td>'+data['data'][i]['tgl_awal_kontrak']+'</td>';
							str+= '<td>'+data['data'][i]['tgl_akhir_kontrak']+'</td>';
							// str+= '<td>'+'<a href="'+base_url+'hr/update_sdm/'+data['data'][i]['id']+'" class="btn btn-success m-t-15 waves-effect">Edit</a>'+'</td>';
							str+= '</tr>';
						}

						table.destroy();
						$('#tbody').html(str);
						table = $('#table-pegawai').DataTable({
							"order": [],
							// "bDestroy": true
						});
					
					},
					error: function(xhr, textStatus, errorThrown) {
						console.log(textStatus.reponseText);
					}
				});
			}
		});
		$('#form-filter_report').trigger('click');
	//=======================//

	//=======================//
	//export excel
		$('#export').click(function(){
			var date_start0 = $('#date_start').val();
			let date_start1 = new Date(date_start0);
			let date_start = date_start1.getFullYear() + "-" + (date_start1.getMonth() + 1) + "-" + date_start1.getDate();
			var date_end0 = $('#date_end').val(); 
			let date_end1 = new Date(date_end0);
			let date_end = date_end1.getFullYear() + "-" + (date_end1.getMonth() + 1) + "-" + date_end1.getDate();

			var status_sdm = $('#status_sdm').val();
			var ctp = $('#ctp').val();
			var jabatan = $('#jabatan').val();
			var unit = $('#unit').val();
			var id_tl = $('#id_tl').val();
			var id_spv = $('#id_spv').val();

			if(typeof date_start == 'undefined' || date_start =='NaN-NaN-NaN'  || date_start =='' || date_start == null) { date_start = '-' }  
			if(typeof date_end == 'undefined' || date_end =='NaN-NaN-NaN' || date_start =='' || date_end == null) { date_end = '-' }
			if(typeof status_sdm == 'undefined' || status_sdm =='' || status_sdm == null) { status_sdm = '-' }
			if(typeof ctp == 'undefined' || ctp =='' || ctp == null) { ctp = '-' }
			if(typeof unit == 'undefined' || unit =='' || unit == null) { unit = '-' }
			if(typeof jabatan == 'undefined' || jabatan =='' || jabatan == null) { jabatan = '-' }
			if(typeof id_tl == 'undefined' || id_tl =='' || id_tl == null) { id_tl = '-' }
			if(typeof id_spv == 'undefined' || id_spv =='' || id_spv == null) { id_spv = '-' }
			var url = base_url+'cs_sdm/get_xls/'+date_start+'/'+date_end+'/'+status_sdm+'/'+ctp+'/'+jabatan+'/'+unit+'/'+id_tl+'/'+id_spv;
			// console.log(url);
			window.open(url);
		});
	//=======================//
})