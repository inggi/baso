$(function () {
	var d = new Date();

	//=======================//
	//Setting default fillter
		var year = d.getFullYear();
		$('#tahun').val(year);

		var month = d.getMonth();
		$('#month').val(month).selectpicker('refresh');
	//=======================//

	//=======================//
	//Create table kode pola jadwal//
		jQuery.ajax({
			url: base_url+'ca_pola_jadwal/get_pola_jadwal',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				var str = '';
				for (var i = 0; i < (Object.keys(data['data']).length/2).toFixed(0); i++) {
					str += '<tr>';
					str += '<td >'+(i+1)+'</td>';
					str += '<td >'+data['data'][i]['pola']+'</td>';
					str += '<td >'+data['data'][i]['jam_masuk']+'</td>';
					str += '<td >'+data['data'][i]['jam_pulang']+'</td>';
					str += '<td >'+data['data'][i]['ket']+'</td>';
					str += '</tr>';
				}
				$('#tbody_jadwal1').append(str);

				str = '';
				for (var i =(Object.keys(data['data']).length/2).toFixed(0); i < Object.keys(data['data']).length; i++) {
					str += '<tr>';
					str += '<td >'+(i+1)+'</td>';
					str += '<td >'+data['data'][i]['pola']+'</td>';
					str += '<td >'+data['data'][i]['jam_masuk']+'</td>';
					str += '<td >'+data['data'][i]['jam_pulang']+'</td>';
					str += '<td >'+data['data'][i]['ket']+'</td>';
					str += '</tr>';
				}
				$('#tbody_jadwal2').append(str);
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//
	
	//=======================//
	//Show pola jadwal
		$('#btn_show_pola').click(function(){
	        $('#pola_jadwal').removeClass('hidden');
	    });

		$('#btn-close_pola_jadwal').click(function(){
	        $('#pola_jadwal').addClass('hidden');
	    });
	//=======================//

	//=======================//
	//Create select nama agent//
		// jQuery.ajax({
		// 	url: base_url+'cs_sdm/get_unit_spv',
		// 	type: 'POST',
		// 	data: {},
		// 	dataType : 'json',
		// 	success: function(data, textStatus, xhr) {
		// 		for (var i = 0; i < Object.keys(data['data']).length; i++) {

		// 			var o = '<option value="'+data['data'][i]['id']+'" data-subtext="'+data['data'][i]['jabatan']+'">'+data['data'][i]['name']+'</option>';
		// 			$("#unit").append(o).selectpicker('refresh');
		// 		}
		// 	},
		// 	error: function(xhr, textStatus, errorThrown) {
		// 		console.log(textStatus.reponseText);
		// 	}
		// });
	//=======================//

	//Create select unit//
		jQuery.ajax({
			url: base_url+'cs_unit/get_unit',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				$("#spv option[value='search']").remove();
				for (var i = 0; i < Object.keys(data['data']).length; i++) {

					var o = '<option value="'+data['data'][i]['name']+'">'+data['data'][i]['name']+'</option>';
					$("#unit").append(o).selectpicker('refresh');
				}
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

	//=======================//
	//Setting table jadwal agent
		$('#form-filter_jadwal').click(function(){
			console.log($('#unit').val());
			if(!!$('#unit').val()){
				setting_jadwal_agent();
				$('#unit').closest('.form-line').removeClass('error');
			}
			else{
				$('#unit').closest('.form-line').addClass('error');
			}
		});

		function setting_jadwal_agent(){
		jQuery.ajax({
			url: base_url+'ca_jadwal/get_jadwal_byunit',
			type: 'POST',
			data: {month: $('#month').val(), year: $('#tahun').val(), unit: $('#unit').val()},
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				var str = '';
				var str_head = '<th>Name</th><th align="right">CSDM</th>';
				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					str += '<tr>';
					str += '<td scope="row" nowrap>'+data['data'][i]['name']+'</td>';
					str += '<td align="right">'+data['data'][i]['csdm']+'</td>';

					lastd = new Date($('#tahun').val(), (parseInt($('#month').val())+1), 0);
					var p = 0;
					for (var c = 1; c <= lastd.getDate(); c++) {
						if(i==0){
							var temp_date = new Date($('#tahun').val(), ($('#month').val()), c);
							var vc = temp_date.getDay() == 0 ? "bg-red":"";
							str_head += '<td class="'+vc+'" align="center">'+c+'</td>';
						}

						var pola='';
						if(typeof(data['data'][i]['jadwal']) !== 'undefined' && typeof(data['data'][i]['jadwal'][p]) !== 'undefined'){
							if( c == parseInt( data['data'][i]['jadwal'][p]['day'] )){
								pola = data['data'][i]['jadwal'][p]['pola'];
								p++;
							}
						}
						str += '<td align="center">'+pola+'</td>';
					}
					str += '</tr>';
				}
				
				$('#head_table').html('');
				$('#head_table').append(str_head);
				
				$('#tbody_jadwal').html('');
				$('#tbody_jadwal').append(str);
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
		}
	//=======================//

	//=======================//
	//export excel
		$('#export_excel').click(function(){
			console.log($('#unit').val());
			if(!!$('#unit').val()){
				export_jadwal();
				$('#unit').closest('.form-line').removeClass('error');
			}
			else{
				$('#unit').closest('.form-line').addClass('error');
			}
		});

		function export_jadwal(){
			var unit = $('#unit').val();
			var month = $('#month').val();
			var tahun = $('#tahun').val();
			
			var url = base_url+'ca_jadwal/get_xls_jadwal/'+month+'/'+tahun+'/'+unit;
			window.open(url);
		}
	//=======================//
})