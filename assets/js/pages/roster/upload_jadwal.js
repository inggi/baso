$(function () {
	//=======================//
	//Create table kode pola jadwal//
		jQuery.ajax({
			url: base_url+'ca_pola_jadwal/get_pola_jadwal',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				var str = '';
				for (var i = 0; i < (Object.keys(data['data']).length/2).toFixed(0); i++) {
					str += '<tr>';
					str += '<td >'+(i+1)+'</td>';
					str += '<td >'+data['data'][i]['pola']+'</td>';
					str += '<td >'+data['data'][i]['jam_masuk']+'</td>';
					str += '<td >'+data['data'][i]['jam_pulang']+'</td>';
					str += '<td >'+data['data'][i]['ket']+'</td>';
					str += '</tr>';
				}
				$('#tbody_jadwal1').append(str);

				str = '';
				for (var i = (Object.keys(data['data']).length/2).toFixed(0); i < Object.keys(data['data']).length; i++) {
					str += '<tr>';
					str += '<td >'+(i+1)+'</td>';
					str += '<td >'+data['data'][i]['pola']+'</td>';
					str += '<td >'+data['data'][i]['jam_masuk']+'</td>';
					str += '<td >'+data['data'][i]['jam_pulang']+'</td>';
					str += '<td >'+data['data'][i]['ket']+'</td>';
					str += '</tr>';
				}
				$('#tbody_jadwal2').append(str);
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

	//=======================//
	//Import pola jadwal//
		$('#do_upload_jadwal').submit(function(e){
			e.preventDefault();
			var formdata = new FormData(this);
			formdata.append('sheet', $('#sheet').val());

			$.ajax({
				url:base_url+'ca_jadwal/do_import_jadwal',
				type:"post",
				data:formdata,
				processData:false,
				contentType:false,
				cache:false,
				async:false,
				dataType : 'json',
				success: function(data){
					if (data['hasil'] == 'success') {
						alert('File Berhasil Di Import.');
					}
					else{
						alert(data['hasil']);
					}
				}
			});
		});
	//=======================//
})