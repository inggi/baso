$(function () {
	var table =  $('#table-pegawai').DataTable({
		"order": []
	});

	//=======================//
	//Hiden form filter and  upload
		$('#show_upload').click(function(){
			$('#form-upload').removeClass('hidden');
		});
		$('#close_upload').click(function(){
			$('#form-upload').addClass('hidden');
		});

		$('#show_filter').click(function(){
			$('#form-filter').removeClass('hidden');
		});
		$('#close_filter').click(function(){
			$('#form-filter').addClass('hidden');
		});
	//=======================//

	//=======================//
	//Create select unit//
		jQuery.ajax({
			url: base_url+'cs_unit/get_unit',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					var o = '<option value="'+data['data'][i]['name']+'">'+data['data'][i]['name']+'</option>';
					$("#unit").append(o).selectpicker('refresh');
				}
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

	//=======================//
	//Filter karyawan//
		$('#form-filter_report').click(function(){
			var unit = $('#unit').val();
			var status_validate = 1;

			if(unit == null){
				status_validate = 0;
				$('#unit').closest('.form-line').addClass('error');
			}

			if(status_validate == 1 ){
				jQuery.ajax({
					url: base_url+'cs_sdm/get_sdm',
					type: 'POST',
					data: {unit:unit},
					dataType : 'json',
					success: function(data, textStatus, xhr) {
						$('#unit').closest('.form-line').removeClass('error');

						var str = '';
						for (var i = 0; i < Object.keys(data['data']).length; i++) {
							str+= '<tr>';
							str+= '<td>'+data['data'][i]['csdm']+'</td>';
							str+= '<td nowrap>'+data['data'][i]['name']+'</td>';
							str+= '<td>'+data['data'][i]['unit']+'</td>';
							str+= '<td>'+data['data'][i]['jabatan']+'</td>';
							str+= '<td>'+data['data'][i]['jabatan_level']+'</td>';
							str+= '<td>'+data['data'][i]['name_tl']+'</td>';
							str+= '<td>'+data['data'][i]['name_spv']+'</td>';
							str+= '<td></td>';
							str+= '</tr>';
						}

						table.destroy();
						$('#tbody').html(str);
						table = $('#table-pegawai').DataTable({
							"order": [],
							// "bDestroy": true
						});
					
					},
					error: function(xhr, textStatus, errorThrown) {
						console.log(textStatus.reponseText);
					}
				});
			}
		});
	//=======================//

	//=======================//
	//Upload Karyawan//
		$('#do_upload').submit(function(e){
			e.preventDefault();
			var formdata = new FormData(this);
			formdata.append('sheet', $('#sheet').val());

			$.ajax({
				url:base_url+'cs_sdm/do_upload_sdm',
				type:"post",
				data:formdata,
				processData:false,
				contentType:false,
				cache:false,
				async:false,
				dataType : 'json',
				success: function(data){
					if (data['hasil'] == 'success') {
						alert('Berhasil!!\nJumlah Data Yang Berhasil di Update ' + data['message']);
					}
					else{
						alert(data['message']);
					}
				}
			});
		});
	//=======================//
})