$(function () {
	//=======================//
	//Import pola jadwal//
		$('#do_upload_jadwal').submit(function(e){
			e.preventDefault();
			var formdata = new FormData(this);
			formdata.append('sheet', $('#sheet').val());

			$.ajax({
				url:base_url+'cs_grading/do_import_grading',
				type:"post",
				data:formdata,
				processData:false,
				contentType:false,
				cache:false,
				async:false,
				dataType : 'json',
				success: function(data){
					if (data['hasil'] == 'success') {
						alert('Nilai Grading Berhasil Di Upload.');
					}
					else{
						alert(data['hasil']);
					}
				}
			});
		});
	//=======================//
})