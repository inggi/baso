$(function () {
	//=======================//
	//datatable 1
		jQuery.ajax({
			url: base_url+'cs_sdm/get_outofcontract',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				var str = '';
				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					str+= '<tr height="25px">';
					str+= '<td>'+data['data'][i]['csdm']+'</td>';
					str+= '<td nowrap>'+data['data'][i]['name']+'</td>';
					str+= '<td>'+data['data'][i]['jabatan']+'</td>';
					str+= '<td>'+data['data'][i]['tgl_awal_kontrak']+'</td>';
					str+= '<td>'+data['data'][i]['tgl_akhir_kontrak']+'</td>';
					str+= '<td>'+data['data'][i]['grading']+'</td>';
					str+= '<td>'+data['data'][i]['grade_upload']+'</td>';
					
					action = '<a href="'+base_url+'hr/update_sdm/'+data['data'][i]['id']+'" class="btn btn-xs btn-primary" title="edit sdm"><i class="material-icons">edit</i></a>';
					if(data['data'][i]['grading'] !== '-' ){
						action += ' <button type="button" id="btn_perpanjangkontrak" class="btn btn-success btn-xs" data-toggle="modal" data-target="#modalperpanjangkontrak" data-id_sdm="'+data['data'][i]['id']+'" data-csdm="'+data['data'][i]['csdm']+'" data-name="'+data['data'][i]['name']+'" data-grading="'+data['data'][i]['grading']+'" title="Perpanjang Otomatis"><i class="material-icons">verified_user</i></button>';
					}
					
					str+= '<td nowrap>'+action+'</td>';
					str+= '</tr>';
				}

				$('#tbody').html(str);

				table = $('#table_pegawai').DataTable({
					"pageLength": 50,
					"order": [],
					// "bDestroy": true
				});
			
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	// fungsi datatable
	function datatable_ajax(){
		jQuery.ajax({
			url: base_url+'cs_sdm/get_endcontract',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				var str = '';
				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					str+= '<tr height="25px">';
					str+= '<td>'+data['data'][i]['csdm']+'</td>';
					str+= '<td nowrap>'+data['data'][i]['name']+'</td>';
					str+= '<td>'+data['data'][i]['jabatan']+'</td>';
					str+= '<td>'+data['data'][i]['tgl_awal_kontrak']+'</td>';
					str+= '<td>'+data['data'][i]['tgl_akhir_kontrak']+'</td>';
					str+= '<td>'+data['data'][i]['grading']+'</td>';
					str+= '<td>'+data['data'][i]['grade_upload']+'</td>';
					
					action = '<a href="'+base_url+'hr/update_sdm/'+data['data'][i]['id']+'" class="btn btn-xs btn-primary" title="edit sdm"><i class="material-icons">edit</i></a>';
					if(data['data'][i]['grading'] == 'A' || data['data'][i]['grading'] == 'B' || data['data'][i]['grading'] == 'C'){
						action += ' <button type="button" id="btn_perpanjangkontrak" class="btn btn-success btn-xs" data-toggle="modal" data-target="#modalperpanjangkontrak" data-id_sdm="'+data['data'][i]['id']+'" data-csdm="'+data['data'][i]['csdm']+'" data-name="'+data['data'][i]['name']+'" data-grading="'+data['data'][i]['grading']+'" title="Perpanjang Otomatis"><i class="material-icons">verified_user</i></button>';
					}
					
					str+= '<td nowrap>'+action+'</td>';
					str+= '</tr>';
				}

				table.destroy();
				$('#tbody').html(str);
				table = $('#table_pegawai').DataTable({
					"pageLength": 50,
					"order": [],
					// "bDestroy": true
				});
			
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	}
	//=======================//

	//=======================//
	//modal value//
	$('#table_pegawai tbody').on('click', 'button', function(){
		var id_sdm = $(this).data('id_sdm');
		var csdm = $(this).data('csdm');
		var grading = $(this).data('grading');
		var name = $(this).data('name');
		$("#id_sdm").html('');
		$("#id_sdm").val(id_sdm);
		$("#grading").html('');
		$("#grading").val(grading);
		$("#name").html('');
		$("#name").val(''+name+' - '+csdm+'');
		$("#grading").html('');
		if (grading == 'A') {
			$("#gradingmodal").val('GRADE '+grading+' PERPANJANG KONTRAK 1 TAHUN');
		} else if (grading == 'B') {
			$("#gradingmodal").val('GRADE '+grading+' PERPANJANG KONTRAK 10 BULAN');
		} else if (grading == 'C') {
			$("#gradingmodal").val('GRADE '+grading+' PERPANJANG KONTRAK 6 BULAN');
		} else {
			$("#gradingmodal").val('GRADE INVALID');
			$('#modalperpanjangkontrak').modal('toggle');
			alert('GRADE INVALID');
		}
	});
	//modal button save_kontrak//
	$('#save_kontrak').click(function () {
		jQuery.ajax({
			url: base_url+'cs_sdm/do_perpanjang_kontrak',
			type: 'POST',
			data: {id_sdm: $('#id_sdm').val(), grading: $('#grading').val()},
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				if(data['hasil'] == 'success'){
					$('#modalperpanjangkontrak').modal('toggle');
					alert('Berhasil Perpanjang Kontrak');
					datatable_ajax();
				}
				else{ alert(data['hasil']); }
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	});
	//=======================//

	//=======================//
	//value on modal//
	// $('#table_pegawai tbody').on('click', 'button', function(){
	// 	var id_sdm = $(this).data('id_sdm');
	// 	var name = $(this).data('name');
	// 	var csdm = $(this).data('csdm');
	// 	var id_sdm2 = $(this).data('id_sdm2');
	// 	var name2 = $(this).data('name2');
	// 	var csdm2 = $(this).data('csdm2');
	// 	// var notes = $(this).data('notes');
	// 	var notes = $(this).data('notes');
	// 	var namefile = $(this).data('namefile');
	// 	var pathfile = $(this).data('pathfile');
	// 	var id_doc = $(this).data('id_doc');
	// 	var cek = $(this).data('cek');
	// 	// alert(name+id_sdm);
	// 	jQuery.ajax({
	// 		success: function() {
	// 			if(cek == "button_upload"){
	// 				$('#name').val(name);
	// 				$('#id_sdm').val(id_sdm);
	// 				$('#csdm').val(csdm);
	// 			}else if(cek == "button_reupload"){
	// 				$('#name2').val(name2);
	// 				$('#id_sdm2').val(id_sdm2);
	// 				$('#csdm2').val(csdm2);
	// 				$('#notes2').val(notes);
	// 				$('#id_doc').val(id_doc);
	// 				document.getElementById("namefile").innerHTML ="<a href=' "+base_url+"document/"+csdm2+"/"+pathfile+" '> "+namefile+" </a>";

	// 			}
	// 		}
	// 	});
	// })
	//=======================//

	// $('#button_submit').click(function(){
 //        table_pegawai.ajax.reload();
 //    });

	//=======================//
	//upload file
	// $('#do_upload').submit(function(e){
	// 	e.preventDefault();
	// 	jQuery.ajax({
	// 		url:base_url+'cs_document/do_upload_document',
	// 		type:"post",
	// 		data:new FormData(this),
	// 		processData:false,
	// 		contentType:false,
	// 		cache:false,
	// 		async:false,
	// 		dataType : 'json',
	// 		success: function(data, textStatus, xhr) {
	// 			console.log(data['hasil']);
	// 			if (data['hasil'] == 'success') {
	// 				$('#do_upload').trigger("reset");
	// 				$('#modalupload1').modal('hide');
	// 				alert('Data Berhasil di Simpan.');
	// 				// window.location = base_url+data['url'];
	// 				window.location = base_url+'hr/content/outofcontract';
	// 				// table_pegawai.ajax.reload();
	// 			}
	// 			else{ alert(data['hasil']); }
	// 		},
	// 		error: function(xhr, textStatus, errorThrown) {
	// 			console.log(textStatus.reponseText);
	// 		}
	// 	});
	// });
	//reupload file
	// $('#do_reupload').submit(function(e){
	// 	e.preventDefault();
	// 	jQuery.ajax({
	// 		url:base_url+'cs_document/do_reupload_document',
	// 		type:"post",
	// 		data:new FormData(this),
	// 		processData:false,
	// 		contentType:false,
	// 		cache:false,
	// 		async:false,
	// 		dataType : 'json',
	// 		success: function(data, textStatus, xhr) {
	// 			console.log(data['hasil']);
	// 			if (data['hasil'] == 'success') {
	// 				$('#do_reupload').trigger("reset");
	// 				$('#modaledit1').modal('hide');
	// 				alert('Data Berhasil di Update.');
	// 				// table_pegawai.ajax.reload();
	// 				// window.location = base_url+data['url'];
	// 				window.location = base_url+'hr/content/outofcontract';
					
	// 			}
	// 			else{ alert(data['hasil']); }
	// 		},
	// 		error: function(xhr, textStatus, errorThrown) {
	// 			console.log(textStatus.reponseText);
	// 		}
	// 	});
	// });
	//=======================//

	//=======================//
	//export excel
		// $('#export').click(function(){
		// 	var date_start0 = $('#date_start').val();
		// 	let date_start1 = new Date(date_start0);
		// 	let date_start = date_start1.getFullYear() + "-" + (date_start1.getMonth() + 1) + "-" + date_start1.getDate();
		// 	var date_end0 = $('#date_end').val(); 
		// 	let date_end1 = new Date(date_end0);
		// 	let date_end = date_end1.getFullYear() + "-" + (date_end1.getMonth() + 1) + "-" + date_end1.getDate();
			
		// 	var unit = $('#unit').val();
		// 	var ctp = $('#ctp').val();
			
		// 	if(typeof date_start == 'undefined' || date_start =='NaN-NaN-NaN'  || date_start =='' || date_start == null) { date_start = '-' }  
		// 	if(typeof date_end == 'undefined' || date_end =='NaN-NaN-NaN' || date_start =='' || date_end == null) { date_end = '-' }  
		// 	if(typeof unit == 'undefined' || unit =='' || unit == null) { unit = '-' }  
		// 	if(typeof ctp == 'undefined' || ctp =='' || ctp == null) { ctp = '-' }  
		// 	var url = base_url+'cs_sdm/get_xls/'+date_start+'/'+date_end+'/'+unit+'/'+ctp;

		// 	window.open(url);
		// });
	//=======================//
})