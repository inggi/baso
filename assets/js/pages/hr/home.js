$(function () {

	//=======================//
	//Create array home//
		jQuery.ajax({
			url: base_url+'cs_sdm/get_home_totalkaryawanbyjabatan',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				var str = 'a';
				for (var i = 0; i < Object.keys(data['data']).length; i++) {

					var o = '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" ><div class="info-box-3 bg-green hover-zoom-effect" ><div class="icon"><i class="material-icons">assignment_ind</i></div><div class="content"><div class="text">'+data['data'][i]['jabatan']+'</div><div class="number">'+data['data'][i]['total']+'</div></div></div></div>';

					$("#homee").append(o);
				}
				// $('#homee').html(str);
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//	

	var table =  $('#table-pegawai').DataTable({
		"order": []
	});
	//=======================//
	//home//
		jQuery.ajax({
			url: base_url+'cs_sdm/get_total_home_userhr2',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {
			
				var str = '';
				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					str+= '<tr>';
					str+= '<td>'+data['data'][i]['unit']+'</td>';
					str+= '<td>'+data['data'][i]['kode_los_rumus']+'</td>';
					str+= '<td>'+data['data'][i]['csdm']+'</td>';
					// str+= '<td nowrap>'+data['data'][i]['name']+'</td>';
					// str+= '<td>'+data['data'][i]['los_rumus']+'</td>';
					// str+= '<td>'+data['data'][i]['tgl_awal_kontrak']+'</td>';
					// str+= '<td>'+data['data'][i]['tgl_akhir_kontrak']+'</td>';
					str+= '</tr>';
				}

				table.destroy();
				$('#tbody').html(str);
				table = $('#table-pegawai').DataTable({
					"order": [],
					// "bDestroy": true
				});
			
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

})