$(function () {

	$('#bs_datepicker_container input').datepicker({
        autoclose: true,
        container: '#bs_datepicker_container'
    });

	//=======================//
	//Create select jabatan//
		function get_selectjabatan(){
			jQuery.ajax({
				url: base_url+'cs_jabatan/get_jabatan',
				type: 'POST',
				data: {},
				dataType : 'json',
				success: function(data, textStatus, xhr) {
					var temp = $("#temp_jabatan").val();
					z = '<option value="'+temp+'" readonly>'+temp+'</option>';
					$("#jabatan").html(z).selectpicker('refresh');

					for (var i = 0; i < Object.keys(data['data']).length; i++) {
						var o = '<option value="'+data['data'][i]['jabatan']+'">'+data['data'][i]['jabatan']+'</option>';
						$("#jabatan").append(o).selectpicker('refresh');
					}
					var temp = $("#temp_jabatan").val();
					$("#jabatan").val(temp).selectpicker('refresh');
				},
				error: function(xhr, textStatus, errorThrown) {
					console.log(textStatus.reponseText);
				}
			});
		}
		jQuery.ajax({
			url: base_url+'cs_jabatan/get_jabatan',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				var temp = $("#temp_jabatan").val();
				// $("#jabatan").val(temp).selectpicker('refresh');
				z = '<option value="'+temp+'" readonly>'+temp+'</option>';
				$("#jabatan").html(z).selectpicker('refresh');
				get_selectjabatan();
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//modal button save_jabatan//
		$('#save_jabatan').click(function () {
			jQuery.ajax({
				url: base_url+'cs_jabatan/do_insert_jabatan',
				type: 'POST',
				data: {nama_jabatan_baru: $('#nama_jabatan_baru').val()},
				dataType : 'json',
				success: function(data, textStatus, xhr) {
					if(data['hasil'] == 'success'){
						$('#modaltambahjabatan').modal('toggle');
						$('#nama_jabatan_baru').trigger("reset");
						$('#nama_jabatan_baru').val('');
						get_selectjabatan();
						alert('Berhasil Tambah Jabatan Baru');
					}
				},
				error: function(xhr, textStatus, errorThrown) {
					console.log(textStatus.reponseText);
				}
			});
		});
	//=======================//
	//value modal remove jabatan//
		function get_selectjabatan2(){
			jQuery.ajax({
				url: base_url+'cs_jabatan/get_jabatan',
				type: 'POST',
				data: {},
				dataType : 'json',
				success: function(data, textStatus, xhr) {
					var str = '';
					$("#listnamajabatan").html('');
					for (var i = 0; i < Object.keys(data['data']).length; i++) {
						str += '<tr>';
						str += '<td >'+data['data'][i]['jabatan']+'</td>';
						str += '<td ><button type="button" class="btn bg-red btn-xs" data-id_jabatan="'+data['data'][i]['id_jabatan']+'" id="hapus_jabatan">Hapus</button></td>';
						str += '</tr>';
					}
					$('#listnamajabatan').append(str);
				},
				error: function(xhr, textStatus, errorThrown) {
					console.log(textStatus.reponseText);
				}
			});
		}
	//button get value modal remove jabatan//
		$('#button_modal_hapus_jabatan').click(function () {
			get_selectjabatan2();
		});
	//Delete jabatan//
		$('#table_jabatan tbody').on('click', 'button', function(){
			var id_jabatan = $(this).data('id_jabatan');
			// alert(id_jabatan);
			jQuery.ajax({
				url: base_url+'cs_jabatan/do_delete_jabatan',
				type: 'POST',
				data: {id_jabatan: id_jabatan},
				dataType : 'json',
				success: function(data, textStatus, xhr) {
					if(data['hasil'] == 'success'){
						get_selectjabatan2();
						get_selectjabatan();
						alert('Berhasil Hapus jabatan');
					}
				},
				error: function(xhr, textStatus, errorThrown) {
					console.log(textStatus.reponseText);
				}
			});
		})
	//=======================//

	//=======================//
	//Create select unit//
		jQuery.ajax({
			url: base_url+'cs_unit/get_unit',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					var o = '<option value="'+data['data'][i]['name']+'">'+data['data'][i]['name']+'</option>';
					$("#unit").append(o).selectpicker('refresh');
				}
				var temp = $("#temp_unit").val();
				$("#unit").val(temp).selectpicker('refresh');
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

	//=======================//
	//Create select ctp//
		jQuery.ajax({
			url: base_url+'cs_unit/get_ctp',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					var o = '<option value="'+data['data'][i]['ctp']+'">'+data['data'][i]['ctp']+'</option>';
					$("#ctp").append(o).selectpicker('refresh');
				}

				var temp = $("#temp_ctp").val();
				$("#ctp").val(temp).selectpicker('refresh');
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

	//=======================//
	//Create select responsive jabatan_level//
		function get_select_tl_byjabatanlevel(){
			var jabatan_level = $('#jabatan_level').val();
			jQuery.ajax({
			url: base_url+'cs_sdm/get_tl',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				var o;
				if (jabatan_level == 'STAFF') {
					for (var i = 0; i < Object.keys(data['data']).length; i++) {
						var o = '<option value="'+data['data'][i]['id']+'" data-subtext="'+data['data'][i]['jabatan']+'">'+data['data'][i]['name']+'</option>';
						$("#id_tl").append(o).selectpicker('refresh');
					}
					var temp = $("#temp_tl").val();
					$("#id_tl").val(temp).selectpicker('refresh');
				}
				else{
					o += '<option value="" readonly>NO TL</option>';	
					$("#id_tl").html(o).selectpicker('refresh');
				}
				
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
		}

		function get_select_spv_byjabatanlevel(){
			var jabatan_level = $('#jabatan_level').val();
			jQuery.ajax({
			url: base_url+'cs_sdm/get_spv',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				var o;
				if (jabatan_level == 'STAFF' || jabatan_level == 'ROSTER' || jabatan_level == 'TL') {
					for (var i = 0; i < Object.keys(data['data']).length; i++) {
						var o = '<option value="'+data['data'][i]['id']+'" data-subtext="'+data['data'][i]['jabatan']+'">'+data['data'][i]['name']+'</option>';
						$("#id_spv").append(o).selectpicker('refresh');
					}
					var temp = $("#temp_spv").val();
					$("#id_spv").val(temp).selectpicker('refresh');
					// get_select_tl_byjabatanlevel();
				}
				else if (jabatan_level == 'ADMIN') {
					var o = '<option value="" readonly>NO SPV</option>';
					for (var i = 0; i < Object.keys(data['data']).length; i++) {
						var o = '<option value="'+data['data'][i]['id']+'" data-subtext="'+data['data'][i]['jabatan']+'">'+data['data'][i]['name']+'</option>';
						$("#id_spv").append(o).selectpicker('refresh');
					}
					var temp = $("#temp_spv").val();
					$("#id_spv").val(temp).selectpicker('refresh');
					// get_select_tl_byjabatanlevel();
				}
				else{
					o += '<option value="" readonly>NO SPV</option>';
					$("#id_spv").html(o).selectpicker('refresh');
				}
				
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
		}
		
		$('#jabatan_level').change(function(){
			get_select_tl_byjabatanlevel();
			get_select_spv_byjabatanlevel();
		});
	//=======================//

	//=======================//
	//Create select tl and spv//
		jQuery.ajax({
			url: base_url+'cs_sdm/get_tl',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					var o = '<option value="'+data['data'][i]['id']+'" data-subtext="'+data['data'][i]['jabatan']+'">'+data['data'][i]['name']+'</option>';
					$("#id_tl").append(o).selectpicker('refresh');
				}

				var temp = $("#temp_tl").val();
				$("#id_tl").val(temp).selectpicker('refresh');
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
		
		jQuery.ajax({
			url: base_url+'cs_sdm/get_spv',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					var o = '<option value="'+data['data'][i]['id']+'" data-subtext="'+data['data'][i]['jabatan']+'">'+data['data'][i]['name']+'</option>';
					$("#id_spv").append(o).selectpicker('refresh');
				}
				var temp = $("#temp_spv").val();
				$("#id_spv").val(temp).selectpicker('refresh');
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});

	//Create select responsive by TL//
		function get_selectspv(){
			jQuery.ajax({
				url: base_url+'cs_sdm/get_spv',
				type: 'POST',
				data: {},
				dataType : 'json',
				success: function(data, textStatus, xhr) {

					for (var i = 0; i < Object.keys(data['data']).length; i++) {
						var o = '<option value="'+data['data'][i]['id']+'" data-subtext="'+data['data'][i]['jabatan']+'">'+data['data'][i]['name']+'</option>';
						$("#id_spv").append(o).selectpicker('refresh');
					}
				},
				error: function(xhr, textStatus, errorThrown) {
					console.log(textStatus.reponseText);
				}
			})
		}
		function get_selectspvbytl(){
			var id_tl = $('#id_tl').val();
			jQuery.ajax({
				url: base_url+'cs_sdm/get_selectspvbytl',
				type: 'POST',
				data: {id_tl:id_tl},
				dataType : 'json',
				success: function(data, textStatus, xhr) {
					var o;
					if (id_tl == 'All') {
						for (var i = 0; i < Object.keys(data['data']).length; i++) {
							var o = '<option value="'+data['data'][i]['id']+'" data-subtext="'+data['data'][i]['jabatan']+'">'+data['data'][i]['name']+'</option>';
							$("#id_spv").append(o).selectpicker('refresh');
						}
						var temp = $("#temp_spv").val();
						$("#id_spv").val(temp).selectpicker('refresh');
					}
					else{
						for (var i = 0; i < Object.keys(data['data']).length; i++) {

							o += '<option value="'+data['data'][i]['id_spv']+'" data-subtext="'+data['data'][i]['jabatan']+'">'+data['data'][i]['name_spv']+'</option>';
						}
						$("#id_spv").html(o).selectpicker('refresh');
						get_selectspv();
					}
				},
				error: function(xhr, textStatus, errorThrown) {
					console.log(textStatus.reponseText);
				}
			});
		}
		
		$('#id_tl').change(function(){
			get_selectspvbytl();
		});
	//=======================//

	// $('#button_submit2').click(function(e){
	// 	alert('tes');
	// });
	//=======================//
	//Update SDM//
		$('#update_sdm_form').submit(function(e){
			//alert('tes');
			e.preventDefault();
			jQuery.ajax({
				url: base_url+'cs_sdm/do_update_sdm2',
				type: 'POST',
				data: new FormData(this),
				processData:false,
				contentType:false,
				cache:false,
				async:false,
	            dataType : 'json',
				success: function(data, textStatus, xhr) {
					//console.log('aa');
					console.log(data['hasil']);
					if (data['hasil'] == 'success') {
						alert('Data Berhasil di update.');
						window.location = base_url+data['url'];
						//window.location = base_url+'page/content';
					}
					else{ alert(data['hasil']); }
				},
				error: function(xhr, textStatus, errorThrown) {
					console.log(textStatus.reponseText);
				}
			});
		});
	//Update SDM old//
		// $('.update_sdm').change(function(){
		// 	var column = $(this).attr('id');
		// 	var value = $(this).val();
		// 	var id = $('#id_sdm').val();
		// 	var temp = $(this).prev('input').val();
		// 	if(value != temp){
		// 		jQuery.ajax({
		// 			url: base_url+'cs_sdm/do_update_sdm',
		// 			type: 'POST',
		// 			data: {id:id, column:column, value:value},
		// 			dataType : 'json',
		// 			success: function(data, textStatus, xhr) {
		// 				if(data['hasil'] == 'success'){
		// 					$('#message').html(data['message']);
		// 					$('#success-update').removeClass('hidden');

		// 					setTimeout(function(){ $('#success-update').addClass('hidden'); }, 3000);
		// 				}
		// 				else{
		// 					alert(data['message']);
		// 				}
		// 			},
		// 			error: function(xhr, textStatus, errorThrown) {
		// 				console.log(textStatus.reponseText);
		// 			}
		// 		});
		// 	}
		// });
	//=======================//
})