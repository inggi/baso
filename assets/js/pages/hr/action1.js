$(function () {
	//=======================//
	//datatable 1
	var url1 = window.location.href;
	console.log(url1);
	var pathurl = window.location.pathname;
	pathurl = pathurl.substr(28, pathurl.lastIndexOf('/'));
	// pathurl = pathurl.substr(25, pathurl.lastIndexOf('/'));
	console.log(pathurl);

		jQuery.ajax({
			url: base_url+'cs_sdm/get_rekomendasi_spvtl2',
			type: 'POST',
			data: {pathurl:pathurl},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				var str = '';
				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					str+= '<tr height="25px">';
					str+= '<td>'+data['data'][i]['csdm']+'</td>';
					str+= '<td nowrap>'+data['data'][i]['name']+'</td>';
					str+= '<td>'+data['data'][i]['jabatan']+'</td>';
					str+= '<td>'+data['data'][i]['name_tl']+'</td>';
					str+= '<td>'+data['data'][i]['name_spv']+'</td>';
					str+= '<td>'+data['data'][i]['tgl_awal_kontrak']+'</td>';
					str+= '<td>'+data['data'][i]['tgl_akhir_kontrak']+'</td>';
					
					var action='';
					if(data['data'][i]['kategori_file'] == 'CONTRACT'){
						action = '<a href="'+base_url+'hr/update_sdm2/'+data['data'][i]['id']+'" class="btn btn-xs bg-green"><i class="material-icons">edit</i></a>';
					}
					else if(data['data'][i]['kategori_file'] == 'RESIGN ON THE SPOT'){
						action = '<a href="'+base_url+'hr/update_sdm2/'+data['data'][i]['id']+'" class="btn btn-xs bg-indigo"><i class="material-icons">edit</i></a>';
					}
					else if(data['data'][i]['kategori_file'] == 'RESIGN ONE MONTH NOTICE'){
						action = '<a href="'+base_url+'hr/update_sdm2/'+data['data'][i]['id']+'" class="btn btn-xs bg-teal"><i class="material-icons">edit</i></a>';
					}
					else if(data['data'][i]['kategori_file'] == 'CUTI MELAHIRKAN'){
						action = '<a href="'+base_url+'hr/update_sdm2/'+data['data'][i]['id']+'" class="btn btn-xs bg-blue"><i class="material-icons">edit</i></a>';
					}
					else{
						action = '<a href="'+base_url+'hr/update_sdm2/'+data['data'][i]['id']+'" class="btn btn-xs bg-deep-orange"><i class="material-icons">edit</i></a>';
					}

					str+= '<td nowrap>'+action+'</td>';
					str+= '</tr>';
				}

				$('#tbody').html(str);

				table = $('#table_pegawai').DataTable({
					"pageLength": 50,
					"order": [],
					// "bDestroy": true
				});
			
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

	//=======================//
	//export excel
		$('#export').click(function(){
			var url = base_url+'cs_sdm/get_xls_rekomendasi/'+pathurl;
			// console.log(url);
			window.open(url);
		});
	//=======================//
})