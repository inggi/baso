$(function () {
	//=======================//
	//datatable 1
		jQuery.ajax({
			url: base_url+'cs_sdm/get_rekomendasi_spvtl',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				var str = '';
				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					str+= '<tr height="25px">';
					str+= '<td>'+data['data'][i]['csdm']+'</td>';
					str+= '<td nowrap>'+data['data'][i]['name']+'</td>';
					str+= '<td>'+data['data'][i]['jabatan']+'</td>';
					str+= '<td>'+data['data'][i]['name_tl']+'</td>';
					str+= '<td>'+data['data'][i]['name_spv']+'</td>';
					str+= '<td>'+data['data'][i]['tgl_awal_kontrak']+'</td>';
					str+= '<td>'+data['data'][i]['tgl_akhir_kontrak']+'</td>';
					
					var action='';
					if(data['data'][i]['kategori_file'] == 'CONTRACT < 30D'){
						action = '<a href="'+base_url+'hr/update_sdm2/'+data['data'][i]['id']+'" class="btn btn-xs bg-green"><i class="material-icons">edit</i></a>';
					}
					else if(data['data'][i]['kategori_file'] == 'RESIGN ON THE SPOT'){
						action = '<a href="'+base_url+'hr/update_sdm2/'+data['data'][i]['id']+'" class="btn btn-xs bg-indigo"><i class="material-icons">edit</i></a>';
					}
					else if(data['data'][i]['kategori_file'] == 'RESIGN ONE MONTH NOTICE'){
						action = '<a href="'+base_url+'hr/update_sdm2/'+data['data'][i]['id']+'" class="btn btn-xs bg-teal"><i class="material-icons">edit</i></a>';
					}
					else{
						action = '<a href="'+base_url+'hr/update_sdm2/'+data['data'][i]['id']+'" class="btn btn-xs bg-deep-orange"><i class="material-icons">edit</i></a>';
					}

					str+= '<td nowrap>'+action+'</td>';
					str+= '</tr>';
				}

				$('#tbody').html(str);

				table = $('#table_pegawai').DataTable({
					"pageLength": 50,
					"order": [],
					// "bDestroy": true
				});
			
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//
	//datatable 2
	// var table_pegawai =  $('#table_pegawai').DataTable({
	// 	"ajax":{
	// 		url: base_url+'cs_sdm/get_endcontract_userspv',
	// 		type: 'POST',
	// 		data: {},
	// 		dataType : 'json',
	// 		success: function(data, textStatus, xhr) {

	// 			var str = '';
	// 			for (var i = 0; i < Object.keys(data['data']).length; i++) {
	// 				str+= '<tr height="25px">';
	// 				str+= '<td>'+data['data'][i]['csdm']+'</td>';
	// 				str+= '<td nowrap>'+data['data'][i]['name']+'</td>';
	// 				str+= '<td>'+data['data'][i]['jabatan']+'</td>';
	// 				str+= '<td>'+data['data'][i]['tgl_awal_kontrak']+'</td>';
	// 				str+= '<td>'+data['data'][i]['tgl_akhir_kontrak']+'</td>';
					
	// 				var action='';
	// 				if(data['data'][i]['status'] == null ){
	// 					action = '<button type="button" data-cek="button_upload" class="btn btn-primary btn-xs modalupload1" data-toggle="modal" data-target="#modalupload1" data-id_sdm="'+data['data'][i]['id']+'" data-name="'+data['data'][i]['name']+'" data-csdm="'+data['data'][i]['csdm']+'"><i class="material-icons">file_upload</i></button>';
	// 				}else{
	// 					action = '<button type="button" data-cek="button_reupload" class="btn btn-success btn-xs modaledit1" data-toggle="modal" data-target="#modaledit1" data-id_sdm2="'+data['data'][i]['id']+'" data-name2="'+data['data'][i]['name']+'" data-csdm2="'+data['data'][i]['csdm']+'" data-notes="'+data['data'][i]['notes']+'" data-namefile="'+data['data'][i]['namefile']+'" data-pathfile="'+data['data'][i]['pathfile']+'" data-id_doc="'+data['data'][i]['id_doc']+'"><i class="material-icons">edit</i></button>';
	// 				}

	// 				str+= '<td nowrap>'+action+'</td>';
	// 				str+= '</tr>';
	// 			}

	// 			$('#tbody').html(str);
			
	// 		},
	// 		error: function(xhr, textStatus, errorThrown) {
	// 			console.log(textStatus.reponseText);
	// 		}
	// 	},
	// 	"pageLength": 5,
	// 	"order": [],
	// });
	//=======================//
	//datatable 3
	// var table_pegawai =  $('#table_pegawai').DataTable({
	// 	"responsive": true,
	// 	"bLengthChange": false,
	// 	"bDestroy": true,
	// 	"bFilter": false,
	// 	"bProcessing": true,
	// 	// "serverSide": false,
	// 	"serverSide": true,
	// 	"columns": [
	// 		{ "data": "csdm" },
	// 		{ "data": "name" },
	// 		{ "data": "jabatan" },
	// 		{ "data": "tgl_awal_kontrak" },
	// 		{ "data": "tgl_akhir_kontrak" },
	// 		{ 
	//             "data": "id",
	//             render : function(data, type, row, meta) { 
	//             	if(row['status'] == null ){ return '<button type="button" data-cek="button_upload" class="btn btn-primary btn-xs modalupload1" data-toggle="modal" data-target="#modalupload1" data-id_sdm="'+data+'" data-name="'+row['name']+'" data-csdm="'+row['csdm']+'"><i class="material-icons">file_upload</i></button>'; }
	//             	else { return '<button type="button" data-cek="button_reupload" class="btn btn-success btn-xs modaledit1" data-toggle="modal" data-target="#modaledit1" data-id_sdm2="'+data+'" data-name2="'+row['name']+'" data-csdm2="'+row['csdm']+'" data-notes="'+row['notes']+'" data-namefile="'+row['namefile']+'" data-pathfile="'+row['pathfile']+'" data-id_doc="'+row['id_doc']+'"><i class="material-icons">edit</i></button>'; }
	//             }
	//         },
	// 	],
	// 	"ajax":{
	// 		url: base_url+'cs_sdm/get_endcontract_userspv',
	// 		type: 'POST',
	// 		data: function(d){
	// 			// d.date = $('#date_start').val();
	// 			// d.agent = $('#agent').val();
	// 		},
	// 		error: function(){  // error handling code
	// 			$('#table-element').css("display","none");
	// 		}
	// 	},
	// 	"pageLength": 50,
	// 	"order": [[ 0, "asc" ]]
	// });
	//=======================//

	//=======================//
	//value on modal//
	$('#table_pegawai tbody').on('click', 'button', function(){
		var id_sdm = $(this).data('id_sdm');
		var name = $(this).data('name');
		var csdm = $(this).data('csdm');
		var id_sdm2 = $(this).data('id_sdm2');
		var name2 = $(this).data('name2');
		var csdm2 = $(this).data('csdm2');
		// var notes = $(this).data('notes');
		var notes = $(this).data('notes');
		var namefile = $(this).data('namefile');
		var pathfile = $(this).data('pathfile');
		var id_doc = $(this).data('id_doc');
		var cek = $(this).data('cek');
		// alert(name+id_sdm);
		jQuery.ajax({
			success: function() {
				if(cek == "button_upload"){
					$('#name').val(name);
					$('#id_sdm').val(id_sdm);
					$('#csdm').val(csdm);
				}else if(cek == "button_reupload"){
					$('#name2').val(name2);
					$('#id_sdm2').val(id_sdm2);
					$('#csdm2').val(csdm2);
					$('#notes2').val(notes);
					$('#id_doc').val(id_doc);
					document.getElementById("namefile").innerHTML ="<a href=' "+base_url+"document/"+csdm2+"/"+pathfile+" '> "+namefile+" </a>";

				}
			}
		});
	})
	//=======================//

	$('#button_submit').click(function(){
        table_pegawai.ajax.reload();
    });

	//=======================//
	//upload file
	$('#do_upload').submit(function(e){
		e.preventDefault();
		jQuery.ajax({
			url:base_url+'cs_document/do_upload_document',
			type:"post",
			data:new FormData(this),
			processData:false,
			contentType:false,
			cache:false,
			async:false,
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				console.log(data['hasil']);
				if (data['hasil'] == 'success') {
					$('#do_upload').trigger("reset");
					$('#modalupload1').modal('hide');
					alert('Data Berhasil di Simpan.');
					// window.location = base_url+data['url'];
					window.location = base_url+'spv/content/endkontrak';
					// table_pegawai.ajax.reload();
				}
				else{ alert(data['hasil']); }
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	});
	//reupload file
	$('#do_reupload').submit(function(e){
		e.preventDefault();
		jQuery.ajax({
			url:base_url+'cs_document/do_reupload_document',
			type:"post",
			data:new FormData(this),
			processData:false,
			contentType:false,
			cache:false,
			async:false,
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				console.log(data['hasil']);
				if (data['hasil'] == 'success') {
					$('#do_reupload').trigger("reset");
					$('#modaledit1').modal('hide');
					alert('Data Berhasil di Update.');
					// table_pegawai.ajax.reload();
					// window.location = base_url+data['url'];
					window.location = base_url+'spv/content/endkontrak';
					
				}
				else{ alert(data['hasil']); }
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	});
	//=======================//

	//=======================//
	//export excel
		// $('#export').click(function(){
		// 	var date_start0 = $('#date_start').val();
		// 	let date_start1 = new Date(date_start0);
		// 	let date_start = date_start1.getFullYear() + "-" + (date_start1.getMonth() + 1) + "-" + date_start1.getDate();
		// 	var date_end0 = $('#date_end').val(); 
		// 	let date_end1 = new Date(date_end0);
		// 	let date_end = date_end1.getFullYear() + "-" + (date_end1.getMonth() + 1) + "-" + date_end1.getDate();
			
		// 	var unit = $('#unit').val();
		// 	var ctp = $('#ctp').val();
			
		// 	if(typeof date_start == 'undefined' || date_start =='NaN-NaN-NaN'  || date_start =='' || date_start == null) { date_start = '-' }  
		// 	if(typeof date_end == 'undefined' || date_end =='NaN-NaN-NaN' || date_start =='' || date_end == null) { date_end = '-' }  
		// 	if(typeof unit == 'undefined' || unit =='' || unit == null) { unit = '-' }  
		// 	if(typeof ctp == 'undefined' || ctp =='' || ctp == null) { ctp = '-' }  
		// 	var url = base_url+'cs_sdm/get_xls/'+date_start+'/'+date_end+'/'+unit+'/'+ctp;

		// 	window.open(url);
		// });
	//=======================//
})