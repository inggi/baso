$(function () {

	//Filter karyawan//
		jQuery.ajax({
			url: base_url+'cs_pmb_flow_history/get_kesalahan_agent',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				var str = '';
				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					str+= '<tr>';
					str+= '<td>'+data['data'][i]['tgl_dibuat']+'</td>';
					str+= '<td>'+data['data'][i]['tgl_kejadian']+'</td>';
					str+= '<td>'+data['data'][i]['jenis_kesalahan']+'</td>';
					str+= '<td>'+data['data'][i]['description']+'</td>';
					str+= '<td>'+data['data'][i]['nama_created']+'</td>';
					var action='';
					if(data['data'][i]['status'] == '0' ){
						action = '<a href="'+base_url+'agent/approve_agent/'+data['data'][i]['id']+'" class="btn bg-light-blue waves-effect btn-xs" >Approve</a>';
					}
					str+= '<td>'+action+'</td>';
					str+= '</tr>';
				}

				$('#tbody').html(str);
				table = $('#table_sanksi').DataTable({
					"order": [],
					// "bDestroy": true
				});
			
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//
});