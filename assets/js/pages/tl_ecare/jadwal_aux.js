$(function () {
	var table =  $('#table-jadwal_report').DataTable({
		"order": []
	});

	//=======================//
	//Create select ..//
		jQuery.ajax({
			url: base_url+'cs_sdm/get_jabatan_ecare',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				$("#jabatan option[value='search']").remove();
				for (var i = 0; i < Object.keys(data['data']).length; i++) {

					var o = '<option value="'+data['data'][i]['jabatan']+'" >'+data['data'][i]['jabatan']+'</option>';
					$("#jabatan").append(o).selectpicker('refresh');
				}
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

	//=======================//
	//Create select ..	//
		$("#jabatan").change(function(){
			$.ajax({
				success: function(){
					var o = '<iframe src="http://localhost/aplikasi/adminfania/document/New folder/a.htm" width="100%" height="1000px">';
			        //$("#tampil").html(obj);
			       	$("#tampil").append(o); 
			      }
			});
		});
	//=======================//

	//=======================//
	//Setting datepicker
	$('#bs_datepicker_range_container').datepicker({
		autoclose: true,
		container: '#bs_datepicker_range_container'
	});
	//=======================//

	//=======================//
	//form upload
	$('#btn-upload_document').click(function(){
		$('#id_file').val(0);
		$("#nama_document").val('');
        $("#s_contactcenter").val('').selectpicker('refresh');

        $("#bulan").val('').selectpicker('refresh');
        $("#tahun").val('').selectpicker('refresh');
		$("#uploaded").val('');

        $('#form_upload').removeClass('hidden');
        $('#title_upload').removeClass();
        $('#title_reupload').removeClass();
        $('#title_reupload').addClass('hidden');
        
        $('#input-note').removeClass();
        $('#input-note').addClass('hidden');
    });

	$('#btn-close_form_upload').click(function(){
        $('#form_upload').addClass('hidden');
        
        $('#input-note').removeClass();
        $('#input-note').addClass('hidden');
    });
	//=======================//

})