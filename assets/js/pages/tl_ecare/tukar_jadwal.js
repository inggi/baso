$(function () {
	//=======================//
	//Bootstrap datepicker plugin//
		$('#bs_datepicker_container input').datepicker({
			autoclose: true,
			container: '#bs_datepicker_container'
		});
	//=======================//

	//=======================//
	//Create select nama agent//
		jQuery.ajax({
			url: base_url+'cs_sdm/get_karwayan_ecare',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				for (var i = 0; i < Object.keys(data['data']).length; i++) {

					var o = '<option value="'+data['data'][i]['csdm']+'" data-subtext="'+data['data'][i]['csdm']+'">'+data['data'][i]['name']+'</option>';
					$("#agent1").append(o).selectpicker('refresh');
					$("#agent2").append(o).selectpicker('refresh');
				}
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

	//=======================//
	//Create Pola//
		$('#agent1').change(function(){ create_val_pola($(this).val(), $('#tgl_ubah1').val(), 'pola1' ) });
		$('#agent2').change(function(){ create_val_pola($(this).val(), $('#tgl_ubah2').val(), 'pola2' ) });

		$('#tgl_ubah1').change(function(){ create_val_pola($('#agent1').val(), $(this).val(), 'pola1' ) });
		$('#tgl_ubah2').change(function(){ create_val_pola($('#agent2').val(), $(this).val(), 'pola2' ) });

		function create_val_pola(csdm, tgl, elm_pola){
			if($.trim(csdm) != '' && $.trim(tgl) != ''){
				jQuery.ajax({
					url: base_url+'cs_jadwal/get_jadwal_agent',
					type: 'POST',
					data: {csdm:csdm, tgl:tgl},
					dataType : 'json',
					success: function(data, textStatus, xhr) {
						if(Object.keys(data['data']).length > 0){
							var jam = '';
							if(data['data']['jam_masuk']){jam=' <small class="text-muted">('+data['data']['jam_masuk']+' - '+data['data']['jam_pulang']+')</small>'}
							$('#'+elm_pola+'_view').val(data['data']['pola']+' ('+data['data']['jam_masuk']+'-'+data['data']['jam_pulang']+')');
							$('#'+elm_pola).val(data['data']['pola']);

							if(elm_pola == 'pola1'){
								if(data['data']['pola'] == 'X'){
									$('#type_tukar').val('Tukar Libur');
								}
								else if(data['data']['pola'] == 'CT'){
									$('#type_tukar').val('Tukar Cuti');
								}
								else if(!!data['data']['jam_masuk']){
									$('#type_tukar').val('Tukar Dinas');
									$('#tgl_ubah2').val(tgl);
								}
								else{
									$('#type_tukar').val('')
								}
							}
						}
						else {
							$('#'+elm_pola).val('')
						}

					},
					error: function(xhr, textStatus, errorThrown) {
						console.log(textStatus.reponseText);
					}
				});
			}
		}
	//=======================//

	//=======================//
	//Submit Tukar Jadwal//
		$('#form-tukar-jadwal').validate({
			messages: {
				'pola1': { required: "Please select an Nama and Tanggal." },
				'pola2': { required: "Please select an Nama and Tanggal." },
				'type_tukar': { required: "Pilih Pola Yang Bisa Ditukar." },
			},
			highlight: function (input) {
				$(input).parents('.form-line').addClass('error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-line').removeClass('error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			},
			submitHandler: function(form, event){
				if($('#type_tukar').val() == 'Tukar Dinas' && $('#tgl_ubah1').val() != $('#tgl_ubah2').val()){
					alert('Untuk Tukar Dinas Tanggal User Peminta dan User Pengganti Harus Sama!');
				}
				else if ($('#type_tukar').val() == 'Tukar Dinas' && ($('#pola2').val() == 'X' || $('#pola2').val() == 'CT')){
					alert('Untuk Tukar Dinas Jadwal User Pengganti Harus Sedang Dinas!');
				}
				else if ($('#type_tukar').val() == 'Tukar Cuti' && ($('#pola2').val() != 'CT')){
					alert('Untuk Tukar Cuti Jadwal User Pengganti Harus Sedang Cuti!');
				}
				else if ($('#type_tukar').val() == 'Tukar Libur' && ($('#pola2').val() != 'X')){
					alert('Untuk Tukar Libur Jadwal User Pengganti Harus Sedang Libur!');
				}
				else{
					event.preventDefault();

					var formData = new FormData();
					formData.append('agent1', $('#agent1').val());
					formData.append('agent2', $('#agent2').val());
					formData.append('tgl_ubah1', $('#tgl_ubah1').val());
					formData.append('tgl_ubah2', $('#tgl_ubah2').val());
					formData.append('pola1', $('#pola1').val());
					formData.append('pola2', $('#pola2').val());
					formData.append('type_tukar', $('#type_tukar').val());
					formData.append('alasan', $('#alasan').val());
					formData.append('bukti', $('input[type=file]')[0].files[0]);

					jQuery.ajax({
						url: base_url+'Cs_history_tukarjadwal/do_tukarjadwal_ecare',
						type: 'POST',
						data: formData,
						// data: {agent1:agent1, agent2:agent2, tgl_ubah1:tgl_ubah1, tgl_ubah2:tgl_ubah2, pola1:pola1, pola2:pola2, alasan:alasan, type_tukar:type_tukar},
						processData:false,
						contentType:false,
						cache:false,
						async:false,
					    dataType : 'json',
						success: function(data, textStatus, xhr) {
							if (data['hasil'] == 'success') {
								$('#tgl_ubah1').val('');
								$('#tgl_ubah2').val('');
								$('#pola1').val('');
								$('#pola2').val('');
								$('#type_tukar').val('');
								$('#reaseon').val('');

								alert($('#type_tukar').val()+' Berhasil.')
							}
							else{ alert(data['hasil']); }
						},
						error: function(xhr, textStatus, errorThrown) {
							console.log(textStatus.reponseText);
						}
					});
				}
			}
		});
		// $('#form-tukar-jadwal').submit(function(event){
		// 	console.log('atata');
		// 			event.preventDefault();

		// 			jQuery.ajax({
		// 				url: base_url+'Cs_history_tukarjadwal/do_tukarjadwal',
		// 				type: 'POST',
		// 				data: new FormData(this),
		// 				// data: {agent1:agent1, agent2:agent2, tgl_ubah1:tgl_ubah1, tgl_ubah2:tgl_ubah2, pola1:pola1, pola2:pola2, alasan:alasan, type_tukar:type_tukar},
		// 				cache: false,
		// 				processData: false, 
		// 			    dataType : 'json',
		// 				success: function(data, textStatus, xhr) {
		// 					if (data['hasil'] == 'success') {
		// 						$('#tgl_ubah1').val('');
		// 						$('#tgl_ubah2').val('');
		// 						$('#pola1').val('');
		// 						$('#pola2').val('');
		// 						$('#type_tukar').val('');
		// 						$('#reaseon').val('');

		// 						alert($('#type_tukar').val()+' Berhasil.')
		// 					}
		// 					else{ alert(data['hasil']); }
		// 				},
		// 				error: function(xhr, textStatus, errorThrown) {
		// 					console.log(textStatus.reponseText);
		// 				}
		// 			});
		// })
	//=======================//

})