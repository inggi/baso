$(function () {
		//Filter karyawan//
		jQuery.ajax({
			url: base_url+'cs_pmb_flow_history/get_history_pembinaan',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				var str = '';
				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					str+= '<tr>';
					str+= '<td>'+data['data'][i]['tgl_dibuat']+'</td>';
					str+= '<td>'+data['data'][i]['tgl_kejadian']+'</td>';
					str+= '<td>'+data['data'][i]['csdm_agent']+'</td>';
					str+= '<td>'+data['data'][i]['nama_agent']+'</td>';
					str+= '<td>'+data['data'][i]['description']+'</td>';
					str+= '<td>'+data['data'][i]['nama_created']+'</td>';
					str+= '<td>'+data['data'][i]['flow_title']+'</td>';
					str+= '</tr>';
				}

				$('#tbody').html(str);
				table = $('#data_agent').DataTable({
					"order": [],
					// "bDestroy": true
				});
			
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//
});