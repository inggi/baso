$(function () {
	$('#bs_datepicker_container input').datepicker({
        autoclose: true,
        container: '#bs_datepicker_container'
    });


    //=======================//
	//Create select//
		function get_kesalahan(){
			var jenis_kesalahan = $('#jenis_kesalahan').val();
			var kategori_kesalahan = $('#kategori_kesalahan').val();
			jQuery.ajax({
				url: base_url+'cs_m_kesalahan/get_kesalahan',
				type: 'POST',
				data: {kategori_kesalahan:kategori_kesalahan,jenis_kesalahan:jenis_kesalahan},
				dataType : 'json',
				success: function(data, textStatus, xhr) {
					var o;
					for (var i = 0; i < Object.keys(data['data']).length; i++) {

						o += '<option value="'+data['data'][i]['id']+'" data-nilai="'+data['data'][i]['nilai']+'">'+data['data'][i]['description']+'</option>';
					}
					$("#id_kesalahan").html(o).selectpicker('refresh');
				},
				error: function(xhr, textStatus, errorThrown) {
					console.log(textStatus.reponseText);
				}
			});
		}
		
		$('#kategori_kesalahan').change(function(){
			get_kesalahan();
		});
	//=======================//

    //=======================//
	//Create select//
		function get_kategory_kesalahan(){
			var jenis_kesalahan = $('#jenis_kesalahan').val();
			jQuery.ajax({
				url: base_url+'cs_m_kesalahan/get_kategory_kesalahan',
				type: 'POST',
				data: {jenis:jenis_kesalahan},
				dataType : 'json',
				success: function(data, textStatus, xhr) {
					var o;
					for (var i = 0; i < Object.keys(data['data']).length; i++) {

						o += '<option value="'+data['data'][i]['category']+'" >'+data['data'][i]['category']+'</option>';
					}
					$("#kategori_kesalahan").html(o).selectpicker('refresh');
					get_kesalahan();
				},
				error: function(xhr, textStatus, errorThrown) {
					console.log(textStatus.reponseText);
				}
			});
		}
		get_kategory_kesalahan();
		$('#jenis_kesalahan').change(function(){
			get_kategory_kesalahan()
		});
	//=======================//



	$('#form_insert_sanksi').submit(function(e){
		e.preventDefault();
		var nilai = $('#id_kesalahan option:selected').data('nilai');
		// alert(nilai);
		$('#nilai').val(nilai);
		jQuery.ajax({
			url: base_url+'tl/do_insert_sanksi_from_qc',
			type: 'POST',
			data: new FormData(this),
			processData:false,
			contentType:false,
			cache:false,
			async:false,
            dataType : 'json',
			success: function(data, textStatus, xhr) {
				console.log(data['hasil']);
				if (data['hasil'] == 'success') {
					window.location = base_url+data['url'];
				}
				else{ alert(data['hasil']); }
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	});

});