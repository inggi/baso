$(function () {
	// var table =  $('#data_agent').DataTable({
	// 	"order": []
	// });

	//Filter karyawan//
		jQuery.ajax({
			url: base_url+'cs_pmb_kesalahan/get_kesalahan',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				var str = '';
				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					str+= '<tr>';
					str+= '<td>'+data['data'][i]['create_by']+'</td>';
					str+= '<td>'+data['data'][i]['tgl_kesalahan']+'</td>';
					str+= '<td>'+data['data'][i]['csdm_agent']+'</td>';
					str+= '<td>'+data['data'][i]['name']+'</td>';
					str+= '<td>'+data['data'][i]['description']+'</td>';
					str+= '<td>'+data['data'][i]['created']+'</td>';
					var action='';
					if(parseFloat(data['data'][i]['flow']) < 100 ){
						action = '<a href="'+base_url+'tl/input_pembinaan_from_qc_cho/'+data['data'][i]['id']+'" class="btn bg-light-blue waves-effect btn-xs" >Pembinaan</a>';
					}
					str+= '<td>'+action+'</td>';
					str+= '</tr>';
				}

				$('#tbody').html(str);
				table = $('#table_sanksi').DataTable({
					"order": [],
					// "bDestroy": true
				});
			
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//
});