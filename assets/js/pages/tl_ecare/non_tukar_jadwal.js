$(function () {
	//=======================//
	//Bootstrap datepicker plugin//
		$('#bs_datepicker_container input').datepicker({
			autoclose: true,
			container: '#bs_datepicker_container'
		});
	//=======================//

	//=======================//
	//Create select nama agent//
		jQuery.ajax({
			url: base_url+'cs_sdm/get_karwayan_ecare',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				for (var i = 0; i < Object.keys(data['data']).length; i++) {

					var o = '<option value="'+data['data'][i]['csdm']+'" data-subtext="'+data['data'][i]['csdm']+'">'+data['data'][i]['name']+'</option>';
					$("#agent1").append(o).selectpicker('refresh');
				}
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

	//=======================//
	//Create select pola pengganti//
		jQuery.ajax({
			url: base_url+'ca_pola_jadwal/get_pola_pengganti',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					var o = '<option value="'+data['data'][i]['pola']+'" data-subtext="'+data['data'][i]['value']+'">'+data['data'][i]['pola']+'</option>';
					$("#pola_ganti").append(o).selectpicker('refresh');
				}
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

	//=======================//
	//Create Pola//
		$('#agent1').change(function(){ create_val_pola($(this).val(), $('#tgl_ubah1').val(), 'pola1' ) });
		$('#tgl_ubah1').change(function(){ create_val_pola($('#agent1').val(), $(this).val(), 'pola1' ) });

		function create_val_pola(csdm, tgl, elm_pola){
			if($.trim(csdm) != '' && $.trim(tgl) != ''){
				jQuery.ajax({
					url: base_url+'cs_jadwal/get_jadwal_agent',
					type: 'POST',
					data: {csdm:csdm, tgl:tgl},
					dataType : 'json',
					success: function(data, textStatus, xhr) {
						if(Object.keys(data['data']).length > 0){
							$('#'+elm_pola).val(data['data']['pola']);
						}
						else {
							$('#'+elm_pola).val('')
						}

					},
					error: function(xhr, textStatus, errorThrown) {
						console.log(textStatus.reponseText);
					}
				});
			}
		}
	//=======================//

	//=======================//
	//Submit Tukar Jadwal//
		$('#form-tukar-jadwal').validate({
			messages: {
				'pola1': { required: "Please select an Nama and Tanggal." },
				'pola2': { required: "Please select an Nama and Tanggal." },
			},
			highlight: function (input) {
				$(input).parents('.form-line').addClass('error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-line').removeClass('error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			},
			submitHandler: function(form, event){
				event.preventDefault();
				var formData = new FormData();
				formData.append('agent1', $('#agent1').val());
				formData.append('tgl_ubah1', $('#tgl_ubah1').val());
				formData.append('pola1', $('#pola1').val());
				formData.append('pola_ganti', $('#pola_ganti').val());
				formData.append('alasan', $('#alasan').val());
				formData.append('bukti', $('input[type=file]')[0].files[0]);

				jQuery.ajax({
					url: base_url+'Cs_history_tukarjadwal/do_nontukarjadwal_ecare',
					type: 'POST',
					data: formData,
					// data: {agent1:agent1, tgl_ubah1:tgl_ubah1, pola1:pola1, alasan:alasan, pola_ganti:pola_ganti},
				    processData:false,
					contentType:false,
					cache:false,
					async:false,
				    dataType : 'json',
					success: function(data, textStatus, xhr) {
						if (data['hasil'] == 'success') {
							alert('Pindah Pola '+$('#pola_ganti option:selected').text()+' Berhasil.');
							$('#tgl_ubah1').val('');
							$('#tgl_ubah2').val('');
							$('#pola1').val('');
							$('#pola2').val('');
							$('#pola_ganti').val('');
							$('#alasan').val('');

						}
						else{ alert(data['hasil']); }
					},
					error: function(xhr, textStatus, errorThrown) {
						console.log(textStatus.reponseText);
					}
				});			
			}
		});
	//=======================//
})