$(function () {
	$('#form_insert_sanksi').submit(function(e){
		e.preventDefault();
		
		jQuery.ajax({
			url: base_url+'tl/do_insert_sanksi_from_qc',
			type: 'POST',
			data: new FormData(this),
			processData:false,
			contentType:false,
			cache:false,
			async:false,
            dataType : 'json',
			success: function(data, textStatus, xhr) {
				console.log(data['hasil']);
				if (data['hasil'] == 'success') {
					window.location = base_url+data['url'];
				}
				else{ alert(data['hasil']); }
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	});

});