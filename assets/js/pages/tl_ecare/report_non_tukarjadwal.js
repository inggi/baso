$(function () {
	var d = new Date();
	var table = $('#table-jadwal_report').DataTable({
        responsive: true,
    });
    
	//=======================//
	//Setting default fillter
		var year = d.getFullYear();
		$('#tahun').val(year);

		var month = d.getMonth();
		$('#month').val(month).selectpicker('refresh');
	//=======================//
	
	//=======================//
	//export excel
		$('#export_excel').click(function(){
			var month = $('#month').val(); 
			var year = $('#tahun').val();
			
			var url = base_url+'cs_history_tukarjadwal/get_xls_history_nontukarjadwal_ecare/'+month+'/'+year;
			window.open(url);
		});
	//=======================//

	//=======================//
	//Setting table jadwal agent
		setting_jadwal_agent();

		$('#form-filter_report').click(function(){
			setting_jadwal_agent();
			// alert('makan');
		});

		function setting_jadwal_agent(){
		jQuery.ajax({
			url: base_url+'cs_history_tukarjadwal/get_history_nontukarjadwal_ecare',
			type: 'POST',
			data: {month: $('#month').val(), year: $('#tahun').val()},
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				var str = '';
				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					str += '<tr>';
					str += '<td >'+data['data'][i]['tgl_awal']+'</td>';
					str += '<td scope="row" nowrap>'+data['data'][i]['nama_peminta']+'</td>';
					str += '<td >'+data['data'][i]['csdm']+'</td>';
					str += '<td >'+data['data'][i]['pola_awal']+'</td>';
					str += '<td >'+data['data'][i]['pola_pengganti']+'</td>';
					str += '<td >'+data['data'][i]['alasan']+'</td>';
					str += '<td ><a target="_blank" rel="noopener noreferrer" href="'+base_url+'uploads/'+data['data'][i]['uploads']+'">Download</a></td>';

					str += '</tr>';
				}
				
				$('#tbody_report').html('');
				//$('#tbody_report').append(str);

				table.destroy();
				$('#tbody_report').html(str);
				table = $('#table-jadwal_report').DataTable({
					"order": [],
					// "bDestroy": true
				});
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
		}
	//=======================//

})