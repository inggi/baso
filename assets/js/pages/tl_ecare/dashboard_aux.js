$(function () {
	// $('.count-to').countTo();
	console.log('tes');
	jQuery.ajax({
		url: base_url+'cs_presensi_aux/do_reset_show',
		type: 'POST',
		data: {},
		dataType : 'json',
		error: function(xhr, textStatus, errorThrown) {
			console.log(textStatus.reponseText);
		}
	});

	//Get Karyawan AUZ//
	function set_list_aux(){
		var id_aux_show = '';
		$('.list_aux').each(function(){
			if(id_aux_show!=''){id_aux_show+=', '}
			id_aux_show+= $(this).data('idaux');
		})
		
		jQuery.ajax({
			url: base_url+'cs_presensi/get_agent_ecare_login',
			type: 'POST',
			data: {id_aux_show:id_aux_show},
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				$('#fb_login').html(data['total']['fb_login']);
				$('#fb_aux').html(data['total']['fb_aux']);
				$('#tw_login').html(data['total']['tw_login']);
				$('#tw_aux').html(data['total']['tw_aux']);
				$('#va_login').html(data['total']['va_login']);
				$('#va_aux').html(data['total']['va_aux']);
				$('#ps_login').html(data['total']['ps_login']);
				$('#ps_aux').html(data['total']['ps_aux']);
				$('#ig_login').html(data['total']['ig_login']);
				$('#ig_aux').html(data['total']['ig_aux']);
				$('#email_login').html(data['total']['email_login']);
				$('#email_aux').html(data['total']['email_aux']);

				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					if(data['data'][i]['type']=='show'){
						var str = '';
						var diff = parseInt(data['data'][i]['diff']);
						var  detik = diff%60;
						var  menit = parseInt((diff%3600)/60);
						var  jam = parseInt(diff/3600);
						str += '<li class="list_aux" data-idaux="'+data['data'][i]['id_aux']+'" = id="idaux_'+data['data'][i]['id_aux']+'">';
							str += '<span class="name_agent">'+data['data'][i]['name_view']+'</span><br>';
							str += '<b>'+data['data'][i]['jenis_aux']+'</b>';
							str += '<span class="pull-right waktu_aux" data-jam="'+jam+'" data-menit="'+menit+'" data-detik="'+detik+'" data-kode_aux="'+data['data'][i]['kode_aux']+'">';
								str += addZero(jam)+':'+addZero(menit)+':'+addZero(detik)+'</span>';
						str += '</li>';
						if(data['data'][i]['jabatan'] == 'AGENT FACEBOOK'){
							$('#list_fb').append(str);
						}
						else if(data['data'][i]['jabatan'] == 'AGENT INSTAGRAM'){
							$('#list_ig').append(str);
						}
						else if(data['data'][i]['jabatan'] == 'AGENT PLAYSTORE'){
							$('#list_ps').append(str);
						}
						else if(data['data'][i]['jabatan'] == 'AGENT TWITTER'){
							$('#list_tw').append(str);
						}
						else if(data['data'][i]['jabatan'] == 'AGENT VA'){
							$('#list_va').append(str);
						}
						else if(data['data'][i]['jabatan'] == 'AGENT EMAIL'){
							$('#list_email').append(str);
						}
					}
					if(data['data'][i]['type']=='delete'){
						$('#idaux_'+data['data'][i]['id_aux']).remove();
					}
				}
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	}
	// =======================//
	var detik_reload = 0;
	setInterval(function(){
		set_list_aux();
		detik_reload += 1;
		if(detik_reload == 1800 ){ location.reload(); }
		$('.waktu_aux').each(function(index){
			var jam = parseInt($(this).data('jam'));
			var menit = parseInt($(this).data('menit'));
			var detik = parseInt($(this).data('detik'));
			if($(this).data('kode_aux') == 'aux_1' && menit >=15 ){
				$(this).addClass('red');
			}else if($(this).data('kode_aux') == 'aux_2' && menit >=15 ){
				$(this).addClass('red');
			}else if($(this).data('kode_aux') == 'aux_3' && menit >=30 ){
				$(this).addClass('red');
			}
			detik += 1;
			// console.log('ttest1: '+menit);
			$(this).data('detik', detik);
			if(detik > 60){
			// console.log('ttest2: '+menit);
				detik = 0;
				menit += 1;
				$(this).data('detik', '0');
				$(this).data('menit', menit);
				if(menit > 60 ){
			// console.log('ttest3: '+menit);
					menit = 0;
					jam += 1;
					$(this).data('menit', '0');
					$(this).data('jam', jam);
				}
			}

			var text = addZero(jam)+':'+addZero(menit)+':'+addZero(detik);
			$(this).html(text);
		});
	}, 1000);

	function addZero(angka){
		if(angka < 10 ){
			return "0"+angka;
		}
		return angka;
	}
});