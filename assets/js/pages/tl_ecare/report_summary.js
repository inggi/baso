$(function () {
	var table =  $('#table-jadwal_report').DataTable({
		"order": []
	});

	//=======================//
	//Setting datepicker
	$('#bs_datepicker_range_container').datepicker({
		autoclose: true,
		container: '#bs_datepicker_range_container'
	});
	//=======================//

	//=======================//
	//export excel
		$('#export_excel').click(function(){
			var date_start = $('#date_start').val(); 
			var date_end = $('#date_end').val(); 
			var jabatan_ecare = $('#jabatan_ecare').val();
			var status_validate = 1;

			if(date_start == ''){
				status_validate = 0;
				$('#date_start').closest('.form-line').addClass('focused error');
			}
			else { $('#date_start').closest('.form-line').removeClass('error'); }

			if(date_end == ''){
				status_validate = 0;
				$('#date_end').closest('.form-line').addClass('focused error');
			}
			else { $('#date_end').closest('.form-line').removeClass('error'); }

			if(jabatan_ecare == null){
				status_validate = 0;
				$('#jabatan_ecare').closest('.form-line').addClass('error');
			}
			else { $('#jabatan_ecare').closest('.form-line').removeClass('error'); }

			if(status_validate === 1){	
				var url = base_url+'cs_jadwal/get_xls_summary_ecare/'+date_start+'/'+date_end+'/'+jabatan_ecare;
				window.open(url);
			}
		});
	//=======================//

	//=======================//
	//Get info keterlambatan
	$('#form-filter_report').click(function(){
		var jabatan_ecare = $('#jabatan_ecare').val(); 
		var date_start = $('#date_start').val(); 
		var date_end = $('#date_end').val();
		var status_validate = 1;

		if(date_start == ''){
			status_validate = 0;
			$('#date_start').closest('.form-line').addClass('focused error');
		}
		else { $('#date_start').closest('.form-line').removeClass('error'); }

		if(date_end == ''){
			status_validate = 0;
			$('#date_end').closest('.form-line').addClass('focused error');
		}
		else { $('#date_end').closest('.form-line').removeClass('error'); }

		if(status_validate == 1 ){
			jQuery.ajax({
				url: base_url+'cs_jadwal/get_report_summary_ecare',
				type: 'POST',
				data: { jabatan_ecare:jabatan_ecare,date_start:date_start, date_end:date_end},
				dataType : 'json',
				success: function(data, textStatus, xhr) {
					var str = '';
					for (var i = 0; i < Object.keys(data['data']).length; i++) {
						str+= '<tr>';
						str+= '<td class="text-right">'+data['data'][i]['csdm']+'</td>';
						str+= '<td nowrap>'+data['data'][i]['name']+'</td>';
						if(data['data'][i]['jumlah_hari'] == null ){
							str+= '<td class="text-center" bgcolor="#d6d6d4"></td>';
							str+= '<td class="text-center info"></td>';
							//str+= '<td class="text-right">oFixed(2)+' %</td>';
							str+= '<td class="text-center" bgcolor="#b5f78f"></td>';
							str+= '<td class="text-center" bgcolor="#b5f78f"></td>';
							str+= '<td class="text-center" bgcolor="#b5f78f"></td>';
							str+= '<td class="text-center" bgcolor="#b5f78f"></td>';
							str+= '<td class="text-center info"></td>';
							str+= '<td class="text-center" bgcolor="#d6d6d4"></td>';
							str+= '<td class="text-center" bgcolor="#f6f98e"></td>';
							str+= '<td class="text-center" bgcolor="#fc7979"></td>';
							str+= '<td class="text-center" bgcolor="#caa6ed"></td>';
							str+= '<td class="text-center" bgcolor="#caa6ed"></td>';
							str+= '<td class="text-center" bgcolor="#caa6ed"></td>';
							str+= '<td class="text-center info"></td>';
							str+= '<td class="text-center" bgcolor="#6ec1c9"></td>';
							str+= '<td class="text-center" bgcolor="#ccad86"></td>';
							str+= '<td class="text-center" bgcolor="#ccad86"></td>';
							str+= '<td class="text-center" bgcolor="#ccad86"></td>';
							str+= '<td class="text-center" bgcolor="#fcc9c9"></td>';
							str+= '<td class="text-center info"></td>';
						}
						else{
							str+= '<td class="text-center" bgcolor="#d6d6d4">'+data['data'][i]['jumlah_hari']+'</td>';
							str+= '<td class="text-center info">'+data['data'][i]['hadir']+'</td>';
							//str+= '<td class="text-right">'+data['data'][i]['presentasi'].toFixed(2)+' %</td>';
							str+= '<td class="text-center" bgcolor="#b5f78f">'+data['data'][i]['CUK']+'</td>';
							str+= '<td class="text-center" bgcolor="#b5f78f">'+data['data'][i]['CUD']+'</td>';
							str+= '<td class="text-center" bgcolor="#b5f78f">'+data['data'][i]['CM']+'</td>';
							str+= '<td class="text-center" bgcolor="#b5f78f">'+data['data'][i]['CT']+'</td>';
							str+= '<td class="text-center info">'+data['data'][i]['total_cuti']+'</td>';
							str+= '<td class="text-center" bgcolor="#d6d6d4">'+data['data'][i]['L']+'</td>';
							str+= '<td class="text-center" bgcolor="#f6f98e">'+data['data'][i]['S']+'</td>';
							str+= '<td class="text-center" bgcolor="#fc7979">'+data['data'][i]['TK']+'</td>';
							str+= '<td class="text-center" bgcolor="#caa6ed">'+data['data'][i]['TD']+'</td>';
							str+= '<td class="text-center" bgcolor="#caa6ed">'+data['data'][i]['TL']+'</td>';
							str+= '<td class="text-center" bgcolor="#caa6ed">'+data['data'][i]['TC']+'</td>';
							str+= '<td class="text-center info">'+data['data'][i]['tukar_jadwal']+'</td>';
							str+= '<td class="text-center" bgcolor="#6ec1c9">'+(parseFloat(data['data'][i]['et'])/60).toFixed(0)+'</td>';
							str+= '<td class="text-center" bgcolor="#ccad86">'+data['data'][i]['SA']+'</td>';
							str+= '<td class="text-center" bgcolor="#ccad86">'+data['data'][i]['terlambat']+'</td>';
							str+= '<td class="text-center" bgcolor="#ccad86">'+data['data'][i]['akumulasi_keterlambatan']+'</td>';
							str+= '<td class="text-center" bgcolor="#fcc9c9">'+data['data'][i]['ketidakharian']+'</td>';
							str+= '<td class="text-center info">'+parseFloat(data['data'][i]['ketidakharian_presentase']).toFixed(2)+'</td>';
						}
						str+= '</tr>';
					}
					table.destroy();
					$('#tbody_report').html(str);
					table = $('#table-jadwal_report').DataTable({
						"order": [],
						// "bDestroy": true
					});
				},
				error: function(xhr, textStatus, errorThrown) {
					console.log(textStatus.reponseText);
				}
			});
		}
	});
	//=======================//
})