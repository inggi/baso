$(function () {
		//Filter karyawan//
		jQuery.ajax({
			url: base_url+'cs_sdm/get_agent_status_pembinaan',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {

				var str = '';
				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					str+= '<tr>';
					str+= '<td>'+data['data'][i]['csdm']+'</td>';
					str+= '<td>'+data['data'][i]['name']+'</td>';
					str+= '<td>'+data['data'][i]['status']+'</td>';
					str+= '<td>'+data['data'][i]['tgl_kejadian']+'</td>';
					var action = '';
					if(parseFloat(data['data'][i]['flow']) < 100 ){
						action = '<a href="'+base_url+'tl/input_pembinaan/'+data['data'][i]['id']+'" class="btn bg-light-blue waves-effect btn-xs" >Pembinaan</a>';
					}
					str+= '<td>'+action+'</td>';
					str+= '</tr>';
				}

				$('#tbody').html(str);
				table = $('#data_agent').DataTable({
					"order": [],
					// "bDestroy": true
				});
			
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//
});