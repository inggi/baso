$(function () {
	var d = new Date();

	//=======================//
	//Setting default fillter
		var year = d.getFullYear();
		$('#tahun').val(year);

		var month = d.getMonth();
		$('#month').val(month).selectpicker('refresh');
	//=======================//

	//=======================//
	//Create table kode pola jadwal//
		jQuery.ajax({
			url: base_url+'ca_pola_jadwal/get_pola_jadwal',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				var str = '';
				for (var i = 0; i < (Object.keys(data['data']).length/2).toFixed(0); i++) {
					str += '<tr>';
					str += '<td >'+(i+1)+'</td>';
					str += '<td >'+data['data'][i]['pola']+'</td>';
					str += '<td >'+data['data'][i]['jam_masuk']+'</td>';
					str += '<td >'+data['data'][i]['jam_pulang']+'</td>';
					str += '<td >'+data['data'][i]['ket']+'</td>';
					str += '</tr>';
				}
				$('#tbody_jadwal1').append(str);

				str = '';
				for (var i =(Object.keys(data['data']).length/2).toFixed(0); i < Object.keys(data['data']).length; i++) {
					str += '<tr>';
					str += '<td >'+(i+1)+'</td>';
					str += '<td >'+data['data'][i]['pola']+'</td>';
					str += '<td >'+data['data'][i]['jam_masuk']+'</td>';
					str += '<td >'+data['data'][i]['jam_pulang']+'</td>';
					str += '<td >'+data['data'][i]['ket']+'</td>';
					str += '</tr>';
				}
				$('#tbody_jadwal2').append(str);
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//
	
	//=======================//
	//Show pola jadwal
		$('#btn_show_pola').click(function(){
	        $('#pola_jadwal').removeClass('hidden');
	    });

		$('#btn-close_pola_jadwal').click(function(){
	        $('#pola_jadwal').addClass('hidden');
	    });
	//=======================//

	//=======================//
	//Setting table jadwal agent
		setting_jadwal_agent();

		$('#form-filter_jadwal').click(function(){
			setting_jadwal_agent();
			// alert('makan');
		});

		function setting_jadwal_agent(){
		jQuery.ajax({
			url: base_url+'cs_jadwal/get_jadwal_ecare',
			type: 'POST',
			data: {month: $('#month').val(), year: $('#tahun').val(), jabatan_ecare: $('#jabatan').val()},
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				var str = '';
				var str_head = '<th>Name</th><th align="right">CSDM</th>';
				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					str += '<tr>';
					str += '<td scope="row" nowrap>'+data['data'][i]['name']+'</td>';
					str += '<td align="right">'+data['data'][i]['csdm']+'</td>';

					lastd = new Date($('#tahun').val(), (parseInt($('#month').val())+1), 0);
					var p = 0;
					for (var c = 1; c <= lastd.getDate(); c++) {
						if(i==0){
							var temp_date = new Date($('#tahun').val(), ($('#month').val()), c);
							var vc = temp_date.getDay() == 0 ? "bg-red":"";
							str_head += '<td class="'+vc+'" align="center">'+c+'</td>';
						}

						var pola='';
						if(typeof(data['data'][i]['jadwal']) !== 'undefined' && typeof(data['data'][i]['jadwal'][p]) !== 'undefined'){
							if( c == parseInt( data['data'][i]['jadwal'][p]['day'] )){
								pola = data['data'][i]['jadwal'][p]['pola'];
								p++;
							}
						}
						str += '<td align="center">'+pola+'</td>';
					}
					str += '</tr>';
				}
				
				$('#head_table').html('');
				$('#head_table').append(str_head);
				
				$('#tbody_jadwal').html('');
				$('#tbody_jadwal').append(str);

			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
		}
	//=======================//

	//=======================//
	//Create select jabatan//
		jQuery.ajax({
			url: base_url+'cs_sdm/get_jabatan_ecare',
			type: 'POST',
			data: {},
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				//$("#jabatan_ecare option[value='search']").remove();
				for (var i = 0; i < Object.keys(data['data']).length; i++) {

					var o = '<option value="'+data['data'][i]['jabatan']+'" >'+data['data'][i]['jabatan']+'</option>';
					$("#jabatan").append(o).selectpicker('refresh');
				}
				setting_jadwal_agent();
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//=======================//

})