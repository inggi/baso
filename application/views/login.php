<!DOCTYPE html>
<html>
<?php
	require_once('layout/head.php');
?>
<body  background="bgedit.jpg"></body>

<body class="login-page">
	<!-- Jquery Core Js -->
	<script src="<?php echo base_url();?>assets/plugins/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core Js -->
	<script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.js"></script>

	<!-- Waves Effect Plugin Js -->
	<script src="<?php echo base_url();?>assets/plugins/node-waves/waves.js"></script>

	<!-- Custom Js -->
	<script src="<?php echo base_url();?>assets/js/admin.js"></script>
    <script type="text/javascript">
        var base_url = '<?php echo base_url()?>';
    </script>
    <script src="<?php echo base_url();?>assets/js/login.js"></script>

	<div class="login-box">
        <div style="padding-top:15px">
            <center><img src="logoinf.png" alt="" name="popcal" width="100" height="100"></center>
        </div>
        <div style="padding-top:0px">
        <center>
            <a href="javascript:void(0);"><font color="white" size="10"><b>BASO</b></font></a><br>
            <a href="javascript:void(0);"><font color="white" size="5">(Basic Info)</font></a><br><br><br>
        </center>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_in" method="POST">
                    <div class="msg">Sign in to start your session</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="username" placeholder="User" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control form-password" name="password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="row">
                       <!--  <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Remember Me</label>
                        </div> -->
                        <div class="col-xs-4">
                            <!-- <input type="submit" hidden> -->
                            <button class="btn btn-block bg-red waves-effect" type="submit" id="btn-sign_in">SIGN IN</button>
                        </div>
                    </div>
                   <!--  <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <a href="sign-up.html">Register Now!</a>
                        </div>
                        <div class="col-xs-6 align-right">
                            <a href="forgot-password.html">Forgot Password?</a>
                        </div>
                    </div> -->
                </form>
            </div>
        </div>
    </div>

    <div class="legal">
        <div class="copyright" align="center">
             <small>© 2019 Infomedia ITCC<font color="#FF0000">BDG</font></small>
        </div>
    </div>
</body>

</html>
