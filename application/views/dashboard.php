<!DOCTYPE html>
<html>

<?php require_once('layout/head.php') ?>

<body class="theme-black">

	<?php //require_once('layout/page_loader.php') ?>

	<?php require_once('layout/header_dashboard.php') ?>


	<?php require_once('pages/'.$view.'.php') ?>

	<?php require_once('layout/script.php') ?>
</body>

</html>
