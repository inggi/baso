<!-- Menu -->
<div class="menu">
	<ul class="list">
		<li class="header">MAIN NAVIGATION</li>
		<li>
			<a href="<?php echo base_url();?>tl/content">
			<!-- <a href="<?php echo base_url();?>spv/home"> -->
				<i class="material-icons">home</i>
				<span>Home</span>
			</a>
		</li>
		<li>
			<a href="javascript:void(0);" class="menu-toggle">
				<i class="material-icons">assignment</i>
				<span>Data Karyawan</span>
			</a>
			<ul class="ml-menu">
				<li>
					<a href="<?php echo base_url();?>tl/content/data_agent">Data Karyawan Underteam</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>tl/content/endkontrak">End Contract < 60D</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>tl/content/outofcontract">Out of Contract</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>tl/content/history">History</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>tl/content/view_datakaryawan">View Data Karyawan</a>
				</li>
			</ul>
		</li>
	</ul>
</div>
<!-- #Menu -->