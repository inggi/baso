<!-- Menu -->
<div class="menu">
	<ul class="list">
		<li class="header">MAIN NAVIGATION</li>
		<li>
			<a href="<?php echo base_url();?>hr/content">
				<i class="material-icons">home</i>
				<span>Home</span>
			</a>
		</li>
		<!-- <li>
			<a href="<?php echo base_url();?>hr/profile">
				<i class="material-icons">lock_open</i>
				<span>Change Password</span>
			</a>
		</li> -->
		<li>
			<a href="javascript:void(0);" class="menu-toggle">
				<i class="material-icons">assignment</i>
				<span>Data Karyawan</span>
			</a>
			<ul class="ml-menu">
				<!-- <li>
					<a href="<?php echo base_url();?>page/content/report_karyawan">Report Karyawan</a>
				</li> -->
				<li>
					<a href="<?php echo base_url();?>hr/content/data_agent">Data Karyawan</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>hr/content/input_karyawan">Input Karyawan</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>hr/content/history">History</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>hr/content/grading">Upload Grading</a>
				</li>
			</ul>
		</li>
	</ul>
</div>
<!-- #Menu -->