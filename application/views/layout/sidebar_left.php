<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
	<!-- User Info -->
	<div class="user-info">
		<div class="image">
			<img src="<?php echo base_url();?>assets/images/user.png" width="48" height="48" alt="User" />
		</div>
		<div class="info-container">
			<div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><font color='red'><b><?php echo $this->session->jabatan ?></b></font></div>
			<div style="color:white; font-size: 10px"><?php echo $this->session->name ?></div>
			<div class="btn-group user-helper-dropdown">
				<i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">input</i>
				<ul class="dropdown-menu pull-right">
					<li><a href="<?php echo base_url(); ?>login/do_log_out"><i class="material-icons">input</i>Sign Out</a></li>
			<?php if($this->session->jabatan == 'ADMIN SUPPORT' || $this->session->jabatan == 'HR SUPPORT'){ ?>
					<li><a href="<?php echo base_url();?>hr/profile"><i class="material-icons">lock_open</i>Change Password</a></li>
			<?php } ?>
					
				</ul>
			</div>
		</div>
	</div>
	<!-- #User Info -->
	<?php require_once($menu.'.php') ?>
	<?php require_once('footer.php') ?>

</aside>
<!-- #END# Left Sidebar -->