<script type="text/javascript"> 
    var base_url = '<?php echo base_url()?>';
</script>

<!-- Jquery Core Js -->
<script src="<?php echo base_url();?>assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="<?php echo base_url();?>assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="<?php echo base_url();?>assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="<?php echo base_url();?>assets/plugins/node-waves/waves.js"></script>

<!-- Bootstrap Datepicker Plugin Js -->
<script src="<?php echo base_url();?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<!-- Jquery Nprogress Plugin Css -->
<script src="<?php echo base_url();?>assets/plugins/nprogress/nprogress.js"></script>

<!-- Jquery jquery-countto -->
<script src="<?php echo base_url();?>assets/plugins/jquery-countto/jquery.countTo.js"></script>

<!-- Jquery Spinner Plugin Js -->
<script src="<?php echo base_url();?>assets/plugins/jquery-spinner/js/jquery.spinner.js"></script>

<!-- Jquery Validation Plugin Css -->
<script src="<?php echo base_url();?>assets/plugins/jquery-validation/jquery.validate.js"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="<?php echo base_url();?>assets/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    
<!-- Custom Js -->
<script src="<?php echo base_url();?>assets/js/admin.js"></script>



<?php
if (file_exists('assets/js/pages/'.$view.'.js')) {
?>
	<script src="<?php echo base_url();?>assets/js/pages/<?php echo $view; ?>.js"></script>
<?php
}
?>