<section class="content">
	<div class="container-fluid">
		<!-- Basic Examples -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							FORM INPUT SDM
							<!-- <small>Perubahan Form Update</small> -->
						</h2>
					</div>
					<div class="body">
					<form id="input_sdm_form" method="POST">
						<!-- <h2 class="card-inside-title">Profile</h2> -->
						<div class="row clearfix">
							<div class="col-sm-12">
								<div class="form-group-sm" style="visibility: hidden;">
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Nama karyawan</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="name" id="name" value=""/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">NIK HRIS</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="nik_hris" id="nik_hris" value=""/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Gender</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<select class="form-control show-tick update_sdm" data-live-search="true" data-size="6" id="gender" name="gender">
											<option value=''>-- Pilih --</option>
											<option value='PEREMPUAN'>PEREMPUAN</option>
											<option value='LAKI-LAKI'>LAKI-LAKI</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">NIK CSDM</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="number" class="form-control update_sdm" name="csdm" id="csdm" value="" required />
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Agama</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="agama" id="agama" value=""/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Batch</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="batch" id="batch" value=""/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">PERNER</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="perner" id="perner" value=""/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Nama Online</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="nama_online" id="nama_online" value=""/>
									</div>
								</div>
							</div>	
							<div class="col-sm-2">
								<label class="form-label">Jabatan</label>
							</div>
							<div class="col-sm-8">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="hidden" class="form-control update_sdm" name="temp_jabatan" id="temp_jabatan" value=""/>
										<select class="form-control show-tick update_sdm" data-live-search="true" data-size="6" id="jabatan" name="jabatan">
											<option value='0' disabled>-- Pilih jabatan --</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
									<button type="button" class="btn bg-blue btn-xs" data-toggle="modal" data-target="#modaltambahjabatan">Tambah</button>&nbsp;
									<button type="button" class="btn bg-red btn-xs" data-toggle="modal" data-target="#modalhapusjabatan" id="button_modal_hapus_jabatan">Hapus</button>
							</div>
						</div>
						<div class="row clearfix">
							<div class="col-sm-2">
								<label class="form-label">Jabatan Level</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<select class="form-control show-tick update_sdm" data-live-search="true" data-size="6" id="jabatan_level" name="jabatan_level" required>
											<option value='0' disabled>-- Pilih Jabatan Level --</option>
											<option value='STAFF'>Staff</option>
											<option value='ROSTER'>Roster</option>
											<option value='TL'>Team Leader</option>
											<option value='SPV'>Supervisor</option>
											<option value='ADMIN'>Admin</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Unit</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="hidden" class="form-control update_sdm" name="temp_unit" id="temp_unit" value=""/>
										<select class="form-control show-tick update_sdm" data-live-search="true" data-size="6" id="unit" name="unit" required>
											<option value='0' disabled>-- Pilih Unit --</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">CTP</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="hidden" class="form-control update_sdm" name="temp_ctp" id="temp_ctp" value=""/>
										<select class="form-control show-tick update_sdm" data-live-search="true" data-size="6" id="ctp" name="ctp" required>
											<option value='0' disabled>-- Pilih CTP --</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Skill Layanan</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="skill_layanan" id="skill_layanan" value=""/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Team Leader</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="hidden" class="form-control update_sdm" name="temp_tl" id="temp_tl" value=""/>
										<select class="form-control show-tick update_sdm" data-live-search="true" data-size="6" id="id_tl" name="id_tl">
											<option value='0' disabled>-- Pilih Team Leader --</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Supervisor</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="hidden" class="form-control update_sdm" name="temp_spv" id="temp_spv" value=""/>
										<select class="form-control show-tick update_sdm" data-live-search="true" data-size="6" id="id_spv" name="id_spv">
											<option value='0' disabled>-- Pilih Supervisor --</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Skema Agent</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="skema_agent" id="skema_agent" value=""/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Status Pernikahan</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<select class="form-control show-tick update_sdm" data-live-search="true" data-size="6" id="status_menikah" name="status_menikah">
											<option value=''>-- Pilih --</option>
											<option value='MENIKAH'>MENIKAH</option>
											<option value='BELUM MENIKAH'>BELUM MENIKAH</option>
											<option value='DUDA'>DUDA</option>
											<option value='JANDA'>JANDA</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Tempat Lahir</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="tempat_lahir" id="tempat_lahir" value=""/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Tanggal Lahir</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line" id="bs_datepicker_container">
										<input type="text" data-date-format="yyyy-mm-dd" class="form-control update_sdm" name="tgl_lahir" id="tgl_lahir" value=""/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Usia</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="usia" id="usia" value="" readonly="" />
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Tanggal Awal Kontrak</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line" id="bs_datepicker_container">
										<input type="text" data-date-format="yyyy-mm-dd" class="form-control update_sdm" name="tgl_awal_kontrak" id="tgl_awal_kontrak" value=""/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Join di Insani</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line" id="bs_datepicker_container">
										<input type="text" data-date-format="yyyy-mm-dd" class="form-control update_sdm" name="join_insani" id="join_insani" value=""/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Update LOS</label>
							</div>	
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" data-date-format="yyyy-mm-dd" class="form-control update_sdm" name="update_los" id="update_los" value="<?php echo date('yy-m-d');?>" readonly/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">LOS</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="los" id="los" value="" readonly/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Kode LOS</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="kode_los" id="kode_los" value="" readonly/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Tanggal Akhir Kontrak</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line" id="bs_datepicker_container">
										<input type="text" data-date-format="yyyy-mm-dd" class="form-control update_sdm" name="tgl_akhir_kontrak" id="tgl_akhir_kontrak" value=""/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">PPJP</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="ppjp" id="ppjp" value=""/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">No. HP</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="no_hp" id="no_hp" value=""/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Join Unit</label>
							</div>	
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line" id="bs_datepicker_container">
										<input type="text" data-date-format="yyyy-mm-dd" class="form-control update_sdm" name="join_unit" id="join_unit" value=""/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">LOS di Unit</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="los_unit" id="los_unit" value="" readonly/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Nama Email</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="email" id="email" value=""/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Pendidikan</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="pendidikan" id="pendidikan" value=""/>
									</div>
								</div>
							</div>	
							<div class="col-sm-2">
								<label class="form-label">BPJS TK</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="bpjs_tk" id="bpjs_tk" value=""/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">BPJS Kesehatan</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="bpjs_kesehatan" id="bpjs_kesehatan" value=""/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">NPWP</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="npwp" id="npwp" value=""/>
									</div>
								</div>
							</div>				
						</div>
						<!-- <div class="row clearfix">
							<div class="col-sm-2">
								<label class="form-label">Status Pekerja</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="status_pekerja" id="status_pekerja" value=""/>
									</div>
								</div>
							</div>
						</div> -->
						<h2 class="card-inside-title">File Pendukung</h2>
						<div class="row clearfix">
							<div class="col-sm-12">
								<div class="form-group-sm" style="visibility: hidden;">
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">File 1</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="file" id="file_pendukung1" class="form-control" placeholder="" name="file_pendukung1">
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">File 2</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="file" id="file_pendukung2" class="form-control" placeholder="" name="file_pendukung2">
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">File 3</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="file" id="file_pendukung3" class="form-control" placeholder="" name="file_pendukung3">
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">File 4</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="file" id="file_pendukung4" class="form-control" placeholder="" name="file_pendukung4">
									</div>
								</div>
							</div>
						</div>

						<div class="row clearfix">
							<div class="col-sm-12">
								<!-- <input type="hidden" class="form-control update_sdm" name="id_pmb_kesalahan" id="id_pmb_kesalahan" value="0" />
								 -->
								<input type="hidden" class="form-control" name="session_ctp" id="session_ctp" value="<?php echo $this->session->ctp ?>" readonly/>
								<button type="submit" class="btn btn-primary m-t-15 waves-effect" id="button_submit"><i class="material-icons">file_upload</i> SUBMIT</button>
							</div>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->
	</div>
</section>
<div class="alert alert-success alert-dismissible alertify-logs hidden" role="alert" id="success-update">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div id='message'>
    	Berhasil simpan perubahan.
    </div>
</div>

<div class="modal fade" id="modaltambahjabatan" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="smallModalLabel">Form Tambah Nama Jabatan</h4>
            </div>
            <div class="modal-body" id="smallModalContent">
                <input type="text" class="form-control" id="nama_jabatan_baru" name="nama_jabatan_baru" placeholder="Jabatan">
            </div>
            <div class="modal-footer">
            	<button type="button" class="btn btn-link waves-effect" id="save_jabatan">SAVE</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalhapusjabatan" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm2" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="smallModalLabel">Form Hapus Jabatan</h4>
            </div>
            <div class="modal-body" id="smallModalContent">
                <div class="row">
					<div class="table-responsive">
						<table class="table table-striped" id="table_jabatan">
							<thead>
								<tr>
									<th>Nama Jabatan</th>
									<th>action</th>
								</tr>
							</thead>
							<tbody id="listnamajabatan">
							</tbody>
						</table>
					</div>
				</div>
            </div>
            <div class="modal-footer">
            	<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>