<section class="content">
	<div class="container-fluid">
		<div class="row clearfix hidden" id="form-upload">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>UPLOAD KARYAWAN</b>
						</h2>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown" id="close_upload" style=" cursor: pointer;">
								<i class="material-icons">close</i>
							</li>
						</ul>
					</div>
					<div class="body">
						<form class="form-horizontal" id="do_upload">
							<div class="row">
								<div class="col-lg-2 col-md-2 form-control-label">
									<div class="form-group">
											<label for="password_2">File</label>
									</div>
								</div>
								<div class="col-lg-4 col-md-4">
									<div class="form-group">
										<div class="form-line">
											<input type="file" id="file_upload" class="form-control" placeholder="" name="file_upload">
										</div>
										<div class="help-info" style="color:red">*Ket: Format file harus sama dengan contoh yang disediakan</div>
									</div>
								</div>
								<div class="col-lg-2 col-md-2 hidden">
									<div class="form-group">
										<div class="input-group spinner" data-trigger="spinner">
											<div class="form-line">
												<input type="text" class="form-control text-center" value="1" data-rule="quantity" id="sheet" nama="sheet" data-max="10">
											</div>
											<div class="help-info">Jumlah Sheet</div>
											<span class="input-group-addon">
	                                            <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
	                                            <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
	                                        </span>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
									<button type="submit" class="btn btn-primary m-t-15 waves-effect" id="button_doupload"><i class="material-icons">file_upload</i> SUBMIT</button>

									<a href="<?php echo base_url(); ?>document/Format_Upload_Jadwal.xls" class="btn btn-success m-t-15 waves-effect" id="a_doupload"><i class="material-icons">file_download</i> Format Excel</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- Filter -->
		<div class="row clearfix hidden" id="form-filter">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>FILTER KARYAWAN</b>
						</h2>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown" id="close_filter" style=" cursor: pointer;">
								<i class="material-icons">close</i>
							</li>
						</ul>
					</div>

					<div class="body">
						<form>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" data-live-search="true" data-size="6" id="unit">
												<option value='0' disabled selected>-- Pilih Unit --</option>
												<option value='All'>All</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<button type="button" class="btn btn-primary waves-effect" id="form-filter_report"><i class="material-icons">filter_list</i> <span>FILTER</span></button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->


		<!-- Basic Examples -->
		<div class="row clearfix" id="jadwal">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>DATA KARWAYAN</b>
						</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);" id="show_upload">Upload</a></li>
                                    <li><a href="javascript:void(0);" id="show_filter">Filter</a></li>
                                </ul>
                            </li>
                        </ul>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table id="table-pegawai" class="table table-striped table-bordered table-bordered">
								<thead>
									<tr id="head_table">
										<th class="success">CSDM</th>
										<th class="success">Name</th>
										<th class="success">Unit</th>
										<th class="success">Jabatan</th>
										<th class="success">Level Akses</th>
										<th class="success">TL</th>
										<th class="success">SPV</th>
										<th class="success">Action</th>
									</tr>
								</thead>
								<tbody id="tbody">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->
	</div>
</section>
