<section class="content">
	<div class="container-fluid">
		
		<!-- Filter -->
		<!-- <div class="row clearfix show" id="form-filter">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>FILTER</b>
						</h2>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown" id="close_filter" style=" cursor: pointer;">
								<i class="material-icons">close</i>
							</li>
						</ul>
					</div>

					<div class="body">
						<form>
							<div class="row clearfix">
								<div class="input-daterange" id="bs_datepicker_range_container">
									<div class="col-sm-3">
										<div class="form-group form-float form-group-sm">
											<div class="form-line focused"> 
												<input type="text" class="form-control" name="date_start" id="date_start"/>
												<label class="form-label">Update (From)</label>
											</div>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group form-float form-group-sm">
											<div class="form-line focused"> 
												<input type="text" class="form-control" name="date_end" id="date_end"/>
												<label class="form-label">Update (To)</label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group form-float form-group-sm">
										<div class="form-line">
											<select class="form-control show-tick" data-live-search="true" id="unit" name="unit">
												<option value='0' disabled>-- Pilih Unit --</option>
												<option value='All' selected>All</option>
											</select>
											<label class="form-label">Unit</label>
										</div>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group form-float form-group-sm">
										<div class="form-line">
											<input type="hidden" class="form-control update_sdm" name="temp_ctp" id="temp_ctp" value=""/>
											<select class="form-control show-tick" data-live-search="true" data-size="6" id="ctp" name="ctp">
												<option value='0' disabled>-- Pilih CTP --</option>
												<option value='All' selected>All</option>
											</select>
											<label class="form-label">CTP</label>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<button type="button" class="btn btn-primary waves-effect" id="form-filter_report"><i class="material-icons">filter_list</i> <span>FILTER</span></button>
									<button type="button" class="btn btn-success btn-lg m-l-15 waves-effect" id="export">EXPORT</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div> -->
		<!-- #END# Basic Examples -->

		<!-- Basic Examples -->
		<div class="row clearfix" id="jadwal">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>LIST END CONTRACT < 60D</b>
						</h2>
                        <ul class="header-dropdown m-r--5">
                            <!-- <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);" id="show_upload">Upload</a></li> 
                                    <li><a href="javascript:void(0);" id="show_filter">Filter</a></li> 
                                </ul>
                            </li> -->
                        </ul>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table id="table_pegawai" class="table table-striped table-bordered table-bordered">
								<thead>
									<tr id="head_table" height="40px">
										<th class="success">CSDM</th>
										<th class="success">Name</th>
										<th class="success">Jabatan</th>
										<th class="success">Start Contract</th>
										<th class="success">End Contract</th>
										<th class="success">Grade</th>
										<th class="success">Date Uploaded Grade</th>
										<th class="success" width="2">Action</th>
									</tr>
								</thead>
								<tbody id="tbody">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->
	</div>
</section>

<div class="modal fade" id="modalperpanjangkontrak" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="smallModalLabel">YAKIN PERPANJANG KONTRAK ?</h4>
            </div>
            <div class="modal-body" id="smallModalContent">
            	<div class="col-lg-12">
					<div class="form-group">
						<div class="form-line">
							<input type="text" class="form-control" name="name" id="name" readonly/>
						</div>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="form-group">
						<div class="form-line">
							<input type="text" class="form-control" name="gradingmodal" id="gradingmodal" readonly/>
						</div>
					</div>
				</div>
				<input type="hidden" class="form-control" id="id_sdm" name="id_sdm">   
				<input type="hidden" class="form-control" id="grading" name="grading">   
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect btn bg-red" data-dismiss="modal">CANCEL</button>
            	<button type="button" class="btn btn-link waves-effect btn bg-light-blue" id="save_kontrak">YA</button>
            	<!-- <button type="button" class="btn btn-link waves-effect btn bg-green" id="tesi">YA</button> -->
            </div>
        </div>
    </div>
</div>