<section class="content">
		<div class="container-fluid">
			<!-- Counter Examples -->
			<div class="row clearfix">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h3>
						Contract
					</h3>	
					<div class="row">
						<!-- <a href="<?php echo base_url();?>hr/content/endkontrak">
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
								<div class="info-box-3 bg-deep-orange hover-zoom-effect" >
									<div class="icon">
										<i class="material-icons">assignment_ind</i>
									</div>
									<div class="content">
										<div class="text">End Contract < 30 Day</div>
										<div class="number"><?php echo $data['end_kontrak']; ?></div>
									</div>
								</div>
							</div>
						</a> -->
						<a href="<?php echo base_url();?>hr/content/endkontrak">
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
								<div class="info-box-3 bg-deep-orange hover-zoom-effect" >
									<div class="icon">
										<i class="material-icons">assignment_ind</i>
									</div>
									<div class="content">
										<div class="text">End Contract < 60 Day</div>
										<div class="number"><?php echo $data['end_kontrak60']; ?></div>
									</div>
								</div>
							</div>
						</a>
						<a href="<?php echo base_url();?>hr/content/outofcontract">
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
								<div class="info-box-3 bg-orange hover-zoom-effect" >
									<div class="icon">
										<i class="material-icons">assignment_late</i>
									</div>
									<div class="content">
										<div class="text"><!-- (<?php echo $this->session->ctp; ?>)  -->Contract is Out of Date</div>
										<div class="number"><?php echo $data['outofkontrak']; ?></div>
									</div>
								</div>
							</div>
						</a>
						<!-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-amber hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">assignment_late</i>
								</div>
								<div class="content">
									<div class="text">(OBC) Contract is Out of Date</div>
									<div class="number"><?php echo $data['outofdateobc']; ?></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-yellow hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">assignment_late</i>
								</div>
								<div class="content">
									<div class="text">(ECARE) Contract is Out of Date</div>
									<div class="number"><?php echo $data['outofdateecare']; ?></div>
								</div>
							</div>
						</div> -->
						<!-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-lime hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">assignment_late</i>
								</div>
								<div class="content">
									<div class="text">(ITCC) Out of Contract</div>
									<div class="number"><?php echo $data['outofdateeit']; ?></div>
								</div>
							</div>
						</div> -->
					</div>
				</div>
			</div>
			<?php if($this->session->ctp == 'IBC'){ ?>
			<div class="row hidden">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h3>
						Waiting Action
					</h3>	
					<div class="row">
						<a href="<?php echo base_url();?>hr/content/action1/3">
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
								<div class="info-box-3 bg-green hover-zoom-effect" >
									<div class="icon">
										<i class="material-icons">assignment_late</i>
									</div>
									<div class="content">
										<div class="text">Rekomendasi SPV/TL</div>
										<div class="number"><?php echo $data['rekomendasi_tlspv']; ?></div>
									</div>
								</div>
							</div>
						</a>
						<a href="<?php echo base_url();?>hr/content/action1/1">
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
								<div class="info-box-3 bg-indigo hover-zoom-effect" >
									<div class="icon">
										<i class="material-icons">assignment_late</i>
									</div>
									<div class="content">
										<div class="text">Resign On The Spot</div>
										<div class="number"><?php echo $data['onthespot']; ?></div>
									</div>
								</div>
							</div>
						</a>
						<a href="<?php echo base_url();?>hr/content/action1/2">
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
								<div class="info-box-3 bg-teal hover-zoom-effect" >
									<div class="icon">
										<i class="material-icons">assignment_late</i>
									</div>
									<div class="content">
										<div class="text">Resign One Month Notice</div>
										<div class="number"><?php echo $data['onemonthnotice']; ?></div>
									</div>
								</div>
							</div>
						</a>
						<a href="<?php echo base_url();?>hr/content/action1/4">
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
								<div class="info-box-3 bg-blue hover-zoom-effect" >
									<div class="icon">
										<i class="material-icons">done_all</i>
									</div>
									<div class="content">
										<div class="text">Cuti Melahirkan</div>
										<div class="number"><?php echo $data['cuti_m']; ?></div>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
			<?php } ?>

			<div class="row clearfix hidden">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h3>
						Cuti Melahirkan
					</h3>	
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-blue hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">assignment_ind</i>
								</div>
								<div class="content">
									<div class="text">Cuti Melahirkan</div>
									<div class="number"><?php echo $data['cuti_m']; ?></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row clearfix">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h3>
						Total Karyawan by Jabatan
					</h3>	
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-green hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">assignment_ind</i>
								</div>
								<div class="content">
									<div class="text">TOTAL KARYAWAN</div>
									<div class="number"><?php echo $data['total_karyawan']; ?></div>
								</div>
							</div>
						</div>
						<div id="homee">
						</div>
					</div>
				</div>
			</div>
			
			<!-- <div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h3>
						Jumlah Agent Terkena Sanksi
						<small>Jumlah Agent Terkena Sanksi</small>
					</h3>
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-indigo hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">assignment_turned_in</i>
								</div>
								<div class="content">
									<div class="text">Coaching</div>
									<div class="number"><?php echo $data['agent_coaching']; ?></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-blue hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">assignment</i>
								</div>
								<div class="content">
									<div class="text">Konseling</div>
									<div class="number"><?php echo $data['agent_konseling']; ?></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-light-blue hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">assignment_ind</i>
								</div>
								<div class="content">
									<div class="text">Berita Acara Temuan Langsung</div>
									<div class="number"><?php echo $data['agent_BATL']; ?></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" > 
							<div class="info-box-3 bg-cyan hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">assignment_late</i>
								</div>
								<div class="content">
									<div class="text">Surat Peringatan</div>
									<div class="number"><?php echo $data['agent_SP']; ?></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h3>
						Jumlah Team Leader Terkena Sanksi
						<small>Jumlah Agent Terkena Sanksi</small>
					</h3>
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-teal hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">assignment_turned_in</i>
								</div>
								<div class="content">
									<div class="text">Coaching</div>
									<div class="number"><?php echo $data['tl_coaching']; ?></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-green hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">assignment</i>
								</div>
								<div class="content">
									<div class="text">Konseling</div>
									<div class="number"><?php echo $data['tl_konseling']; ?></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-light-green hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">assignment_ind</i>
								</div>
								<div class="content">
									<div class="text">Berita Acara Temuan Langsung</div>
									<div class="number"><?php echo $data['tl_BATL']; ?></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-lime hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">assignment_late</i>
								</div>
								<div class="content">
									<div class="text">Surat Peringatan</div>
									<div class="number"><?php echo $data['tl_SP']; ?></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h3>
						Waiting Action
						<small>Jumlah Agent Terkena Sanksi</small>
					</h3>
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-yellow hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">access_alarm</i>
								</div>
								<div class="content">
									<div class="text">OUT TIME</div>
									<div class="number"><?php echo $data['out_time']; ?></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-amber hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">done_all</i>
								</div>
								<div class="content">
									<div class="text">WAITING APPROVAL</div>
									<div class="number"><?php echo $data['approve']; ?></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-orange hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">account_box</i>
								</div>
								<div class="content">
									<div class="text">WAITING QC</div>
									<div class="number"><?php echo $data['qc']; ?></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-deep-orange hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">account_box</i>
								</div>
								<div class="content">
									<div class="text">WAITING CHO</div>
									<div class="number"><?php echo $data['ch']; ?></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> -->

		<!-- Basic Examples -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>DATA KARYAWAN</b>
						</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <!-- <li><a href="javascript:void(0);" id="show_upload">Upload</a></li> -->
                                    <li><a href="javascript:void(0);" id="show_filter">Filter</a></li>
                                </ul>
                            </li>
                        </ul>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table id="table-pegawai" class="table table-striped table-bordered table-bordered">
								<thead>
									<tr id="head_table" height="40px">
										<th class="success">Unit</th>
										<th class="success">Kode LOS</th>
										<th class="success">Total</th>
										<!-- <th class="success">LOS</th>
										<th class="success">Kode LOS</th>
										<th class="success">Start Contract</th>
										<th class="success">End Contract</th> -->
									</tr>
								</thead>
								<tbody id="tbody">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->

		</div>
	</section>