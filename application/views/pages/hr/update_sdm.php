<section class="content">
	<div class="container-fluid">
		<!-- Basic Examples -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							FORM UPDATE (<?php echo $data['name'];?> - <?php echo $data['csdm'];?>)
							<!-- <small>Perubahan Form Update</small> -->
						</h2>
						
					</div>
					<div class="body">
					<form id="update_sdm_form" method="POST">
						<!-- <h2 class="card-inside-title">Profile</h2> -->
						<input type="hidden" class="form-control update_sdm" name="id_sdm" id="id_sdm" value="<?php echo $data['id'];?>"/>
						<div class="row clearfix">
							<div class="col-sm-12">
								<div class="form-group-sm" style="visibility: hidden;">
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Nama karyawan</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="name" id="name" value="<?php echo $data['name'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">NIK HRIS</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="nik_hris" id="nik_hris" value="<?php echo $data['nik_hris'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Gender</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<select class="form-control show-tick update_sdm" data-live-search="true" data-size="6" id="gender" name="gender">
											<option value='0' disabled>-- Pilih Gander --</option>
											<option value='PEREMPUAN' <?php echo $data['gender']=='PEREMPUAN'?'selected':'';?>>PEREMPUAN</option>
											<option value='LAKI-LAKI' <?php echo $data['gender']=='LAKI-LAKI'?'selected':'';?>>LAKI-LAKI</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">NIK CSDM</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="number" class="form-control update_sdm" name="csdm" id="csdm" value="<?php echo $data['csdm'];?>" required />
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Agama</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="agama" id="agama" value="<?php echo $data['agama'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Batch</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="batch" id="batch" value="<?php echo $data['batch'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">PERNER</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="perner" id="perner" value="<?php echo $data['perner'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Nama Online</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="nama_online" id="nama_online" value="<?php echo $data['nama_online'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Jabatan</label>
							</div>
							<div class="col-sm-8">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="hidden" class="form-control update_sdm" name="temp_jabatan" id="temp_jabatan" value="<?php echo $data['jabatan'];?>"/>
										<select class="form-control show-tick update_sdm" data-live-search="true" data-size="6" id="jabatan" name="jabatan">
											<option value='0' disabled>-- Pilih jabatan --</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
									<button type="button" class="btn bg-blue btn-xs" data-toggle="modal" data-target="#modaltambahjabatan">Tambah</button>&nbsp;
									<button type="button" class="btn bg-red btn-xs" data-toggle="modal" data-target="#modalhapusjabatan" id="button_modal_hapus_jabatan">Hapus</button>
							</div>
						</div>
						<div class="row clearfix">
							<div class="col-sm-2">
								<label class="form-label">Jabatan Level</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<select class="form-control show-tick update_sdm" data-live-search="true" data-size="6" id="jabatan_level" name="jabatan_level" required>
											<option value='0' disabled>-- Pilih Jabatan Level --</option>
											<option value='STAFF' <?php echo $data['jabatan_level']=='STAFF'?'selected':'';?>>Staff</option>
											<option value='ROSTER' <?php echo $data['jabatan_level']=='ROSTER'?'selected':'';?>>Roster</option>
											<option value='TL' <?php echo $data['jabatan_level']=='TL'?'selected':'';?>>Team Leader</option>
											<option value='SPV' <?php echo $data['jabatan_level']=='SPV'?'selected':'';?>>Supervisor</option>
											<option value='ADMIN' <?php echo $data['jabatan_level']=='ADMIN'?'selected':'';?>>Admin</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Unit</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="hidden" class="form-control update_sdm" name="temp_unit" id="temp_unit" value="<?php echo $data['unit'];?>"/>
										<select class="form-control show-tick update_sdm" data-live-search="true" data-size="6" id="unit" name="unit" required>
											<option value='0' disabled>-- Pilih Unit --</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">CTP</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="hidden" class="form-control update_sdm" name="temp_ctp" id="temp_ctp" value="<?php echo $data['ctp'];?>"/>
										<select class="form-control show-tick update_sdm" data-live-search="true" data-size="6" id="ctp" name="ctp" required>
											<option value='0' disabled>-- Pilih CTP --</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Skill Layanan</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="skill_layanan" id="skill_layanan" value="<?php echo $data['skill_layanan'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Team Leader</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="hidden" class="form-control update_sdm" name="temp_tl" id="temp_tl" value="<?php echo $data['id_tl'];?>"/>
										<select class="form-control show-tick update_sdm" data-live-search="true" data-size="6" id="id_tl" name="id_tl">
											<option value='0' disabled>-- Pilih Team Leader --</option>
											<option value="0">No TL</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Supervisor</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="hidden" class="form-control update_sdm" name="temp_spv" id="temp_spv" value="<?php echo $data['id_spv'];?>"/>
										<select class="form-control show-tick update_sdm" data-live-search="true" data-size="6" id="id_spv" name="id_spv">
											<option value='0' disabled>-- Pilih Supervisor --</option>
											<option value="">No SPV</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Skema Agent</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="skema_agent" id="skema_agent" value="<?php echo $data['skema_agent'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Status Pernikahan</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<select class="form-control show-tick update_sdm" data-live-search="true" data-size="6" id="status_menikah" name="status_menikah">
											<option value=''>-- Pilih --</option>
											<option value='MENIKAH' <?php echo $data['status_menikah']=='MENIKAH'?'selected':'';?>>MENIKAH</option>
											<option value='BELUM MENIKAH' <?php echo $data['status_menikah']=='BELUM MENIKAH'?'selected':'';?>>BELUM MENIKAH</option>

											<option value='DUDA' <?php echo $data['status_menikah']=='DUDA'?'selected':'';?>>DUDA</option>
											<option value='JANDA' <?php echo $data['status_menikah']=='JANDA'?'selected':'';?>>JANDA</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Tempat Lahir</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="tempat_lahir" id="tempat_lahir" value="<?php echo $data['tempat_lahir'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Tanggal Lahir</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line" id="bs_datepicker_container">
										<input type="text" data-date-format="yyyy-mm-dd" class="form-control update_sdm" name="tgl_lahir" id="tgl_lahir" value="<?php echo $data['tgl_lahir'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Usia</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="usia" id="usia" value="<?php echo $data['usia'];?>" readonly="" />
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Tanggal Awal Kontrak</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line" id="bs_datepicker_container">
										<input type="text" data-date-format="yyyy-mm-dd" class="form-control update_sdm" name="tgl_awal_kontrak" id="tgl_awal_kontrak" value="<?php echo $data['tgl_awal_kontrak'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Join di Insani</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line" id="bs_datepicker_container">
										<input type="text" data-date-format="yyyy-mm-dd" class="form-control update_sdm" name="join_insani" id="join_insani" value="<?php echo $data['join_insani'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Update LOS</label>
							</div>	
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" data-date-format="yyyy-mm-dd" class="form-control update_sdm" name="update_los" id="update_los" value="<?php echo date('yy-m-d');?>" readonly/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">LOS</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="los" id="los" value="<?php echo $data['los'];?>" readonly/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Kode LOS</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="kode_los" id="kode_los" value="<?php echo $data['kode_los'];?>" readonly/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Tanggal Akhir Kontrak</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line" id="bs_datepicker_container">
										<input type="text" data-date-format="yyyy-mm-dd" class="form-control update_sdm" name="tgl_akhir_kontrak" id="tgl_akhir_kontrak" value="<?php echo $data['tgl_akhir_kontrak'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">PPJP</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="ppjp" id="ppjp" value="<?php echo $data['ppjp'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">No. HP</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="no_hp" id="no_hp" value="<?php echo $data['no_hp'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Join Unit</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line" id="bs_datepicker_container">
										<input type="text" data-date-format="yyyy-mm-dd" class="form-control update_sdm" name="join_unit" id="join_unit" value="<?php echo $data['join_unit'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">LOS di Unit</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="los_unit" id="los_unit" value="<?php echo $data['los_unit'];?>" readonly/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Nama Email</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="email" id="email" value="<?php echo $data['email'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Pendidikan</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="pendidikan" id="pendidikan" value="<?php echo $data['pendidikan'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">BPJS TK</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="bpjs_tk" id="bpjs_tk" value="<?php echo $data['bpjs_tk'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">BPJS Kesehatan</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="bpjs_kesehatan" id="bpjs_kesehatan" value="<?php echo $data['bpjs_kesehatan'];?>"/>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">NPWP</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="npwp" id="npwp" value="<?php echo $data['npwp'];?>"/>
									</div>
								</div>
							</div>	
						</div>
						<!-- <div class="row clearfix">
							<div class="col-sm-2">
								<label class="form-label">Status Pekerja</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="status_pekerja" id="status_pekerja" value="<?php //echo $data['status_pekerja'];?>"/>
									</div>
								</div>
							</div>
						</div> -->
						<h2 class="card-inside-title">File Pendukung</h2>
						<div class="row clearfix">
							<div class="col-sm-12">
								<div class="form-group-sm" style="visibility: hidden;">
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">File 1</label>
							</div>
							<div class="col-sm-6">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="hidden" id="file_pendukung1" name="file_pendukung1" value="<?php echo $data['file_pendukung1'];?>" readonly>
										<p id="file_pendukung1" name="file_pendukung1">
											<a href="<?php echo base_url();?>document/<?php echo $data['csdm'];?>/<?php echo $data['file_pendukung1'];?> "><?php echo $data['file_pendukung1'];?></a>
										</p>
									</div>
								</div>
							</div>
							<div class="col-sm-1">
									Reupload
							</div>
							<div class="col-sm-3">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="file" id="file_reupload1" class="form-control" placeholder="" name="file_reupload1">
									</div>
								</div>
							</div>
						</div>
						<div class="row clearfix">
							<div class="col-sm-2">
								<label class="form-label">File 2</label>
							</div>
							<div class="col-sm-6">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="hidden" id="file_pendukung2" name="file_pendukung2" value="<?php echo $data['file_pendukung2'];?>" readonly>
										<p id="file_pendukung2" name="file_pendukung2">
											<a href="<?php echo base_url();?>document/<?php echo $data['csdm'];?>/<?php echo $data['file_pendukung2'];?> "><?php echo $data['file_pendukung2'];?></a>
										</p>
									</div>
								</div>
							</div>
							<div class="col-sm-1">
									Reupload
							</div>
							<div class="col-sm-3">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="file" id="file_reupload2" class="form-control" placeholder="" name="file_reupload2">
									</div>
								</div>
							</div>
						</div>
						<div class="row clearfix">
							<div class="col-sm-2">
								<label class="form-label">File 3</label>
							</div>
							<div class="col-sm-6">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="hidden" id="file_pendukung2" name="file_pendukung3" value="<?php echo $data['file_pendukung3'];?>" readonly>
										<p id="file_pendukung3" name="file_pendukung3">
											<a href="<?php echo base_url();?>document/<?php echo $data['csdm'];?>/<?php echo $data['file_pendukung3'];?> "><?php echo $data['file_pendukung3'];?></a>
										</p>
									</div>
								</div>
							</div>
							<div class="col-sm-1">
									Reupload
							</div>
							<div class="col-sm-3">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="file" id="file_reupload3" class="form-control" placeholder="" name="file_reupload3">
									</div>
								</div>
							</div>
						</div>
						<div class="row clearfix">
							<div class="col-sm-2">
								<label class="form-label">File 4</label>
							</div>
							<div class="col-sm-6">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="hidden" id="file_pendukung4" name="file_pendukung4" value="<?php echo $data['file_pendukung4'];?>" readonly>
										<p id="file_pendukung4" name="file_pendukung4">
											<a href="<?php echo base_url();?>document/<?php echo $data['csdm'];?>/<?php echo $data['file_pendukung4'];?> "><?php echo $data['file_pendukung4'];?></a>
										</p>
									</div>
								</div>
							</div>
							<div class="col-sm-1">
									Reupload
							</div>
							<div class="col-sm-3">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="file" id="file_reupload4" class="form-control" placeholder="" name="file_reupload4">
									</div>
								</div>
							</div>
						</div>

						<h2 class="card-inside-title">Update Reason</h2>
						<div class="row clearfix">
							<div class="col-sm-12">
								<div class="form-group-sm" style="visibility: hidden;">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<select class="form-control show-tick update_sdm" data-live-search="true" data-size="6" id="reason_update" name="reason_update">
											<option value='0' disabled>-- Pilih --</option>
											<option value=''>Update Data</option>
											<option value='PERPANJANG KONTRAK'>PERPANJANG KONTRAK</option>
											<option value='TIDAK PERPANJANG KONTRAK'>TIDAK PERPANJANG KONTRAK</option>
											<option value='MUTASI'>MUTASI</option>
											<option value='ROTASI'>ROTASI</option>
											<option value='CUTI MELAHIRKAN'>CUTI MELAHIRKAN</option>
											<option value='RESIGN ONE MONTH NOTICE'>RESIGN ONE MONTH NOTICE</option>
											<option value='RESIGN ON THE SPOT'>RESIGN ON THE SPOT</option>
											<!-- <option value='KONTRAK BARU'>KONTRAK BARU</option> -->
											<option value='END KONTRAK'>END KONTRAK</option>
											<option value='PENGEMBALIAN AGENT'>PENGEMBALIAN AGENT</option>
										</select>
										<label class="form-label">Reason</label>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="form-label">Status Karyawan</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<input type="hidden" class="form-control" name="status_sdm" id="status_sdm" value="<?php echo $data['status_sdm'];?>"/>
										<input type="text" class="form-control" name="temp_status_sdm" id="temp_status_sdm" value="<?php echo $data['status_sdm']=='1'?'AKTIF':'NON AKTIF';?>" readonly/>
									</div>
								</div>
							</div>	
							<div class="col-sm-2">
								<label class="form-label">Keterangan</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group-sm">
									<div class="form-line">
										<textarea rows="3" class="form-control " placeholder="Please type what you want..." name="notes" id="notes"></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="row clearfix">
							<div class="col-sm-2 hidden">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<select class="form-control show-tick update_sdm" data-live-search="true" data-size="6" id="status_sdm" name="status_sdm">
											<option value='1' disabled>-- Pilih --</option>
											<option value='1' <?php echo $data['status_sdm']=='1'?'selected':'';?>>AKTIF</option>
											<option value='0' <?php echo $data['status_sdm']=='0'?'selected':'';?>>NON AKTIF</option>
										</select>
										<label class="form-label">Status Karyawan</label>
									</div>
								</div>
							</div>
							<div class="col-sm-12"><!-- 
								<input type="text" class="form-control" name="nama_tl" id="nama_tl" readonly/> -->
								<!-- <input type="text" class="form-control" name="name2" id="name2" readonly/> -->
								<button type="submit" class="btn btn-primary m-t-15 waves-effect" id="button_submit"><i class="material-icons">file_upload</i> UPDATE</button>
							</div>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->
	</div>
</section>
<!-- <div class="alert alert-success alert-dismissible alertify-logs hidden" role="alert" id="success-update">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div id='message'>
    	Berhasil simpan perubahan.
    </div>	
</div> -->
<div class="modal fade" id="modaltambahjabatan" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="smallModalLabel">Form Tambah Nama Jabatan</h4>
            </div>
            <div class="modal-body" id="smallModalContent">
                <input type="text" class="form-control" id="nama_jabatan_baru" name="nama_jabatan_baru" placeholder="Jabatan">
            </div>
            <div class="modal-footer">
            	<button type="button" class="btn btn-link waves-effect" id="save_jabatan">SAVE</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalhapusjabatan" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm2" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="smallModalLabel">Form Hapus Jabatan</h4>
            </div>
            <div class="modal-body" id="smallModalContent">
                <div class="row">
					<div class="table-responsive">
						<table class="table table-striped" id="table_jabatan">
							<thead>
								<tr>
									<th>Nama Jabatan</th>
									<th>action</th>
								</tr>
							</thead>
							<tbody id="listnamajabatan">
							</tbody>
						</table>
					</div>
				</div>
            </div>
            <div class="modal-footer">
            	<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
