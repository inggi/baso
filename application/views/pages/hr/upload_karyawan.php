<section class="content">
	<div class="container-fluid">
		<div class="row clearfix" id="form-upload">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>UPLOAD KARYAWAN</b>
						</h2>
					</div>
					<div class="body">
						<form class="form-horizontal" id="do_upload">
							<div class="row">
								<div class="col-lg-2 col-md-2 form-control-label">
									<div class="form-group">
											<label for="password_2">File</label>
									</div>
								</div>
								<div class="col-lg-4 col-md-4">
									<div class="form-group">
										<div class="form-line">
											<input type="file" id="file_upload" class="form-control" placeholder="" name="file_upload">
										</div>
										<div class="help-info" style="color:red">*Ket: Format file harus sama dengan contoh yang disediakan</div>
									</div>
								</div>
								<div class="col-lg-2 col-md-2 hidden">
									<div class="form-group">
										<div class="input-group spinner" data-trigger="spinner">
											<div class="form-line">
												<input type="text" class="form-control text-center" value="1" data-rule="quantity" id="sheet" nama="sheet" data-max="10">
											</div>
											<div class="help-info">Jumlah Sheet</div>
											<span class="input-group-addon">
	                                            <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
	                                            <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
	                                        </span>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
									<button type="submit" class="btn btn-primary m-t-15 waves-effect" id="button_doupload"><i class="material-icons">file_upload</i> SUBMIT</button>

									<a href="<?php echo base_url(); ?>document/format_upload_sdm.xls" class="btn btn-success m-t-15 waves-effect" id="a_doupload"><i class="material-icons">file_download</i> Format Excel</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<!-- Basic Examples -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2> KODE KOLOM DATA SDM </h2>
					</div>
					<div class="body">
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<div class="table-responsive">
									<table id="table-jadwal-pegawai" class="table table-striped">
										<thead>
											<tr id="head_table">
												<th>KODE</th>
												<th>KETERANGAN</th>
											</tr>
										</thead>
										<tbody id="tbody_jadwal1">
											<tr>
												<td>name *</td>
												<td>Nama panjang SDM</td>
											</tr>
											<tr>
												<td>nik_hris</td>
												<td>NIK HRIS</td>
											</tr>
											<tr>
												<td>gender</td>
												<td>Jenis Kelamin</td>
											</tr>
											<tr>
												<td>csdm *</td>
												<td>Nama panjang SDM</td>
											</tr>
											<tr>
												<td>perner</td>
												<td>PERNER</td>
											</tr>
											<tr>
												<td>nama_online</td>
												<td>Nama Online Agent</td>
											</tr>
											<tr>
												<td>jabatan</td>
												<td>Jabatan</td>
											</tr>
											<tr>
												<td>jabatan_level *</td>
												<td>ADMIN/SPV/TL/STAFF/ROSTER</td>
											</tr>
											<tr>
												<td>unit</td>
												<td>IBC/OBC</td>
											</tr>
											<tr>
												<td>ctp</td>
												<td>CTP</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="table-responsive">
									<table id="table-jadwal-pegawai" class="table table-striped">
										<thead>
											<tr id="head_table">
												<th>KODE</th>
												<th>KETERANGAN</th>
											</tr>
										</thead>
										<tbody id="tbody_jadwal2">
											<tr>
												<td>tl</td>
												<td>Team Leader</td>
											</tr>
											<tr>
												<td>spv</td>
												<td>Supevisor</td>
											</tr>
											<tr>
												<td>skill</td>
												<td></td>
											</tr>
											<tr>
												<td>skema_agent</td>
												<td></td>
											</tr>
											<tr>
												<td>tgl_awal_kontrak</td>
												<td></td>
											</tr>
											<tr>
												<td>tgl_akhir_kontrak</td>
												<td></td>
											</tr>
											<tr>
												<td>ppjp</td>
												<td></td>
											</tr>
											<tr>
												<td>no_hp</td>
												<td></td>
											</tr>
											<tr>
												<td>bpjs_tk</td>
												<td></td>
											</tr>
											<tr>
												<td>bpjs_kesehatan</td>
												<td></td>
											</tr>
											<tr>
												<td>npwp</td>
												<td></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->
	</div>
</section>
