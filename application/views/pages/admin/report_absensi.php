<section class="content">
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-xs-12 col-sm-3">
				<div class="card profile-card">
					<!-- <div class="profile-header">&nbsp;</div> -->
					<div class="profile-body" style="padding-top: 20px">
						<div class="content-area">
							<h4>Galang Vandi Mehisa</h4>
							<p>STAFF INTERFALL</p>
							<p>STAFF</p>
						</div>
					</div>
				</div>

				<div class="card card-about-me">
					<div class="body">
						<ul>
							<li>
								<div class="title">
									SPV
								</div>
								<div class="content">
									Yudianshay Prima
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-9">
				<div class="card">
					<div class="body">
						<div>
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">Report Personal</a></li>
								<li role="presentation"><a href="#absensi" aria-controls="absensi" role="tab" data-toggle="tab">Report Absensi</a></li>
							</ul>

							<div class="tab-content">
								<div role="tabpanel" class="tab-pane fade in active" id="personal">
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
											<div class="form-group">
												<div class="form-line">
													<select class="form-control show-tick" data-live-search="true" id="month">
														<option value="0">Januari</option>
														<option value="1">Febuari</option>
														<option value="2">Maret</option>
														<option value="3">April</option>
														<option value="4">Mei</option>
														<option value="5">Juni</option>
														<option value="6">Juli</option>
														<option value="7">Agustus</option>
														<option value="8">September</option>
														<option value="9">Oktober</option>
														<option value="10">November</option>
														<option value="11">Desember</option>
													</select>
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
											<div class="form-group">
			                                    <div class="input-group spinner" data-trigger="spinner">
			                                        <div class="form-line">
			                                            <input type="text" class="form-control text-center" value="2019" data-rule="quantity" id="tahun" data-max="2500">
			                                        </div>
			                                        <span class="input-group-addon">
			                                            <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
			                                            <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
			                                        </span>
			                                    </div>
											</div>
										</div>
										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
											<button type="button" class="btn btn-primary btn-sm waves-effect" id="form-filter_report">
												<i class="material-icons">filter_list</i><span>FILTER</span>
											</button>
										</div>
									</div>
									<div class="row clearfix">
										<table id="table-jadwal_report" class="table table-bordered">
											<thead>
												<tr id="head_table" class="text-center info">
													<th>Tgl</th>
													<th>Pola</th>
													<th>Jam Masuk</th>
													<th>Login</th>
													<th>Logout</th>
													<th>Status Terlambat</th>
													<th>Lama Terlambat</th>
													<th>Jam Kerja</th>
												</tr>
											</thead>
											<tbody id="tbody_report">
												<tr id="head_table" class="text-right">
													<td>1</td>
													<td>BD</td>
													<td>08:00:00</td>
													<td>08:02:00</td>
													<td>17:02:00</td>
													<td>Terlambat</td>
													<td>00:02:00</td>
													<td>09:00:00</td>
												</tr>
												<tr id="head_table" class="text-right">
													<td>2</td>
													<td>BD</td>
													<td>08:00:00</td>
													<td>08:02:00</td>
													<td>17:02:00</td>
													<td>Terlambat</td>
													<td>00:02:00</td>
													<td>09:00:00</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane fade in" id="absensi">

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
