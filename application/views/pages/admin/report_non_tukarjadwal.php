<section class="content">
	<div class="container-fluid">
	<!-- Filter -->
		<div class="row clearfix" id="form-filter_admin_document">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>FILTER</b>
						</h2>
					</div>

					<div class="body">
						<form>
							<div class="row clearfix">
                                <div class="col-xs-6">
                                    <h2 class="card-inside-title">Range Date</h2>
                                    <div class="input-daterange input-group" id="bs_datepicker_range_container">
                                    	<span class="input-group-addon">from</span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="DD/MM/YYYY" id="date_start">
                                        </div>
                                        <span class="input-group-addon">to</span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="DD/MM/YYYY" id="date_end">
                                        </div>
                                        <small><div class="help-info" >*Max range 1 bulan</div></small>
                                    </div>
                                </div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
									<h2 class="card-inside-title">Unit</h2>
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" data-live-search="true" data-size="6" id="unit">
												<option value='0' disabled>-- Pilih Unit --</option>
												<option value='search' disabled>Search.. </option>
											</select>
										</div>
									</div>
								</div>
                            </div>
							<div class="row clearfix">
								<!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" data-live-search="true" id="nama">
												<option disabled>-- Pilih nama --</option>
											</select>
										</div>
									</div>
								</div> -->
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
									<button type="button" class="btn btn-primary waves-effect" id="form-filter_report">Submit</button>
									<button target="_blank" href="http://localhost/fania/ca_jadwal/get_xls_keterlambatan" type="button" class="btn btn-success waves-effect" id="export_excel">Export Excel</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->


		<!-- Basic Examples -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>REPORT</b>
						</h2>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table id="table-jadwal_report" class="table table-striped">
								<thead>
									<tr id="head_table">
										<th>Tanggal</th>
										<th>Nama</th>
										<th>NIK CSDM</th>
										<th>Pola</th>
										<th>Pola Pengganti</th>
										<th>Reason</th>
										<th>Bukti</th>
									</tr>
								</thead>
								<tbody id="tbody_report">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->
	</div>
</section>
