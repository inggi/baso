<section class="content">
	<div class="container-fluid">
		<!-- Basic Examples -->
			<div class="row clearfix">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="card">
						<div class="header">
							<h2>
								FORM Pembinaan 
								<small>Input Pembinaan </small>
							</h2>
							<input type="hidden" class="form-control update_sdm" name="id_sdm" id="id_sdm" value=""/>
						</div>
						<div class="body">
							<form class="" id="form_insert_sanksi">
								<h2 class="card-inside-title">Profile</h2>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="name" id="name" value="<?php echo $data['name_agent']; ?>" disabled />
												<label class="form-label">Nama</label>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="csdm" id="csdm" value="<?php echo $data['csdm_agent']; ?>" disabled/>
												<label class="form-label">CSDM</label>
											</div>
										</div>
									</div>
									
								</div>

								<h2 class="card-inside-title">Created</h2>
								<div class="row clearfix">
									<div class="col-sm-6">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="name" id="name2" value="<?php echo $data['name_created']; ?>" disabled />
												<label class="form-label">Nama</label>
											</div>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="csdm" id="csdm2" value="<?php echo $data['csdm_created']; ?>" disabled/>
												<label class="form-label">CSDM</label>
											</div>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="pendidikan" id="tgl_input" value="<?php echo $data['tgl_dibuat']; ?>" disabled/>
												<label class="form-label">Tanggal Input</label>
											</div>
										</div>
									</div>
								</div>

								<h2 class="card-inside-title">Pembinaan</h2>
								<div class="row clearfix">
									<div class="col-sm-3">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="tgl_kejadian_view" id="tgl_kejadian_view" value="<?php echo $data['tgl_kejadian']; ?>" disabled />
												<label class="form-label">Tanggal Kejadian</label>
											</div>
										</div>
									</div>
									<div class="col-sm-9">
										<div class="form-group form-float form-group-sm" style="visibility: hidden;">
											<div class="form-line">
												<input type="text" class="form-control update_sdm" />
												<label class="form-label">HIDDEN</label>
											</div>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="jenis_kesalahan_view" id="jenis_kesalahan_view" value="<?php echo $data['jenis_kesalahan']; ?>" disabled />
												<label class="form-label">Jenis Kesalahan</label>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="category" id="category" value="<?php echo $data['category']; ?>" disabled />
												<label class="form-label">Kategori Kesalahan</label>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="description" id="description" value="<?php echo $data['description']; ?>" disabled />
												<label class="form-label">Deskripsi Kesalahan</label>
											</div>
										</div>
									</div>

									<div class="col-sm-12">
										<div class="form-group form-float form-group-sm">
											<div class="form-line"  style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="uraian" id="uraian" value="<?php echo $data['uraian']; ?>" disabled/>
												<label class="form-label">Uraian</label>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group form-float form-group-sm">
											<div class="form-line"  style="background-color: rgba(228,228,228,0.3);">

												<input type="text" class="form-control update_sdm" name="rekomendasi" id="rekomendasi" value="<?php echo $data['rekomendasi']; ?>" disabled />
												<label class="form-label">Rekomendasi</label>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="pendidikan" id="pendidikan" value="<?php echo $data['flow_title']; ?>" disabled/>
												<label class="form-label">Status Kesalahan</label>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group form-float form-group-sm">
											<div class="form-line">

												<input type="text" class="form-control update_sdm" name="commitment" id="commitment" value="" required />
												<label class="form-label">Komitmen</label>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<input type="hidden" class="form-control update_sdm" name="id" id="id" value="<?php echo $data['id']; ?>" />

										<button type="submit" class="btn btn-primary m-t-15 waves-effect" id="button_sumbit"><i class="material-icons">file_upload</i> SUBMIT</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		<!-- #END# Basic Examples -->
	</div>
</section>
