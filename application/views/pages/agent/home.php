<section class="content">
		<div class="container-fluid">
			<!-- Counter Examples -->
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<div class="info-box-3 bg-deep-orange hover-zoom-effect">
						<div class="icon">
							<i class="material-icons">devices</i>
						</div>
						<div class="content">
							<div class="text">Status</div>
							<div class="number"><?php echo $data['flow_title']; ?></div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="info-box-3 bg-orange hover-zoom-effect">
						<div class="icon">
							<i class="material-icons">devices</i>
						</div>
						<div class="content">
							<div class="text">Finish Pembinaan</div>
							<div class="number"><?php echo $data['tgl_berakhir']; ?></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="info-box-3 bg-green hover-zoom-effect">
						<div class="icon">
							<i class="material-icons">devices</i>
						</div>
						<div class="content">
							<div class="text">Waiting Approval</div>
							<div class="number"><?php echo $data['on_approval']; ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>