<section class="content">
	<div class="container-fluid">
	<!-- Filter -->
		<div class="row clearfix" id="form-filter_admin_document">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>Input Pembinaan</b>
						</h2>
					</div>

					<div class="body">
						<form>
							
 							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" data-live-search="true" data-size="6" id="jenis_kesalahan">
												<option value='0' disabled>-- Pilih Jenis Kesalahan --</option>
												<option value='search' disabled>Search.. </option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" data-live-search="true" data-size="6" id="category">
												<option value='0' disabled>-- Pilih Category --</option>
												<option value='search' disabled>Search.. </option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" data-live-search="true" data-size="6" id="description">
												<option value='0' disabled>-- Pilih Description --</option>
												<option value='search' disabled>Search.. </option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" data-live-search="true" id="lol">
		                                        <option>Hot Dog, Fries and a Soda</option>
		                                        <option>Burger, Shake and a Smile</option>
		                                        <option>Sugar, Spice and all things nice</option>
		                                    </select>
										</div>
									</div>
								</div>
								<!-- <div class="col-md-3">
                                    <p>
                                        <b>With Search Bar</b>
                                    </p>
                                    <select class="form-control show-tick" data-live-search="true">
                                        <option>Hot Dog, Fries and a Soda</option>
                                        <option>Burger, Shake and a Smile</option>
                                        <option>Sugar, Spice and all things nice</option>
                                    </select>
                                </div> -->
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
									<button type="button" class="btn btn-primary waves-effect" id="form-filter_report">Submit</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->


		<!-- Basic Examples -->
		<!-- <div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>REPORT ABSENSI</b>
						</h2>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table id="table-jadwal_report" class="table table-bordered">
								<thead>
									<tr id="head_table" class="text-center info">
										 <th>NIK CSDM</th>
			                             <th>Nama</th>
			                             <th>Tanggal Roster</th>
			                             <th>Pola</th>
			                             <th>ABS</th>
			                             <th>Login</th>
			                             <th>Logout</th>
			                             <th>Status</th>
			                             <th>Lama Terlambat</th>
			                             <th>Lama Kerja</th>
			                             <th>Foto In</th>
			                             <th>Foto Out</th>
									</tr>
								</thead>
								<tbody id="tbody_report">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div> -->
		<!-- #END# Basic Examples -->
	</div>
</section>
