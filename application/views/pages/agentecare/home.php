<section class="content">
	<div class="container-fluid">
	<!-- Filter -->
		<div class="row clearfix" id="form-filter_admin_document">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>FILTER</b>
						</h2>
					</div>

					<div class="body">
						<form>
							<div class="row clearfix">
								<!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" data-live-search="true" data-size="5" id="jabatan_ecare">
												<?php 
													// foreach($jabatan_ecare as $key ){
													// 	echo '<option value="'.$key['jabatan'].'">'.$key['jabatan'].'</option>';
													// }
												?>
											</select>
										</div>
									</div>
								</div> -->
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" data-live-search="true" data-size="5" id="jabatan">
												<!-- <option value='0' disabled>-- Pilih Jabatan --</option>
												<option value='search' disabled>Search.. </option> -->
											</select>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" data-live-search="true" id="month">
												<option value="0">Januari</option>
												<option value="1">Febuari</option>
												<option value="2">Maret</option>
												<option value="3">April</option>
												<option value="4">Mei</option>
												<option value="5">Juni</option>
												<option value="6">Juli</option>
												<option value="7">Agustus</option>
												<option value="8">September</option>
												<option value="9">Oktober</option>
												<option value="10">November</option>
												<option value="11">Desember</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
									<div class="form-group">
	                                    <div class="input-group spinner" data-trigger="spinner">
	                                        <div class="form-line">
	                                            <input type="text" class="form-control text-center" value="2019" data-rule="quantity" id="tahun" data-max="2500">
	                                        </div>
	                                        <span class="input-group-addon">
	                                            <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
	                                            <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
	                                        </span>
	                                    </div>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<button type="button" class="btn btn-primary waves-effect" id="form-filter_jadwal"><i class="material-icons">filter_list</i> <span>FILTER</span></button>
									<button type="button" class="btn btn-success waves-effect" id="btn_show_pola"><i class="material-icons">info_outline</i> <span>POLA</span></button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->

		<!-- Informasi POLA Jadwal -->
		<div class="row clearfix hidden" id="pola_jadwal">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2> POLA JADWAL </h2>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown" id="btn-close_pola_jadwal" style=" cursor: pointer;">
								<i class="material-icons">close</i>
							</li>
						</ul>
					</div>
					<div class="body">
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<div class="table-responsive">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>NO</th>
												<th>Pola</th>
												<th>Masuk</th>
												<th>Pulang</th>
												<th>Keterangan</th>
											</tr>
										</thead>
										<tbody id="tbody_jadwal1">
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="table-responsive">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>NO</th>
												<th>Pola</th>
												<th>Masuk</th>
												<th>Pulang</th>
												<th>Keterangan</th>
											</tr>
										</thead>
										<tbody id="tbody_jadwal2">
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# POLA Jadwal -->

		<!-- Basic Examples -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>JADWAL</b>
						</h2>

					</div>
					<div class="body">
						<div class="table-responsive">
							<table id="table-jadwal-pegawai" class="table table-striped table-bordered">
								<thead>
									<tr id="head_table">
									</tr>
								</thead>
								<tbody id="tbody_jadwal">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->
	</div>
</section>
