<section class="content">
	<div class="container-fluid">
		<!-- Filter -->
		<!-- <div class="row clearfix hidden" id="form-filter">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>FORM AUX</b>
						</h2>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown" id="close_filter" style=" cursor: pointer;">
								<i class="material-icons">close</i>
							</li>
						</ul>
					</div>

					<div class="body">
						<form id=''>
							<div class="row clearfix">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="name" id="name" value="<?php //echo "aa"; ?>" disabled />
												<label class="form-label">Nama</label>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="csdm" id="csdm" value="<?php //echo $data['csdm_agent']; ?>" disabled/>
												<label class="form-label">CSDM</label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" data-live-search="true" data-size="6" id="jenis_aux">
												<option value='0' disabled>-- Choose AUX --</option>
												<option value='All' selected>All</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<button type="button" class="btn bg-green waves-effect" id="form-filter_report"><span>AUX</span></button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div> -->
		<!-- #END# Basic Examples -->


		<!-- Basic Examples -->
		<div class="row clearfix" id="jadwal">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>AUX</b>		
						</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <!-- <li><a href="javascript:void(0);" id="show_upload">Upload</a></li> -->
                                    <li><a href="javascript:void(0);" id="show_filter">AUX</a></li>
                                </ul>
                            </li>
                        </ul>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table id="data_agent" class="table table-striped table-bordered table-bordered">
								<thead>
									<tr id="head_table" height="40" class="info">
										<th class="success text-center">KATEGORI AUX</th>
										<th class="success text-center">QUOTA AUX</th>
										<th class="success text-center">Star Aux</th>
										<th class="success text-center">Duration</th>
										<th class="success text-center">Action</th>
									</tr>
								</thead>
								<tbody id="tbodyy">
								</tbody>
							</table>
						</div>
					</div>
					<input type="hidden" class="form-control" name="id_presensi" id="id_presensi" value="<?php echo $data['id']; ?>" />
					<input type="hidden" class="form-control" name="status_aux" id="status_aux" value="<?php echo $data['status_aux']; ?>" />
					<input type="hidden" class="form-control" name="id_m_aux" id="id_m_aux" value="<?php echo $data['id_m_aux']; ?>" />
					<input type="hidden" class="form-control" name="id_presensi_aux" id="id_presensi_aux" value="<?php echo $data['id_presensi_aux']; ?>" />
					<input type="hidden" class="form-control" name="flag1" id="flag1" value="<?php echo $data['flag1']; ?>" />
					<input type="hidden" class="form-control" name="aux_start" id="aux_start" value="<?php echo $data['aux_start']; ?>" />
					<input type="hidden" class="form-control" name="diff" id="diff" value="<?php echo $data['diff']; ?>" />
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->

		<!-- Basic Examples -->
		<div class="row clearfix" id="jadwal">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>HISTORY AUX</b>
						</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <!-- <li><a href="javascript:void(0);" id="show_upload">Upload</a></li> -->
                                    <li><a href="javascript:void(0);" id="show_filter">Filter</a></li>
                                </ul>
                            </li>
                        </ul>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table id="data_history_aux" class="table table-striped table-bordered table-bordered">
								<thead>
									<tr id="head_table" height="40" class="info">
										<th class="success text-center">Start</th>
										<th class="success text-center">End</th>
										<th class="success text-center">Duration</th>
										<th class="success text-center">AUX REASON</th>
									</tr>
								</thead>
								<tbody id="tbody">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->
	</div>
</section>
