<section class="content">
	<div class="container-fluid">
		
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							UBAH PASSWORD
						</h2>
					</div>
					<div class="body">
						<form class="form-horizontal" id="do_update_password">
							<div class="row">
								<div class="col-lg-3 col-md-3 form-control-label">
									<div class="form-group">
											<label for="password_2">New Password</label>
									</div>
								</div>
								<div class="col-lg-4 col-md-4">
									<div class="form-group">
										<div class="form-line">
											<input type="password" id="password" class="form-control" placeholder="" name="password">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-3 col-md-3 form-control-label">
									<div class="form-group">
											<label for="password_2">Retype New Password</label>
									</div>
								</div>
								<div class="col-lg-4 col-md-4">
									<div class="form-group">
										<div class="form-line">
											<input type="password" id="retype_password" class="form-control" placeholder="" name="retype_password">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-4 col-xs-offset-5">
									<button type="submit" class="btn btn-primary m-t-15 waves-effect" id="button_doupload"> SUBMIT</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>