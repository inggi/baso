<section class="content">
	<div class="container-fluid">
	<!-- Filter -->
		<div class="row clearfix" id="form-filter_admin_document">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>BULAN</b>
						</h2>
					</div>

					<div class="body">
						<form>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" data-live-search="true" id="month">
												<option value="0">Januari</option>
												<option value="1">Febuari</option>
												<option value="2">Maret</option>
												<option value="3">April</option>
												<option value="4">Mei</option>
												<option value="5">Juni</option>
												<option value="6">Juli</option>
												<option value="7">Agustus</option>
												<option value="8">September</option>
												<option value="9">Oktober</option>
												<option value="10">November</option>
												<option value="11">Desember</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
									<div class="form-group">
	                                    <div class="input-group spinner" data-trigger="spinner">
	                                        <div class="form-line">
	                                            <input type="text" class="form-control text-center" value="2019" data-rule="quantity" id="tahun" data-max="2500">
	                                        </div>
	                                        <span class="input-group-addon">
	                                            <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
	                                            <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
	                                        </span>
	                                    </div>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
									<button type="button" class="btn btn-primary waves-effect" id="form-filter_report">Filter</button>
									<button target="_blank" type="button" class="btn btn-success waves-effect" id="export_excel">Export Excel</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->


		<!-- Basic Examples -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>REPORT</b>
						</h2>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table id="table-jadwal_report" class="table table-striped">
								<thead>
									<tr id="head_table">
										<th>Tanggal</th>
										<th>Nama</th>
										<th>NIK CSDM</th>
										<th>Pola</th>
										<th>Pola Pengganti</th>
										<th>Reason</th>
										<th>Bukti</th>
									</tr>
								</thead>
								<tbody id="tbody_report">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->
	</div>
</section>
