<section class="content">
	<div class="container-fluid">
		<!-- Basic Examples -->
			<div class="row clearfix">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="card">
						<div class="header">
							<h2>
								FORM Pembinaan 
								<small>Input Pembinaan </small>
							</h2>
							<input type="hidden" class="form-control update_sdm" name="id_sdm" id="id_sdm" value=""/>
						</div>
						<div class="body">
							<form class="" id="form_insert_sanksi">
								<h2 class="card-inside-title">Profile</h2>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="name" id="name" value="<?php echo $data['nama_agent']; ?>" disabled />
												<label class="form-label">Nama</label>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="csdm" id="csdm" value="<?php echo $data['csdm_agent']; ?>" disabled/>
												<label class="form-label">CSDM</label>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="pendidikan" id="pendidikan" value="<?php echo $data['status_terakhir']; ?>" disabled/>
												<label class="form-label">Status terakhir</label>
											</div>
										</div>
									</div>
								</div>

								<h2 class="card-inside-title">Created</h2>
								<div class="row clearfix">
									<div class="col-sm-6">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="name" id="name2" value="<?php echo $data['created']; ?>" disabled />
												<label class="form-label">Nama</label>
											</div>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="csdm" id="csdm2" value="<?php echo $data['csdm_created']; ?>" disabled/>
												<label class="form-label">CSDM</label>
											</div>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="pendidikan" id="tgl_input" value="<?php echo $data['create_by']; ?>" disabled/>
												<label class="form-label">Tanggal Input</label>
											</div>
										</div>
									</div>
								</div>

								<h2 class="card-inside-title">Pembinaan</h2>
								<div class="row clearfix">
									<div class="col-sm-3">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="tgl_kejadian_view" id="tgl_kejadian_view" value="<?php echo $data['tgl_kesalahan_view']; ?>" disabled />
												<label class="form-label">Tanggal Kejadian</label>
											</div>
										</div>
									</div>
									<div class="col-sm-9">
										<div class="form-group form-float form-group-sm" style="visibility: hidden;">
											<div class="form-line">
												<input type="text" class="form-control update_sdm" />
												<label class="form-label">HIDDEN</label>
											</div>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="jenis_kesalahan_view" id="jenis_kesalahan_view" value="<?php echo $data['jenis_kesalahan']; ?>" disabled />
												<label class="form-label">Jenis Kesalahan</label>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="category" id="category" value="<?php echo $data['category']; ?>" disabled />
												<label class="form-label">Kategori Kesalahan</label>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="description" id="description" value="<?php echo $data['description']; ?>" disabled />
												<label class="form-label">Deskripsi Kesalahan</label>
											</div>
										</div>
									</div>

									<div class="col-sm-12">
										<div class="form-group form-float form-group-sm">
											<div class="form-line">
												<input type="text" class="form-control update_sdm" name="uraian" id="uraian" value="" required/>
												<label class="form-label">Uraian</label>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group form-float form-group-sm">
											<div class="form-line">

												<input type="text" class="form-control update_sdm" name="rekomendasi" id="rekomendasi" value="" required />
												<label class="form-label">Rekomendasi</label>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<input type="hidden" class="form-control update_sdm" name="id_pembinaan" id="id_pembinaan" value="<?php echo $data['id_pembinaan']; ?>" />

										<input type="hidden" class="form-control update_sdm" name="id_pmb_kesalahan" id="id_pmb_kesalahan" value="<?php echo $data['id_pmb_kesalahan']; ?>" />

										<input type="hidden" class="form-control update_sdm" name="flow_id" id="flow_id" value="<?php echo $data['flow_id']; ?>" />

										<input type="hidden" class="form-control update_sdm" name="nilai" id="nilai" value="<?php echo $data['nilai']; ?>" />

										<input type="hidden" class="form-control update_sdm" name="jenis_kesalahan" id="jenis_kesalahan" value="<?php echo $data['jenis_kesalahan']; ?>" />

										<input type="hidden" class="form-control update_sdm" name="id_kesalahan" id="id_kesalahan" value="<?php echo $data['id_kesalahan']; ?>" />

										<input type="hidden" class="form-control update_sdm" name="id_agent" id="id_agent" value="<?php echo $data['id_agent']; ?>" />

										<input type="hidden" class="form-control update_sdm" name="id_created" id="id_created" value="<?php echo $data['id_created']; ?>" />

										<input type="hidden" class="form-control update_sdm" name="tgl_kejadian" id="tgl_kejadian" value="<?php echo $data['tgl_kesalahan']; ?>" />

										<button type="submit" class="btn btn-primary m-t-15 waves-effect" id="button_sumbit"><i class="material-icons">file_upload</i> SUBMIT</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		<!-- #END# Basic Examples -->
	</div>
</section>
