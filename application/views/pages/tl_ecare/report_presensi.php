<section class="content">
	<div class="container-fluid">
	<!-- Filter -->
		<div class="row clearfix" id="form-filter_admin_document">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>FILTER</b>
						</h2>
					</div>

					<div class="body">
						<form>
							<div class="row clearfix">
                                <div class="col-xs-6">
                                    <h2 class="card-inside-title">Range Date</h2>
                                    <div class="input-daterange input-group" id="bs_datepicker_range_container">
                                        <span class="input-group-addon">from</span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="DD/MM/YYYY" id="date_start">
                                        </div>
                                        <span class="input-group-addon">to</span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="DD/MM/YYYY" id="date_end">
                                        </div>
                                        <small><div class="help-info" >*Max range 1 bulan</div></small>
                                    </div>
                                </div>
                            </div>
 							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" data-live-search="true" data-size="6" id="jabatan">
												<option value='0' disabled>-- Pilih Jabatan --</option>
												<option value='search' disabled>Search.. </option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" data-live-search="true" data-size="6" id="name">
												<option value='0' disabled>-- Pilih Nama --</option>
												<option value='search' disabled>Search.. </option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
									<button type="button" class="btn btn-primary waves-effect" id="form-filter_report">Submit</button>
									<button target="_blank" type="button" class="btn btn-success waves-effect" id="export_excel">Export Excel</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->


		<!-- Basic Examples -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>REPORT ABSENSI</b>
						</h2>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table id="table-jadwal_report" class="table table-bordered">
								<thead>
									<tr id="head_table" class="text-center info" height="50px">
										 <th>No CSDM</th>
			                             <th>Nama</th>
			                             <th>Tanggal Roster</th>
			                             <th>Pola</th>
			                             <th>Abs</th>
			                             <th>Login</th>
			                             <th>Logout</th>
			                             <th>Status</th>
			                             <th>Lama Terlambat</th>
			                             <th>Lama Kerja</th>
			                             <th>Foto In</th>
			                             <th>Foto Out</th>
									</tr>
								</thead>
								<tbody id="tbody_report">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->
	</div>
</section>
