<section class="content">
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>FORM TUKAR JADWAL</b>
						</h2>
					</div>
					<div class="body">
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<h2 class="card-inside-title">User Peminta</h2>
							</div>
							<div class="col-lg-6 col-md-6">
								<h2 class="card-inside-title">User Pengganti</h2>
							</div>
						</div>
						<form class="form-horizontal" id="form-tukar-jadwal" autocomplete="off">
							<div class="row">
								<div class="col-lg-1 col-md-1 form-control-label">
									<div class="form-group">
										<label>Nama</label>
									</div>
								</div>
								<div class="col-lg-5 col-md-5">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick " data-live-search="true"  data-show-subtext="true" nama="agent1" data-size="6" id="agent1">
												<option disabled selected>-- Pilih Nama --</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-lg-1 col-md-1 form-control-label">
									<div class="form-group">
										<label>Nama</label>
									</div>
								</div>
								<div class="col-lg-5 col-md-5">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick " data-live-search="true" data-show-subtext="true" nama="agent2" data-size="6" id="agent2">
												<option disabled selected>-- Pilih Nama --</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-1 col-md-1 form-control-label">
									<div class="form-group">
										<label>Tanggal</label>
									</div>
								</div>
								<div class="col-lg-5 col-md-5">
									<div class="form-group">
										<div class="form-line" id="bs_datepicker_container">
											<input type="text" class="form-control" placeholder="Date..." name="tgl_ubah1" id="tgl_ubah1" required>
										</div>
									</div>
								</div>
								<div class="col-lg-1 col-md-1 form-control-label">
									<div class="form-group">
										<label>Tanggal</label>
									</div>
								</div>
								<div class="col-lg-5 col-md-5">
									<div class="form-group">
										<div class="form-line" id="bs_datepicker_container">
											<input type="text" class="form-control" placeholder="Date..." name="tgl_ubah2" id="tgl_ubah2" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-1 col-md-1 form-control-label">
									<div class="form-group">
										<label>Pola</label>
									</div>
								</div>
								<div class="col-lg-5 col-md-5">
									<div class="form-group">
										<div class="form-line">
											<input type="text" class="form-control" readonly="true" name="pola1_view" id="pola1_view" >
											<input type="hidden" class="form-control" readonly="true" name="pola1" id="pola1" required>
										</div>
									</div>
								</div>
								<div class="col-lg-1 col-md-1 form-control-label">
									<div class="form-group">
										<label>Pola</label>
									</div>
								</div>
								<div class="col-lg-5 col-md-5">
									<div class="form-group">
										<div class="form-line">
											<input type="text" class="form-control" readonly="true" name="pola2_view" id="pola2_view" required>
											<input type="hidden" class="form-control" readonly="true" name="pola2" id="pola2" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col-md-12">
									<h2 class="card-inside-title">Keterangan</h2>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-1 col-md-1 form-control-label">
									<div class="form-group">
										<label>Type</label>
									</div>
								</div>
								<div class="col-lg-10 col-md-10">
									<div class="form-group">
										<div class="form-line">
											<input type="text" class="form-control" readonly="true" name="type_tukar" id="type_tukar" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-1 col-md-1 form-control-label">
									<div class="form-group">
										<label>Reason</label>
									</div>
								</div>
								<div class="col-lg-10 col-md-10">
									<div class="form-group">
										<div class="form-line">
											<input type="text" class="form-control"  name="alasan" id="alasan">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-1 col-md-1 form-control-label">
									<div class="form-group">
										<label>Upload Bukti</label>
									</div>
								</div>
								<div class="col-lg-10 col-md-10">
									<div class="form-group">
										<div class="form-line">
											<input type="file" class="form-control"  name="bukti" id="bukti">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
									<button type="submit" class="btn btn-primary m-t-15 waves-effect" id="button_doupload"> SUBMIT</button> 
								</div>
								<div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
									<label></label>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>