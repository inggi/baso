<section class="content">
	<div class="container-fluid">
	<!-- Filter -->
		<div class="row clearfix" id="form-filter_admin_document">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>BULAN</b>
						</h2>
					</div>

					<div class="body">
						<form>
							<div class="row clearfix">
                                <div class="col-xs-3">
                                    <h2 class="card-inside-title">Jabatan</h2>
									<div class="form-line">
										<select class="form-control show-tick" data-live-search="true" data-size="5" id="jabatan_ecare">
											<?php 
												foreach($jabatan_ecare as $key ){
													echo '<option value="'.$key['jabatan'].'">'.$key['jabatan'].'</option>';
												}
											?>
										</select>
									</div>
                                </div>
                                <div class="col-xs-6">
                                    <h2 class="card-inside-title">Range Date</h2>
                                    <div class="input-daterange input-group" id="bs_datepicker_range_container">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Date start..." id="date_start">
                                        </div>
                                        <span class="input-group-addon">to</span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Date end..." id="date_end">
                                        </div>
                                        <div class="help-info" >*Max range 1 bulan</div>
                                    </div>
                                </div>
								<div class="col-xs-3">
									<button type="button" class="btn btn-primary waves-effect" id="form-filter_report">Submit</button>
									<button target="_blank" type="button" class="btn btn-success waves-effect" id="export_excel">Export Excel</button>
								</div>
                            </div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->


		<!-- Basic Examples -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>REPORT SUMMARY</b>
						</h2>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table id="table-jadwal_report" class="table table-bordered">
								<thead>
									<tr id="head_table" class="text-center info">
										<th>NIK CSDM</th>
										<th>Nama</th>
										<th>Jumlah Hari</th>
										<th>Hadir</th>
										
										<!--<th>Presentasi keterlambatan</th>-->
										<th>CUK</th>
										<th>CUD</th>
										<th>CM</th>
										<th>CT</th>
										<th>Total Cuti</th>
										<th>L</th>
										<th>S</th>
										<th>TK</th>
										<th>TD</th>
										<th>TL</th>
										<th>TC</th>
										<th>Jumlah Tukar Jadwal</th>
										<th>Effective Time</th>
										<th>SA</th>
										<th>Terlambat</th>
										<th>Durasi Keterlambatan</th>
										<th>Ketidakhadiran (S+TK)</th>
										<th>Ketidakhadiran (%)</th>
									</tr>
								</thead>
								<tbody id="tbody_report">
									<!-- <tr id="head_table" class="text-right">
										<td>11009676</td>
										<td nowrap><a href="<?php echo base_url();?>admin/content/report_absensi">Galang Vandi Mehisa</a></td>
										<td>28</td>
										<td>0</td>
										<td>0</td>
										<td>00:00:00</td>
										<td class="info">0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td class="info">0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td class="info">0</td>
									</tr> -->
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->
	</div>
</section>
