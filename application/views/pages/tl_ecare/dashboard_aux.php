<section class="content" style="margin: 90px 15px 0 0px !important;">
	<div class="container-fluid">
		<div class="row" id="jadwal">
			<div class="col-lg-2">
				<div class="row" id="jadwal">
					<div class="col-lg-12">
						<div class="info-box bg-indigo hover-expand-effect">
							<div class="icon1">
								<h1>FB</h1>
							</div>
							<div class="icon2">
								<div class="box1">
									<div class="content">
										<div class="text">LOGIN &nbsp;&nbsp;&nbsp;:
											<span id="fb_login">0</span>
										</div>
									</div>
								</div>
								<div class="box2">
									<div class="content">
										<div class="text">AUX &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
											<span id="fb_aux">0</span></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12" class="aux1">
						<div class="card">
	                        <div class="body bg-grey">
	                            <ul class="dashboard-stat-list" id="list_fb"/>
	                                <!-- <li >
	                                	<b class="name_agent">AUX 1</b><br>
	                                    <span class="name_agent">Angga Mau... </span>
	                                    <span class="pull-right waktu_aux red" data-jam="0" data-menit="0" data-detik="20">00:00:00</span>
	                                </li> -->
	                            </ul>
	                        </div>
	                    </div>
					</div>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="row" id="jadwal">
					<div class="col-lg-12">
						<div class="info-box bg-blue hover-expand-effect">
							<div class="icon1">
								<h1>TW</h1>
							</div>
							<div class="icon2">
								<div class="box1">
									<div class="content">
										<div class="text">LOGIN &nbsp;&nbsp;&nbsp;:
											<span id="tw_login">0</span>
										</div>
									</div>
								</div>
								<div class="box2">
									<div class="content">
										<div class="text">AUX &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
											<span id="tw_aux">0</span></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12" class="aux1">
						<div class="card">
	                        <div class="body bg-grey">
	                            <ul class="dashboard-stat-list" id="list_tw"/>
	                            </ul>
	                        </div>
	                    </div>
					</div>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="row" id="jadwal">
					<div class="col-lg-12">
						<div class="info-box bg-red hover-expand-effect">
							<div class="icon1">
								<h1>VA</h1>
							</div>
							<div class="icon2">
								<div class="box1">
									<div class="content">
										<div class="text">LOGIN &nbsp;&nbsp;&nbsp;:
											<span id="va_login">0</span>
										</div>
									</div>
								</div>
								<div class="box2">
									<div class="content">
										<div class="text">AUX &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
											<span id="va_aux">0</span></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12" class="aux1">
						<div class="card">
	                        <div class="body bg-grey">
	                            <ul class="dashboard-stat-list" id="list_va"/>
	                            </ul>
	                        </div>
	                    </div>
					</div>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="row" id="jadwal">
					<div class="col-lg-12">
						<div class="info-box bg-green hover-expand-effect">
							<div class="icon1">
								<h1>PS</h1>
							</div>
							<div class="icon2">
								<div class="box1">
									<div class="content">
										<div class="text">LOGIN &nbsp;&nbsp;&nbsp;:
											<span id="ps_login">0</span>
										</div>
									</div>
								</div>
								<div class="box2">
									<div class="content">
										<div class="text">AUX &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
											<span id="ps_aux">0</span></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12" class="aux1">
						<div class="card">
	                        <div class="body bg-grey">
	                            <ul class="dashboard-stat-list" id="list_ps"/>
	                            </ul>
	                        </div>
	                    </div>
					</div>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="row" id="jadwal">
					<div class="col-lg-12">
						<div class="info-box bg-deep-purple hover-expand-effect">
							<div class="icon1">
								<h1>IG</h1>
							</div>
							<div class="icon2">
								<div class="box1">
									<div class="content">
										<div class="text">LOGIN &nbsp;&nbsp;&nbsp;:
											<span id="ig_login">0</span>
										</div>
									</div>
								</div>
								<div class="box2">
									<div class="content">
										<div class="text">AUX &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
											<span id="ig_aux">0</span></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12" class="aux1">
						<div class="card">
	                        <div class="body bg-grey">
	                            <ul class="dashboard-stat-list" id="list_ig"/>
	                            </ul>
	                        </div>
	                    </div>
					</div>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="row" id="jadwal">
					<div class="col-lg-12">
						<div class="info-box bg-yellow hover-expand-effect">
							<div class="icon1">
								<h3>EMAIL</h3>
							</div>
							<div class="icon2">
								<div class="box1">
									<div class="content">
										<div class="text">LOGIN &nbsp;&nbsp;&nbsp;:
											<span id="email_login">0</span>
										</div>
									</div>
								</div>
								<div class="box2">
									<div class="content">
										<div class="text">AUX &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
											<span id="email_aux">0</span></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12" class="aux1">
						<div class="card">
	                        <div class="body bg-grey">
	                            <ul class="dashboard-stat-list" id="list_email"/>
	                            </ul>
	                        </div>
	                    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>