<section class="content">
	<div class="container-fluid">
		<!-- Basic Examples -->
			<div class="row clearfix">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="card">
						<div class="header">
							<h2>
								FORM Pembinaan 
								<small>Input Pembinaan </small>
							</h2>
							<input type="hidden" class="form-control update_sdm" name="id_sdm" id="id_sdm" value=""/>
						</div>
						<div class="body">
							<form class="" id="form_insert_sanksi">
								<h2 class="card-inside-title">Profile</h2>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="name" id="name" value="<?php echo $data['name']; ?>" disabled />
												<label class="form-label">Nama</label>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="csdm" id="csdm" value="<?php echo $data['csdm']; ?>" disabled/>
												<label class="form-label">CSDM</label>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group form-float form-group-sm">
											<div class="form-line" style="background-color: rgba(228,228,228,0.3);">
												<input type="text" class="form-control update_sdm" name="pendidikan" id="pendidikan" value="<?php echo $data['status_terakhir']; ?>" disabled/>
												<label class="form-label">Status terakhir</label>
											</div>
										</div>
									</div>
								</div>

								<h2 class="card-inside-title">Pembinaan</h2>
								<div class="row clearfix">
									<div class="col-sm-3">
										<div class="form-group form-float form-group-sm">
											<div class="form-line focused" id="bs_datepicker_container"> 
												<input type="text" class="form-control" name="tgl_kejadian" id="tgl_kejadian" value="" required />
												<label class="form-label">Tanggal Kejadian</label>
											</div>
										</div>
									</div>
									<div class="col-sm-9">
										<div class="form-group form-float form-group-sm" style="visibility: hidden;">
											<div class="form-line ">
												<input type="text" class="form-control " />
												<label class="form-label">HIDDEN</label>
											</div>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group form-float form-group-sm">
											<div class="form-line">
												<select class="form-control show-tick" data-live-search="true" id="jenis_kesalahan" name="jenis_kesalahan">
													<option value="KPI">KPI</option>
													<option value="ROG">ROG</option>
												</select>
												<label class="form-label">Jenis Kesalahan</label>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group form-float form-group-sm">
											<div class="form-line">
												<select class="form-control show-tick" data-live-search="true" data-size="4" id="kategori_kesalahan" name="kategori_kesalahan">
												</select>
												<label class="form-label">Kategori Kesalahan</label>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group form-float form-group-sm">
											<div class="form-line">
												<select class="form-control show-tick" data-live-search="true" data-size="4" id="id_kesalahan" name="id_kesalahan">
												</select>
												<label class="form-label">Deskripsi Kesalahan</label>
											</div>
										</div>
									</div>

									<div class="col-sm-12">
										<div class="form-group form-float form-group-sm">
											<div class="form-line">
												<input type="text" class="form-control" name="uraian" id="uraian" value="" required/>
												<label class="form-label">Uraian</label>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group form-float form-group-sm">
											<div class="form-line">
												<input type="text" class="form-control" name="rekomendasi" id="rekomendasi" value="" required />
												<label class="form-label">Rekomendasi</label>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<input type="hidden" class="form-control update_sdm" name="id_pmb_kesalahan" id="id_pmb_kesalahan" value="0" />

										<input type="hidden" class="form-control update_sdm" name="id_pembinaan" id="id_pembinaan" value="<?php echo $data['id_pembinaan']; ?>" />

										<input type="hidden" class="form-control update_sdm" name="flow_id" id="flow_id" value="<?php echo $data['flow_id']; ?>" />

										<input type="hidden" class="form-control update_sdm" name="nilai" id="nilai" value="" />

										<input type="hidden" class="form-control update_sdm" name="id_created" id="id_created" value="<?php echo $this->session->id; ?>" />

										<input type="hidden" class="form-control update_sdm" name="id_agent" id="id_agent" value="<?php echo $data['id_agent']; ?>" />

										<button type="submit" class="btn btn-primary m-t-15 waves-effect" id="button_sumbit"><i class="material-icons">file_upload</i> SUBMIT</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		<!-- #END# Basic Examples -->
	</div>
</section>
