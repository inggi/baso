<section class="content">
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							UPLOAD JADWAL ECARE
						</h2>
					</div>
					<div class="body">
						<form class="form-horizontal" id="do_upload_jadwal">
							<div class="row">
								<div class="col-lg-2 col-md-2 form-control-label">
									<div class="form-group">
											<label for="password_2">File</label>
									</div>
								</div>
								<div class="col-lg-4 col-md-4">
									<div class="form-group">
										<div class="form-line">
											<input type="file" id="file_upload" class="form-control" placeholder="" name="file_upload">
										</div>
										<div class="help-info" style="color:red">*Ket: Format file harus sama dengan contoh yang disediakan</div>
									</div>
								</div>
								<div class="col-lg-2 col-md-2 hidden">
									<div class="form-group">
										<div class="input-group spinner" data-trigger="spinner">
											<div class="form-line">
												<input type="text" class="form-control text-center" value="1" data-rule="quantity" id="sheet" nama="sheet" data-max="10">
											</div>
											<div class="help-info">Jumlah Sheet</div>
											<span class="input-group-addon">
	                                            <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
	                                            <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
	                                        </span>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
									<button type="submit" class="btn btn-primary m-t-15 waves-effect" id="button_doupload"><i class="material-icons">file_upload</i> SUBMIT</button>

									<a href="<?php echo base_url(); ?>document/Format_Upload_Jadwal.xls" class="btn btn-success m-t-15 waves-effect" id="a_doupload"><i class="material-icons">file_download</i> Format Excel</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2> KODE POLA JADWAL </h2>
					</div>
					<div class="body">
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<div class="table-responsive">
									<table id="table-jadwal-pegawai" class="table table-striped">
										<thead>
											<tr id="head_table">
												<th>NO</th>
												<th>Pola</th>
												<th>Masuk</th>
												<th>Pulang</th>
												<th>Keterangan</th>
											</tr>
										</thead>
										<tbody id="tbody_jadwal1">
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="table-responsive">
									<table id="table-jadwal-pegawai" class="table table-striped">
										<thead>
											<tr id="head_table">
												<th>NO</th>
												<th>Pola</th>
												<th>Masuk</th>
												<th>Pulang</th>
												<th>Keterangan</th>
											</tr>
										</thead>
										<tbody id="tbody_jadwal2">
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>