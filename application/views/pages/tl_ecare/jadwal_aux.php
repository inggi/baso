<section class="content">

<!-- Uploader -->
		<div class="row clearfix hidden" id="form_upload">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2 class="hidden" id="title_upload">
							Upload
						</h2>
						<h2 class="hidden" id="title_reupload">
							Reupload
						</h2>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown" id="btn-close_form_upload" style=" cursor: pointer;">
								<i class="material-icons">close</i>
							</li>
						</ul>
					</div>

					<div class="body">
						<form class="form-horizontal" id="do_upload">
							<input type="hidden" name="id_file" value="0" id="id_file">
							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="email_address_2">Nama Dokumen</label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line" >
											<input type="text" class="form-control" placeholder="Nama Dokumen" name="nama_document" id="nama_document" />
											<!-- <select class="form-control show-tick" id="s_nodocument" name="id_nodocument"> -->
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="email_address_2">Contract Center</label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" id="s_contactcenter" name="id_contactcenter">
												<option value="">-- Pilih Contact Center --</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="password_2">Periode</label>
								</div>
								<div class="col-lg-3 col-md-3">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control" id="bulan" name="bulan">
												<option value="">-- Pilih Bulan --</option>
												<option value="1">Januari</option>
												<option value="2">Febuari</option>
												<option value="3">Maret</option>
												<option value="4">Mei</option>
												<option value="5">April</option>
												<option value="6">Juni</option>
												<option value="7">Juli</option>
												<option value="8">Agustus</option>
												<option value="9">September</option>
												<option value="10">Oktober</option>
												<option value="11">November</option>
												<option value="12">Desember</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-lg-2 col-md-2">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control" id="tahun" name="tahun">
												<option value="">--Pilih Tahun--</option>
												<option value="2015">2015</option>
												<option value="2016">2016</option>
												<option value="2017">2017</option>
												<option value="2018">2018</option>
												<option value="2019">2019</option>
												<option value="2020">2020</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-2 col-md-2 form-control-label">
									<div class="form-group">
											<label for="password_2">Uploaded</label>
									</div>
								</div>
								<div class="col-lg-3 col-md-3">
									<div class="form-group">
										<div class="form-line" id="bs_datepicker_container">
											<input type="date" class="form-control" placeholder="Please choose uploaded..." name='uploaded' id="uploaded">
										</div>
									</div>
								</div>
							</div>
                            <div class="row hidden" id="input-note">
                                <div class="col-lg-2 col-md-2 form-control-label">
                                    <div class="form-group">
                                            <label for="password_2">Note</label>
                                    </div>
                                </div>
                                <div class="col-lg-10 col-md-10">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea id="note" name="note" rows="4" class="form-control no-resize" placeholder="Please type what you want..." disabled></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
							<div class="row">
								<div class="col-lg-2 col-md-2 form-control-label">
									<div class="form-group">
											<label for="password_2">File</label>
									</div>
								</div>
								<div class="col-lg-3 col-md-3">
									<div class="form-group">
										<div class="form-line">
											<input type="file" id="file" class="form-control" placeholder="Enter your password"name="file_document">
										</div>
									</div>
								</div>
							</div>
							<!-- <div class="row">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
								</div>
								<div class="col-lg-4 col-md-4 col-sm-8 col-xs-7">
									<div class="form-group">
											<input type="checkbox" id="status_arsip" name="status_arsip" class="filled-in"/>
											<label for="status_arsip">Arsip</label>
									</div>
								</div>
							</div> -->
							<div class="row clearfix">
								<div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
									<button type="submit" class="btn btn-primary m-t-15 waves-effect" id="button_doupload"><i class="material-icons">file_upload</i> SUBMIT</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->
	
	<div class="row clearfix" id="form-filter_admin_document">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>
						<b>FILTER</b>
					</h2>
					<ul class="header-dropdown m-r--5">
						<li class="dropdown">
							<a href="javascript:void(0);" class="dropdown-toggle" id="btn-upload_document">
								<i class="material-icons">more_vert</i>
							</a>
							<ul class="dropdown-menu pull-right">
								<li><a id="btn-upload_document">Upload</a></li>
								<li><a id="btn-filter_document_unit">Filter</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="body">
					<form>
						<div class="row clearfix">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<div class="form-group">
									<div class="form-line">
										<select class="form-control show-tick" data-live-search="true" data-size="6" id="jabatan">
											<option value='0' disabled>-- Pilih Jabatan --</option>
											<option value='search' disabled>Search.. </option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
								<button type="button" class="btn btn-primary waves-effect" id="form-filter_report">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div id="tampil"></div>
				<!-- <iframe src="<?php //echo base_url();?>/document/New folder/REPORT PRESENSI TANGGAL '2019-07-01' SD '2019-08-03'.htm" name="kanan" width="100%" height="1000px"> -->
			</div>
		</div>
	</div>
</section>