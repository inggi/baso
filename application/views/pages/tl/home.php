<section class="content">
		<div class="container-fluid">
			<!-- Counter Examples -->
			<div class="row clearfix">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h3>
						Contract
					</h3>	
					<div class="row">
						<a href="<?php echo base_url();?>tl/content/endkontrak">
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
								<div class="info-box-3 bg-deep-orange hover-zoom-effect" >
									<div class="icon">
										<i class="material-icons">assignment_ind</i>
									</div>
									<div class="content">
										<div class="text">End Contract < 60 Day</div>
										<div class="number"><?php echo $data['end_kontrak']; ?></div>
									</div>
								</div>
							</div>
						</a>
						<a href="<?php echo base_url();?>tl/content/outofcontract">
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
								<div class="info-box-3 bg-orange hover-zoom-effect" >
									<div class="icon">
										<i class="material-icons">assignment_late</i>
									</div>
									<div class="content">
										<div class="text">Contract is Out of Date</div>
										<div class="number"><?php echo $data['outofdate']; ?></div>
									</div>
								</div>
							</div>
						</a>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-green hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">assignment</i>
								</div>
								<div class="content">
									<div class="text">Perpanjang kontrak</div>
									<div class="number"><?php echo $data['perpanjang_kontrak']; ?></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-amber hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">done_all</i>
								</div>
								<div class="content">
									<div class="text">Waiting Approval HR</div>
									<div class="number"><?php echo $data['waiting_aproval_hr']; ?></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row clearfix">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h3>
						Resign
					</h3>	
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-indigo hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">assignment_late</i>
								</div>
								<div class="content">
									<div class="text">Resign On The Spot</div>
									<div class="number"><?php echo $data['onthespot']; ?></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-teal hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">assignment_late</i>
								</div>
								<div class="content">
									<div class="text">Resign One Month Notice</div>
									<div class="number"><?php echo $data['onemonthnotice']; ?></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-amber hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">done_all</i>
								</div>
								<div class="content">
									<div class="text">Waiting Approval HR</div>
									<div class="number"><?php echo $data['aproval_resign']; ?></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row clearfix">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h3>
						TOTAL KARYAWAN
					</h3>	
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="info-box-3 bg-green hover-zoom-effect" >
								<div class="icon">
									<i class="material-icons">assignment_ind</i>
								</div>
								<div class="content">
									<div class="text">TOTAL UNDERTEAM</div>
									<div class="number"><?php echo $data['total_underteam']; ?></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</section>