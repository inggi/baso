<section class="content">
	<div class="container-fluid">
		<!-- Basic Examples -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<?php echo $data['name'];?> - <?php echo $data['csdm'];?>
							<!-- <small>Perubahan Form Update</small> -->
						</h2>
						
					</div>
					<div class="body">
					<form id="update_sdm_form" method="POST">
						<h2 class="card-inside-title">Profile</h2>
						<input type="hidden" class="form-control update_sdm" name="id_sdm" id="id_sdm" value="<?php echo $data['id'];?>"/>
						<div class="row clearfix">
							<div class="col-sm-12">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="name" id="name" value="<?php echo $data['name'];?>" readonly/>
										<label class="form-label">Nama karyawan</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="gender" id="gender" value="<?php echo $data['gender'];?>" readonly/>
										<label class="form-label">GENDER</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="no_hp" id="no_hp" value="<?php echo $data['no_hp'];?>" readonly/>
										<label class="form-label">NO. HP</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="agama" id="agama" value="<?php echo $data['agama'];?>" readonly/>
										<label class="form-label">Agama</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="tempat_lahir" id="tempat_lahir" value="<?php echo $data['tempat_lahir'];?>" readonly/>
										<label class="form-label">Tempat Lahir</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="date" data-date-format="yyyy-mm-dd" class="form-control update_sdm" name="tgl_lahir" id="tgl_lahir" value="<?php echo $data['tgl_lahir'];?>" readonly/>
										<label class="form-label">Tanggal Lahir</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="pendidikan" id="pendidikan" value="<?php echo $data['pendidikan'];?>" readonly>
										<label class="form-label">Pendidikan</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="nik_hris" id="nik_hris" value="<?php echo $data['nik_hris'];?>" readonly/>
										<label class="form-label">NIK HRIS</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="csdm" id="csdm" value="<?php echo $data['csdm'];?>" readonly/>
										<label class="form-label">CSDM</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="perner" id="perner" value="<?php echo $data['perner'];?>" readonly/>
										<label class="form-label">PERNER</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="nama_online" id="nama_online" value="<?php echo $data['nama_online'];?>" readonly/>
										<label class="form-label">Nama Online</label>
									</div>
								</div>
							</div>
							<!-- <div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="batch" id="batch" value=""/>
										<label class="form-label">Batch</label>
									</div>
								</div>
							</div> -->	
						</div>

						<h2 class="card-inside-title">Karir</h2>
						<div class="row clearfix">
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="jabatan" id="jabatan" value="<?php echo $data['jabatan'];?>" readonly/>
										<label class="form-label">Jabatan</label>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="jabatan_level" id="jabatan_level" value="<?php echo $data['jabatan_level2'];?>" readonly/>
										<label class="form-label">Jabatan Level</label>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="unit" id="unit" value="<?php echo $data['unit'];?>" readonly/>
										<label class="form-label">Unit</label>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="ctp" id="ctp" value="<?php echo $data['ctp'];?>" readonly/>
										<label class="form-label">CTP</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="spv" id="spv" value="<?php echo $data['name_spv'];?>" readonly/>
										<label class="form-label">Supervisor</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="tl" id="tl" value="<?php echo $data['name_tl'];?>" readonly/>
										<label class="form-label">Team Leader</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm" style="visibility: hidden;">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" />
										<label class="form-label">HIDDEN</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="skill_layanan" id="skill_layanan" value="<?php echo $data['skill_layanan'];?>" readonly/>
										<label class="form-label">Skill Layanan</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="skema_agent" id="skema_agent" value="<?php echo $data['skema_agent'];?>" readonly/>
										<label class="form-label">Skema Agent</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="status_pekerja" id="status_pekerja" value="<?php echo $data['status_pekerja'];?>" readonly/>
										<label class="form-label">Status Pekerja</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="date" data-date-format="yyyy-mm-dd" class="form-control update_sdm" name="join_insani" id="join_insani" value="<?php echo $data['join_insani'];?>" readonly/>
										<label class="form-label">Join di Insani</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="date" data-date-format="yyyy-mm-dd" class="form-control update_sdm" name="tgl_awal_kontrak" id="tgl_awal_kontrak" value="<?php echo $data['tgl_awal_kontrak'];?>" readonly/>
										<label class="form-label">Tanggal Awal Kontrak</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="date" data-date-format="yyyy-mm-dd" class="form-control update_sdm" name="tgl_akhir_kontrak" id="tgl_akhir_kontrak" value="<?php echo $data['tgl_akhir_kontrak'];?>" readonly/>
										<label class="form-label">Tanggal Akhir Kontrak</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="ppjp" id="ppjp" value="<?php echo $data['ppjp'];?>" readonly/>
										<label class="form-label">PPJP</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="batch" id="batch" value="<?php echo $data['batch'];?>" readonly/>
										<label class="form-label">Batch</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="date" data-date-format="yyyy-mm-dd" class="form-control update_sdm" name="join_unit" id="join_unit" value="<?php echo $data['join_unit'];?>" readonly/>
										<label class="form-label">Join Unit</label>
									</div>
								</div>
							</div>
						</div>

						<h2 class="card-inside-title">No. Jaminan Sosial</h2>
						<div class="row clearfix">
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="bpjs_tk" id="bpjs_tk" value="<?php echo $data['bpjs_tk'];?>" readonly/>
										<label class="form-label">BPJS TK</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="bpjs_kesehatan" id="bpjs_kesehatan" value="<?php echo $data['bpjs_kesehatan'];?>" readonly/>
										<label class="form-label">BPJS Kesehatan</label>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group form-float form-group-sm">
									<div class="form-line">
										<input type="text" class="form-control update_sdm" name="npwp" id="npwp" value="<?php echo $data['npwp'];?>" readonly/>
										<label class="form-label">NPWP</label>
									</div>
								</div>
							</div>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->
	</div>
</section>
<!-- <div class="alert alert-success alert-dismissible alertify-logs hidden" role="alert" id="success-update">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div id='message'>
    	Berhasil simpan perubahan.
    </div>	
</div> -->
