<section class="content">
	<div class="container-fluid">
		
		<!-- Filter -->
		<div class="row clearfix show" id="form-filter">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>FILTER</b>
						</h2>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown" id="close_filter" style=" cursor: pointer;">
								<i class="material-icons">close</i>
							</li>
						</ul>
					</div>

					<div class="body">
						<form>
							<div class="row clearfix">
								<div class="input-daterange" id="bs_datepicker_range_container">
									<div class="col-sm-3">
										<div class="form-group form-float form-group-sm">
											<div class="form-line focused"> 
												<input type="text" class="form-control" data-date-format="yyyy-mm-dd" name="date_start" id="date_start"/>
												<label class="form-label">Created (From)</label>
											</div>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group form-float form-group-sm">
											<div class="form-line focused"> 
												<input type="text" class="form-control" data-date-format="yyyy-mm-dd" name="date_end" id="date_end"/>
												<label class="form-label">Created (To)</label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group form-float form-group-sm">
										<div class="form-line">
											<select class="form-control show-tick" data-live-search="true" data-size="6" id="reason_update" name="reason_update">
												<option value='0' disabled>-- Pilih --</option>
												<option value='All' selected>All</option>
												<option value='PERPANJANG KONTRAK'>PERPANJANG KONTRAK</option>
												<option value='MUTASI'>MUTASI</option>
												<option value='ROTASI'>ROTASI</option>
												<option value='RESIGN ONE MONTH NOTICE'>RESIGN ONE MONTH NOTICE</option>
												<option value='RESIGN ON THE SPOT'>RESIGN ON THE SPOT</option>
												<option value='END KONTRAK'>END KONTRAK</option>
												<option value='PENGEMBALIAN AGENT'>PENGEMBALIAN AGENT</option>
											</select>
											<label class="form-label">Reason</label>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group form-float form-group-sm">
										<div class="form-line">
											<input type="hidden" class="form-control update_sdm" name="temp_tl" id="temp_tl" value=""/>
											<select class="form-control show-tick update_sdm" data-live-search="true" data-size="6" id="id_tl" name="id_tl">
												<!-- <option value='0' disabled>-- Pilih Team Leader --</option> -->
												<option value='All' selected>All</option>
											</select>
											<label class="form-label">Team Leader</label>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<button type="button" class="btn btn-primary waves-effect" id="form-filter_report"><i class="material-icons">filter_list</i> <span>FILTER</span></button>
									<button type="button" class="btn btn-success btn-lg m-l-15 waves-effect" id="export">EXPORT</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->

		<!-- Basic Examples -->
		<div class="row clearfix" id="jadwal">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>HISTORY KARWAYAN</b>
						</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <!-- <li><a href="javascript:void(0);" id="show_upload">Upload</a></li> -->
                                    <li><a href="javascript:void(0);" id="show_filter">Filter</a></li>
                                </ul>
                            </li>
                        </ul>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table id="table-pegawai" class="table table-striped table-bordered table-bordered">
								<thead>
									<tr id="head_table" height="40px" class="text-center info">
										<th class="success">CSDM</th>
										<th class="success">Name</th>
										<th class="success">Team Leader</th>
										<th class="success">Reason Update</th>
										<th class="success">Tanggal Update</th>
										<th class="success" width="2">Action</th>
									</tr>
								</thead>
								<tbody id="tbody">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->
	</div>
</section>
