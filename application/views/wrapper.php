<!DOCTYPE html>
<html>

<?php require_once('layout/head.php') ?>

<body class="theme-black">

	<?php //require_once('layout/page_loader.php') ?>

	<?php require_once('layout/header.php') ?>

	<section>
		<?php require_once('layout/sidebar_left.php') ?>
	</section>

	<?php require_once('pages/'.$view.'.php') ?>

	<?php require_once('layout/script.php') ?>
</body>

</html>
