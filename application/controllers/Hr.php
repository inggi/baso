<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hr extends CI_Controller {

	function __construct() {
        parent::__construct();
		if ($this->session->logged_in != TRUE || $this->session->ceklogin !== "user_hr") {
			redirect('login');
		}
    }

	public function content($page = 'home')
	{
		$this->load->model('ms_sdm');
		$row = $this->ms_sdm->get_total_home_userhr()->row_array();
		$data = array(
			'data' => $row,
			'level' => 'hr',
			'view' => 'hr/'.$page,
			'page' => $page, 
			'menu' => 'menu_hr');
		$this->load->view('wrapper', $data);
	}

	public function update_sdm($id_sdm){
		$this->load->model('ms_sdm');
		$data_sdm = $this->ms_sdm->get_sdm_byid($id_sdm)->row_array();
		$data = array(
			'data' => $data_sdm,
			'level' => 'hr',
			'view' => 'hr/'.'update_sdm',
			'page' => 'update_sdm', 
			'menu' => 'menu_hr');
		$this->load->view('wrapper', $data);
	}

	public function update_sdm2($id_sdm){
		$this->load->model('ms_sdm');
		$data_sdm = $this->ms_sdm->get_sdm_byid($id_sdm)->row_array();
		$data = array(
			'data' => $data_sdm,
			'level' => 'hr',
			'view' => 'hr/'.'update_sdm2',
			'page' => 'update_sdm2', 
			'menu' => 'menu_hr');
		$this->load->view('wrapper', $data);
	}

	public function profile(){
		$data = array(
			'level' => $this->session->jabatan_level,
			'view' => 'profile',
			'page' => 'profile', 
			'menu' => 'menu_hr');
		$this->load->view('wrapper', $data);
	}

	// public function action1($url){
	// 	$data_sdm = $url;
	// 	$data = array(
	// 		'data' => $data_sdm,
	// 		'level' => 'hr',
	// 		'view' => 'hr/'.'action1',
	// 		'page' => 'action1', 
	// 		'menu' => 'menu_hr');
	// 	$this->load->view('wrapper', $data);
	// }
}
