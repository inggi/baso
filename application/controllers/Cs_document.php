<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cs_document extends CI_Controller {

	function __construct() {
        parent::__construct();
		if ($this->session->logged_in != TRUE) {
			redirect('login');
		}

		$this->load->model('ms_document');
    }

	public function do_upload_document()
	{
		$id_sdm= $this->input->post('id_sdm');
		$csdm= $this->input->post('csdm');
		$nama= $this->input->post('name');
		$notes= $this->input->post('notes');
		$kategori_file= $this->input->post('kategori_file');
        $date = date("His_dmY", time());

        $pathfile1 = $_FILES["file_document1"]["name"];
        $namefile1 = current((explode(".", $pathfile1)));
        $pathfile2 = $_FILES["file_document2"]["name"];
        $namefile2 = current((explode(".", $pathfile2)));
        $pathfile3 = $_FILES["file_document3"]["name"];
        $namefile3 = current((explode(".", $pathfile3)));
        
        $config['upload_path']="./document/$csdm"; //path folder file upload
        $config['allowed_types']='gif|jpg|png|jpeg|bmp|ppt|pptx|xls|xlsx|doc|docx|pdf|rar|zip'; //type file yang boleh di upload
		// $config['allowed_types']=$ext; //type file yang boleh di upload
        // $config['file_name'] = $pathfile1; //pathfile1  
        $config['encrypt_name'] = FAlSE;
        $this->load->library('upload',$config); //call library upload 
        
        for ($i=1; $i <=3 ; $i++) { 
            if(!empty($_FILES['file_document'.$i]['name'])){
            	$pathfile = $_FILES['file_document'.$i]['name'];
		        if (is_dir("document/$csdm/")) { //cek if path exist
        			$this->upload->do_upload('file_document'.$i); //upload file
        			// $namefile = $this->upload->data('file_name');
        			// $pathfile;
        			// echo $pathfile;
		        	// if(!$this->upload->do_upload('file_document'.$i))
           //          	$this->upload->display_errors();
           //          else
           //          	echo $pathfile;  
        		}else {
		        	mkdir("document/$csdm/"); //create folder
        			$this->upload->do_upload('file_document'.$i); //upload file
		        	// if(!$this->upload->do_upload('file_document'.$i))
           //          	$this->upload->display_errors();
           //          else
           //          	echo $pathfile;
		        }
            }
        }
        // array($pathfile);
        // print_r($pathfile);  die;
        // echo $pathfile;

      
        $result = $this->ms_document->insert_document($id_sdm, $csdm, $namefile1, $pathfile1, $namefile2, $pathfile2, $namefile3, $pathfile3, $notes, $kategori_file);
            
        if($result == 1){
        	echo json_encode(array('hasil'=>"success"));
			// echo json_encode(array('hasil'=>'success', 'url'=>'page/content'));
		}
	}

	public function do_reupload_document()
	{
		$id_doc= $this->input->post('id_doc');
		$id_sdm= $this->input->post('id_sdm2');
		$csdm= $this->input->post('csdm2');
		$nama= $this->input->post('name2');
		$notes= $this->input->post('notes2');
		$kategori_file= $this->input->post('kategori_file');
        $date = date("His_dmY", time());

        $name = $_FILES["file_document"]["name"];
		$ext = end((explode(".", $name)));
		$namefile = current((explode(".", $name)));

		// $config['upload_path']="".base_url()."/document"; //path folder file upload
		$config['upload_path']="./document/$csdm"; // $config['allowed_types']='pdf|docx|doc|xlsx|xls'; //type file yang boleh di upload
        $config['allowed_types']=$ext; //type file yang boleh di upload
        $config['file_name'] = $csdm.'_'.$nama.'_'.$date.'.'.$ext;
        
        $this->load->library('upload',$config); //call library upload 
        // echo $namefile;die;
		// print_r($_POST);die;
		// print_r($_POST);
        
        if (is_dir("document/$csdm/")) { //cek if path exist
        	$this->upload->do_upload("file_document"); //upload file
        }
        else {
        	mkdir("document/$csdm/"); //create folder
        	$this->upload->do_upload("file_document"); //upload file
        }
        
        if ($name != null) {
        	$result = $this->ms_document->reupload_document($id_doc, $namefile, $this->upload->data('file_name'), $notes, $kategori_file);
        } else{
        	$result = $this->ms_document->update_note_document($id_doc, $notes, $kategori_file);
        }
            
        if($result == 1){
        	echo json_encode(array('hasil'=>"success"));
			// echo json_encode(array('hasil'=>'success', 'url'=>'page/content'));
		}
	}

	function get_dokumenbycsdm(){
		$csdm = $this->input->post('csdm');
		$query = $this->ms_document->get_dokumenbycsdm($csdm)->row_array();
		$data = $query;
		echo json_encode($data);
	}

	public function do_upload_document_old()
	{
		$id_sdm= $this->input->post('id_sdm');
		$csdm= $this->input->post('csdm');
		$nama= $this->input->post('name');
		$notes= $this->input->post('notes');
		$kategori_file= $this->input->post('kategori_file');
        $date = date("His_dmY", time());

        $name = $_FILES["file_document"]["name"];
		$ext = end((explode(".", $name)));
		$namefile = current((explode(".", $name)));

		// $config['upload_path']="".base_url()."/document"; //path folder file upload
		$config['upload_path']="./document/$csdm"; // $config['allowed_types']='pdf|docx|doc|xlsx|xls'; //type file yang boleh di upload
        $config['allowed_types']=$ext; //type file yang boleh di upload
        $config['file_name'] = $csdm.'_'.$nama.'_'.$date.'.'.$ext;
        
        $this->load->library('upload',$config); //call library upload 
        // echo $namefile;die;
		// print_r($_POST);die;
		// print_r($_POST);
        
        if (is_dir("document/$csdm/")) { //cek if path exist
        	$this->upload->do_upload("file_document"); //upload file
        }
        else {
        	mkdir("document/$csdm/"); //create folder
        	$this->upload->do_upload("file_document"); //upload file
        }
        
        $result = $this->ms_document->insert_document($id_sdm, $csdm, $namefile, $this->upload->data('file_name'), $notes, $kategori_file);
            
        if($result == 1){
        	echo json_encode(array('hasil'=>"success"));
			// echo json_encode(array('hasil'=>'success', 'url'=>'page/content'));
		}
	}

	public function do_reupload_document_old()
	{
		$id_doc= $this->input->post('id_doc');
		$id_sdm= $this->input->post('id_sdm2');
		$csdm= $this->input->post('csdm2');
		$nama= $this->input->post('name2');
		$notes= $this->input->post('notes2');
		$kategori_file= $this->input->post('kategori_file');
        $date = date("His_dmY", time());

        $name = $_FILES["file_document"]["name"];
		$ext = end((explode(".", $name)));
		$namefile = current((explode(".", $name)));

		// $config['upload_path']="".base_url()."/document"; //path folder file upload
		$config['upload_path']="./document/$csdm"; // $config['allowed_types']='pdf|docx|doc|xlsx|xls'; //type file yang boleh di upload
        $config['allowed_types']=$ext; //type file yang boleh di upload
        $config['file_name'] = $csdm.'_'.$nama.'_'.$date.'.'.$ext;
        
        $this->load->library('upload',$config); //call library upload 
        // echo $namefile;die;
		// print_r($_POST);die;
		// print_r($_POST);
        
        if (is_dir("document/$csdm/")) { //cek if path exist
        	$this->upload->do_upload("file_document"); //upload file
        }
        else {
        	mkdir("document/$csdm/"); //create folder
        	$this->upload->do_upload("file_document"); //upload file
        }
        
        if ($name != null) {
        	$result = $this->ms_document->reupload_document($id_doc, $namefile, $this->upload->data('file_name'), $notes, $kategori_file);
        } else{
        	$result = $this->ms_document->update_note_document($id_doc, $notes, $kategori_file);
        }
            
        if($result == 1){
        	echo json_encode(array('hasil'=>"success"));
			// echo json_encode(array('hasil'=>'success', 'url'=>'page/content'));
		}
	}

}
