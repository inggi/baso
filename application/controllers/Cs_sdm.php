<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cs_sdm extends CI_Controller {

	function __construct() {
        parent::__construct();
		if ($this->session->logged_in != TRUE) {
			redirect('login');
		}

		$this->load->model('ms_sdm');
    }

	public function get_sdm()
	{	
		$query = '';
		if($this->session->jabatan == 'ADMIN SUPPORT' || $this->session->jabatan  =='HR SUPPORT'){
			$status_sdm = $this->input->post('status_sdm');
			$ctp = $this->input->post('ctp');
			$jabatan = $this->input->post('jabatan');
			$unit = $this->input->post('unit');
			$id_tl = $this->input->post('id_tl');
			$id_spv = $this->input->post('id_spv');
			$date_start = date('Y-m-d', strtotime($this->input->post('date_start')));
			$date_end = date('Y-m-d', strtotime($this->input->post('date_end')));

			$a = " AND a.status_sdm = '".$status_sdm."'";

			if($date_start !='1970-01-01'){
				$a.=" AND DATE_FORMAT(a.tgl_awal_kontrak, '%Y-%m-%d') BETWEEN '".$date_start."' AND '".$date_end."'";
			}
			if($ctp!='All'){
				$a.=" AND a.ctp LIKE '".$ctp."'";
			}
			if($jabatan!='All'){
				$a.=" AND a.jabatan LIKE '".$jabatan."'";
			}
			if($unit!='All'){
				$a.=" AND a.unit LIKE '".$unit."'";
			}
			if($id_tl!='All'){
				$a.=" AND a.id_tl = '".$id_tl."'";
			}
			if($id_spv!='All'){
				$a.=" AND a.id_spv = '".$id_spv."'";
			}
			// echo $id_tl.'a';echo $id_spv;
			$query=$this->ms_sdm->get_sdm($a);
		}
		else if($this->session->jabatan_level == 'SPV'){
			$query=$this->ms_sdm->get_sdm_byspv();
		}else{
			$query=$this->ms_sdm->get_sdm_bytl();
		}
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_viewsdm()
	{	
			$status_sdm = $this->input->post('status_sdm');
			$ctp = $this->input->post('ctp');
			$jabatan = $this->input->post('jabatan');
			$unit = $this->input->post('unit');
			$id_tl = $this->input->post('id_tl');
			$id_spv = $this->input->post('id_spv');
			$date_start = date('Y-m-d', strtotime($this->input->post('date_start')));
			$date_end = date('Y-m-d', strtotime($this->input->post('date_end')));

			$a = " AND a.status_sdm = '".$status_sdm."'";

			if($date_start !='1970-01-01'){
				$a.=" AND DATE_FORMAT(a.tgl_awal_kontrak, '%Y-%m-%d') BETWEEN '".$date_start."' AND '".$date_end."'";
			}
			if($ctp!='All'){
				$a.=" AND a.ctp LIKE '".$ctp."'";
			}
			if($jabatan!='All'){
				$a.=" AND a.jabatan LIKE '".$jabatan."'";
			}
			if($unit!='All'){
				$a.=" AND a.unit LIKE '".$unit."'";
			}
			if($id_tl!='All'){
				$a.=" AND a.id_tl = '".$id_tl."'";
			}
			if($id_spv!='All'){
				$a.=" AND a.id_spv = '".$id_spv."'";
			}
			
		$query=$this->ms_sdm->get_sdm($a);
		
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_sdm_byid()
	{	
		$id = $this->input->post('id_tl');
		// echo $id;die;
		$query=$this->ms_sdm->get_sdm_byid($id);
		
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_tl()
	{
		// $query=$this->ms_sdm->get_tl();
		if($this->session->jabatan_level == "SPV"){
			$query=$this->ms_sdm->get_tl_userspv($this->session->id);
		}else{
			$query=$this->ms_sdm->get_tl();	
		}

		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_home_totalkaryawan()
	{
		// $query=$this->ms_sdm->get_tl();
		if($this->session->jabatan_level == "SPV"){
			$query=$this->ms_sdm->get_home_totalkaryawan_userspv($this->session->id);
		}else{
			$query=$this->ms_sdm->get_tl();	
		}

		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_home_totalkaryawanbyjabatan()
	{
		$query=$this->ms_sdm->get_home_totalkaryawanbyjabatan();
		// if($this->session->jabatan_level == "SPV"){
		// 	$query=$this->ms_sdm->get_home_totalkaryawan_userspv($this->session->id);
		// }else{
		// 	$query=$this->ms_sdm->get_tl();	
		// }

		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_tl_all()
	{
		$query=$this->ms_sdm->get_tl();	
		
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_spv()
	{
		$query=$this->ms_sdm->get_spv();
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_jabatan()
	{
		$query=$this->ms_sdm->get_jabatan();
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i]= $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_selectspvbytl()
	{
		$id_tl = $this->input->post('id_tl');

		if($id_tl!='All' || $id_tl != '' || $id_tl != ' ' || $id_tl != null || $id_tl != '0' ){
			$query=$this->ms_sdm->get_sdm_byid($id_tl);
		}else{
			$query=$this->ms_sdm->get_spv();	
		}	
		
		// $query=$this->ms_sdm->get_spv();	

		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	// public function get_selectjabatanlevel()
	// {	
	// 	$jabatan_level = $this->input->post('jabatan_level');

	// 	$query = '';
	// 	if($jabatan_level == 'STAFF'){
	// 		$query=$this->ms_sdm->get_sdm($a);
	// 	}
	// 	else if($this->session->jabatan_level == 'SPV'){
	// 		$query=$this->ms_sdm->get_sdm_byspv();
	// 	}else{
	// 		$query=$this->ms_sdm->get_sdm_bytl();
	// 	}
	// 	$data['data']= array();
	// 	$i=0;

	// 	foreach ($query->result_array() as $myRow) {
	// 		$data['data'][$i] = $myRow;
	// 		$i++;
	// 	}

	// 	echo json_encode($data);
	// }

	public function get_endcontract()
	{	
		$query = '';
		if($this->session->jabatan == 'ADMIN SUPPORT' || $this->session->jabatan  =='HR SUPPORT'){
			$query=$this->ms_sdm->get_endcontract_userhr();
		}
		else if($this->session->jabatan_level == 'SPV'){
			$query=$this->ms_sdm->get_endcontract_userspv();
		}
		else{
			$query=$this->ms_sdm->get_endcontract_usertl();
		}

		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_outofcontract()
	{	
		$query = '';
		if($this->session->jabatan == 'ADMIN SUPPORT' || $this->session->jabatan  =='HR SUPPORT'){
			$query=$this->ms_sdm->get_outofcontract_userhr();
		}
		else if($this->session->jabatan_level == 'SPV'){
			$query=$this->ms_sdm->get_outofcontract_userspv();
		}
		else{
			$query=$this->ms_sdm->get_outofcontract_usertl();
		}

		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_rekomendasi_spvtl()
	{	
		$query=$this->ms_sdm->get_rekomendasi_spvtl();
		
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_rekomendasi_spvtl2()
	{	
		$pathurl = $this->input->post('pathurl');
		// echo $pathurl;die;
		if($pathurl == '1'){
			// echo $pathurl;die;
			$kategori_file="RESIGN ON THE SPOT";
			$query=$this->ms_sdm->get_rekomendasi_spvtl2($kategori_file);
		}
		else if($pathurl == '2'){
			$kategori_file="RESIGN ONE MONTH NOTICE";
			$query=$this->ms_sdm->get_rekomendasi_spvtl2($kategori_file);
		}
		else if($pathurl == '3'){
			$kategori_file="CONTRACT";
			$query=$this->ms_sdm->get_rekomendasi_spvtl2($kategori_file);
		}
		else if($pathurl == '4'){
			$kategori_file="CUTI MELAHIRKAN";
			$query=$this->ms_sdm->get_rekomendasi_spvtl2($kategori_file);
		}
		else{
			$query=$this->ms_sdm->get_rekomendasi_spvtl();	
		}
		
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_endcontract_userspv_old()
	{
		$params = $_REQUEST;

		$columns = array( 
					0 =>'a.csdm',
					1 =>'a.name', 
					2 => 'a.jabatan',
					3 => 'a.tgl_awal_kontrak',
					4 => 'a.tgl_akhir_kontrak',
					5 => 'a.id'
				);

		// $query=$this->ms_sdm->get_endcontract_userspv();
		$query=$this->ms_sdm->get_endcontract_userspv(
			$columns[$params['order'][0]['column']], 
			$params['order'][0]['dir'], 
			$params['start'], 
			$params['length']);

		$total = $this->ms_sdm->get_total_endcontract_userspv_severside()->row();

		$data['data']= array();
		$i=0;
		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}
		// print_r($total);die;
		$json_data = array(
			// "draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => $total->total,  
			"recordsFiltered" => $total->total,
			"data"            => $data['data']   // total data array
			);
		echo json_encode($data);
	}

	public function get_endcontract_usertl()
	{
		$query=$this->ms_sdm->get_endcontract_usertl();
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function do_upload_sdm(){
		$this->load->library('Spreadsheet_Excel_Reader');
		$this->load->model('ma_sdm');

		if(isset($_FILES)){
			$column_table = array("name", "nik_hris", "gender", 
							"batch", "agama", "csdm", "perner", 
							"nama_online", "password", "jabatan", 
							"jabatan_level", "jabatan_level_kode", "unit", 
							"ctp", "id_tl", "tl",
							"spv", "skill", "skill_layanan", 
							"skema_agent", "status_pekerja", "join_insani", 
							"tempat_lahir", "tgl_lahir", "tgl_awal_kontrak", "join_unit",
							"tgl_akhir_kontrak", "ppjp", "pendidikan", 
							"no_hp", "bpjs_tk", "bpjs_kesehatan", "npwp");
			$unit = array('OPERASIONAL IBC', 'OPERASIONAL OBC', 'COMPLAINT HANDLING', 'QUALITY CONTROL', 'QIA', 'TDP', 'IT SUPPORT', 'KORNAS');
			$ctp = array('IBC','OBC','ITCC');
			$jabatan_level = array('STAFF','TL','SPV','ADMIN','ROSTER');
			$data = new Spreadsheet_Excel_Reader($_FILES['file_upload']['tmp_name']);
			$row = 0;
			$col = 0;

			$jumlahbaris = $data->rowcount($sheet_index=0);
			$jumlahkolom = $data->colcount($sheet_index=0);
			$status = 1;
			$status_unit = 0;
			$status_ctp = 0;
			$status_csdm = 0;
			$status_jabatan_level = 0;

			$column_insert = array();
			$col = 2;
			while ( $row <= $jumlahkolom && $data->val(1,$col)!='' && $status==1) {
				if($data->val(1,$col) == 'unit') $status_unit = 1;
				if($data->val(1,$col) == 'ctp') $status_ctp = 1;
				if($data->val(1,$col) == 'csdm') $status_csdm = 1;
				if($data->val(1,$col) == 'jabatan_level') $status_jabatan_level = 1;

				if(in_array($data->val(1,$col), $column_table)){
					array_push($column_insert, $data->val(1,$col));
				}else{ $status=0; }
				$col ++;
			}
			
			$data_insert = array();
			$data_absensi_insert = array();
			if($status == 0) echo json_encode(array('hasil'=>'error', 'message'=>'Kode "'.$data->val(1,($col-1)).'" tidak terdaftar!'));
			else if($status_ctp == 0) echo json_encode(array('hasil'=>'error', 'message'=>'Column "ctp" harus ada.'));
			else if($status_unit == 0) echo json_encode(array('hasil'=>'error', 'message'=>'Column "unit" harus ada.'));
			else if($status_jabatan_level == 0) echo json_encode(array('hasil'=>'error', 'message'=>'Column "jabatan_level" harus ada.'));
			else{
				$row = 2;
				while ( $row <= $jumlahbaris && $data->val($row,1)!='') {
					$i_col = 0;
					$temp_array = array();
					$temp_array2 = array();
					while ( $i_col < count($column_insert)) {
						$temp_array[ $column_insert[$i_col] ] = $data->val($row,($i_col+2));
						
						if($column_insert[$i_col] == 'unit' && !in_array($data->val($row,($i_col+2)), $unit) ){
							echo json_encode(array('hasil'=> 'success', 'message'=>'Unit "'.$data->val($row,($i_col+2)).'" Belum Terdaftar di Aplikasi.'));
							die;
						}elseif($column_insert[$i_col] == 'ctp' && !in_array($data->val($row,($i_col+2)), $ctp)){
							echo json_encode(array('hasil'=> 'success', 'message'=>'CTP "'.$data->val($row,($i_col+2)).'" Belum Terdaftar di Aplikasi.'));
							die;
						}elseif($column_insert[$i_col] == 'jabatan_level' && !in_array($data->val($row,($i_col+2)), $jabatan_level)){
							echo json_encode(array('hasil'=> 'success', 'message'=>'Jabatan Level "'.$data->val($row,($i_col+2)).'" Belum Terdaftar di Aplikasi.'));
							die;
						}
						
						if($column_insert[$i_col] == 'ctp' || $column_insert[$i_col] == 'jabatan' || $column_insert[$i_col] == 'unit'){
							$temp_array2[ $column_insert[$i_col] ] = $data->val($row,($i_col+2));
						}
						if($column_insert[$i_col] == 'csdm'){
							$temp_array2[ 'perner_id' ] = $data->val($row,($i_col+2));
						}
						if($column_insert[$i_col] == 'name'){
							$temp_array2[ 'sdm_name' ] = $data->val($row,($i_col+2));
						}

						$i_col++;
					}
					array_push($data_insert, $temp_array);
					array_push($data_absensi_insert, $temp_array2);
					$row ++;
				}

				$hasil = $this->ms_sdm->do_insert_sdm_bacth($data_insert);
				if($hasil['affected_rows'] != '-1'){
					$hasil = $this->ma_sdm->do_insert_sdm_bacth($data_absensi_insert);
					$update = $this->ms_sdm->do_update_tl_spv_ctp();
				} 

				if($hasil['affected_rows'] != '-1'){
					echo json_encode(array('hasil'=> 'success', 'message'=>'Berhasil Input SDM '.$hasil['affected_rows'].' Data'));
				}
				else{
					echo json_encode(array('hasil'=> 'error', 'message'=>$hasil['error']['message']));
				}
			}

			// while ( $row <= $jumlahbaris && $data->val($row,1,0)!='') {
			// 	while ( $col <= $jumlahkolom && $data->val(1,$col,0)!='') {
			// 		$result = $this->ma_jadwal->do_insert_jadwal($data->val($row,1,0), $data->val(1,$col,0), $data->val($row,$col,0) );
			// 		if($result != 1) {
			// 			echo json_encode(array('hasil'=>'error'));
			// 		}
			// 		$col ++;
			// 	}
			// 	$row ++;
			// }

			// echo json_encode(array('hasil'=>'success'));
		}
		else {
			echo json_encode(array('hasil'=>'File Kosong.'));
		}
	}

	public function do_insert_sdm(){

		// $this->load->model('ma_sdm');
		$this->load->model('ms_tb_history_update_sdm');

		//----upload file----//
        $csdm= $this->input->post('csdm');

        $config['upload_path']="./document/$csdm"; //path folder file upload
        $config['allowed_types']='gif|jpg|png|jpeg|bmp|ppt|pptx|xls|xlsx|doc|docx|pdf|rar|zip'; //type file yg bisa di upload
		$config['encrypt_name'] = FAlSE;
        $this->load->library('upload',$config);

        $file_pendukung1='';
    	$file_pendukung2='';
    	$file_pendukung3='';
    	$file_pendukung4='';

        for ($i=1; $i <=4 ; $i++) { 	
            if(!empty($_FILES['file_pendukung'.$i]['name'])){
            	$pathfile = $_FILES['file_pendukung'.$i]['name'];
		        if (is_dir("document/$csdm/")) { //cek if path exist
        			$this->upload->do_upload('file_pendukung'.$i); //upload file
        		}else {
		        	mkdir("document/$csdm/"); //create folder
        			$this->upload->do_upload('file_pendukung'.$i); //upload file
		        }
		        $namefile = $this->upload->data('file_name');
		        $file_pendukung="file_pendukung".$i;
		        
		        if ($file_pendukung == 'file_pendukung1' ) {
		        	$file_pendukung1=$namefile;
		        }
		        if ($file_pendukung == 'file_pendukung2' ) {
		        	$file_pendukung2=$namefile;
		        }
		        if ($file_pendukung == 'file_pendukung3' ) {
		        	$file_pendukung3=$namefile;
		        }
		        if ($file_pendukung == 'file_pendukung4' ) {
		        	$file_pendukung4=$namefile;
		        }
            }
        }
        //-----------//

		$x = $this->input->post('id_tl');
		if($x == '0' || $x == ' ' || $x == ''){$id_tl= null;}
		else{$id_tl = $this->input->post('id_tl');}
		
		$y = $this->input->post('id_spv');
		if($y == '0' || $y == ' ' || $y == ''){$id_spv= null;}
		else{$id_spv = $this->input->post('id_spv');}

		$data = array(
			'status_sdm' => '1',
			'password' => 'infomedia',
			'name' => $this->input->post('name'),
			'nik_hris' => $this->input->post('nik_hris'),
			'gender' => $this->input->post('gender'),
			'csdm' => $this->input->post('csdm'),
			'agama' => $this->input->post('agama'),
			'batch' => $this->input->post('batch'),
			'perner' => $this->input->post('perner'),
			'nama_online' => $this->input->post('nama_online'),
			'jabatan' => $this->input->post('jabatan'),
			'jabatan_level' => $this->input->post('jabatan_level'),
			'unit' => $this->input->post('unit'),
			'ctp' => $this->input->post('ctp'),
			'skill_layanan' => $this->input->post('skill_layanan'),
			'id_tl' => $id_tl,
			//'tl' => $this->input->post('notes'),
			'id_spv' => $id_spv,
			//'spv' => $this->input->post('notes'),
			'skema_agent' => $this->input->post('skema_agent'),
			'status_menikah' => $this->input->post('status_menikah'),
			'tempat_lahir' => $this->input->post('tempat_lahir'),
			'tgl_lahir' => $this->input->post('tgl_lahir'),
			'tgl_awal_kontrak' => $this->input->post('tgl_awal_kontrak'),
			'join_insani' => $this->input->post('join_insani'),
			'tgl_akhir_kontrak' => $this->input->post('tgl_akhir_kontrak'),
			'ppjp' => $this->input->post('ppjp'),
			'no_hp' => $this->input->post('no_hp'),
			'join_unit' => $this->input->post('join_unit'),
			'email' => $this->input->post('email'),
			'pendidikan' => $this->input->post('pendidikan'),
			'bpjs_tk' => $this->input->post('bpjs_tk'),
			'bpjs_kesehatan' => $this->input->post('bpjs_kesehatan'),
			'npwp' => $this->input->post('npwp'),
			'input_sdm_by' => $this->session->csdm,
			'last_edited_by' => $this->session->csdm,
			'file_pendukung1' => $file_pendukung1,
			'file_pendukung2' => $file_pendukung2,
			'file_pendukung3' => $file_pendukung3,
			'file_pendukung4' => $file_pendukung4
		);

		// $data2 = array(
		// 	'perner_id' => $this->input->post('csdm'),
		// 	'sdm_name' => $this->input->post('name'),
		// 	'jabatan' => $this->input->post('jabatan'),
		// 	'ctp' => $this->input->post('ctp'),
		// 	'unit' => $this->input->post('unit'),
		// 	'presensi' => 'Y',
		// 	'state_id' => '1'
		// );

		$data3 = array(
			'status_sdm' => '1',
			'password' => 'infomedia',
			'name' => $this->input->post('name'),
			'nik_hris' => $this->input->post('nik_hris'),
			'gender' => $this->input->post('gender'),
			'csdm' => $this->input->post('csdm'),
			'agama' => $this->input->post('agama'),
			'batch' => $this->input->post('batch'),
			'perner' => $this->input->post('perner'),
			'nama_online' => $this->input->post('nama_online'),
			'jabatan' => $this->input->post('jabatan'),
			'jabatan_level' => $this->input->post('jabatan_level'),
			'unit' => $this->input->post('unit'),
			'ctp' => $this->input->post('ctp'),
			'skill_layanan' => $this->input->post('skill_layanan'),
			'id_tl' => $id_tl,
			//'tl' => $this->input->post('notes'),
			'id_spv' => $id_spv,
			//'spv' => $this->input->post('notes'),
			'skema_agent' => $this->input->post('skema_agent'),
			'status_menikah' => $this->input->post('status_menikah'),
			'tempat_lahir' => $this->input->post('tempat_lahir'),
			'tgl_lahir' => $this->input->post('tgl_lahir'),
			'tgl_awal_kontrak' => $this->input->post('tgl_awal_kontrak'),
			'join_insani' => $this->input->post('join_insani'),
			'tgl_akhir_kontrak' => $this->input->post('tgl_akhir_kontrak'),
			'ppjp' => $this->input->post('ppjp'),
			'no_hp' => $this->input->post('no_hp'),
			'join_unit' => $this->input->post('join_unit'),
			'email' => $this->input->post('email'),
			'pendidikan' => $this->input->post('pendidikan'),
			'bpjs_tk' => $this->input->post('bpjs_tk'),
			'bpjs_kesehatan' => $this->input->post('bpjs_kesehatan'),
			'npwp' => $this->input->post('npwp'),
			'reason_update' => 'KONTRAK BARU',
			'last_edited_by' => $this->session->csdm,
			'file_pendukung1' => $file_pendukung1,
			'file_pendukung2' => $file_pendukung2,
			'file_pendukung3' => $file_pendukung3,
			'file_pendukung4' => $file_pendukung4
		);

		$result = $this->ms_sdm->do_insert_sdm($data);
		$result2 = $this->ms_tb_history_update_sdm->do_insert($data3);
		
		if($result['success']){
			
			// $result2 = $this->ma_sdm->do_insert_sdm($data2);
			
			// if($result2['success']){
				echo json_encode(array('hasil'=>'success'));
				// echo json_encode(array('hasil'=>'success', 'url'=>'hr/content/input_karyawan'));
				// echo json_encode(array('hasil'=>'success', 'url'=>'hr/content'));
			// }else{
			// 	echo json_encode(array('hasil'=>$result['error']['message']));
			// }
		}else{
			echo json_encode(array('hasil'=>$result['error']['message']));
		}
	}

	public function do_update_sdm(){
		$this->load->model('ms_tb_history_update_sdm');
		
		//----upload file----//
        $csdm= $this->input->post('csdm');

        $config['upload_path']="./document/$csdm"; //path folder file upload
        $config['allowed_types']='gif|jpg|png|jpeg|bmp|ppt|pptx|xls|xlsx|doc|docx|pdf|rar|zip'; //type file yg bisa di upload
		$config['encrypt_name'] = FAlSE;
        $this->load->library('upload',$config);

        $file_pendukung1=$this->input->post('file_pendukung1');
    	$file_pendukung2=$this->input->post('file_pendukung2');
    	$file_pendukung3=$this->input->post('file_pendukung3');
    	$file_pendukung4=$this->input->post('file_pendukung4');
    	
        for ($i=1; $i <=4 ; $i++) { 	
            if(!empty($_FILES['file_reupload'.$i]['name'])){
            	$pathfile = $_FILES['file_reupload'.$i]['name'];
		        if (is_dir("document/$csdm/")) { //cek if path exist
        			$this->upload->do_upload('file_reupload'.$i); //upload file
        		}else {
		        	mkdir("document/$csdm/"); //create folder
        			$this->upload->do_upload('file_reupload'.$i); //upload file
		        }
		        $namefile = $this->upload->data('file_name');
		        $file_pendukung="file_reupload".$i;
		        
		        if ($file_pendukung == 'file_reupload1' ) {
		        	$file_pendukung1=$namefile;
		        }
		        if ($file_pendukung == 'file_reupload2' ) {
		        	$file_pendukung2=$namefile;
		        }
		        if ($file_pendukung == 'file_reupload3' ) {
		        	$file_pendukung3=$namefile;
		        }
		        if ($file_pendukung == 'file_reupload4' ) {
		        	$file_pendukung4=$namefile;
		        }
            }
        }
        //-----------//

		$x = $this->input->post('id_tl');
		if($x == '0' || $x == ' ' || $x == ''){$id_tl= null;}
		else{$id_tl = $this->input->post('id_tl');}
		
		$y = $this->input->post('id_spv');
		if($y == '0' || $y == ' ' || $y == ''){$id_spv= null;}
		else{$id_spv = $this->input->post('id_spv');}

		$data = array(
			'status_sdm' => $this->input->post('status_sdm'),
			// 'password' => 'infomedia',
			'name' => $this->input->post('name'),
			'nik_hris' => $this->input->post('nik_hris'),
			'gender' => $this->input->post('gender'),
			'csdm' => $this->input->post('csdm'),
			'agama' => $this->input->post('agama'),
			'batch' => $this->input->post('batch'),
			'perner' => $this->input->post('perner'),
			'nama_online' => $this->input->post('nama_online'),
			'jabatan' => $this->input->post('jabatan'),
			'jabatan_level' => $this->input->post('jabatan_level'),
			'unit' => $this->input->post('unit'),
			'ctp' => $this->input->post('ctp'),
			'skill_layanan' => $this->input->post('skill_layanan'),
			'id_tl' => $id_tl,
			//'tl' => $this->input->post('notes'),
			'id_spv' => $id_spv,
			//'spv' => $this->input->post('notes'),
			'skema_agent' => $this->input->post('skema_agent'),
			'status_menikah' => $this->input->post('status_menikah'),
			'tempat_lahir' => $this->input->post('tempat_lahir'),
			'tgl_lahir' => $this->input->post('tgl_lahir'),
			'tgl_awal_kontrak' => $this->input->post('tgl_awal_kontrak'),
			'join_insani' => $this->input->post('join_insani'),
			'tgl_akhir_kontrak' => $this->input->post('tgl_akhir_kontrak'),
			'ppjp' => $this->input->post('ppjp'),
			'no_hp' => $this->input->post('no_hp'),
			'join_unit' => $this->input->post('join_unit'),
			'email' => $this->input->post('email'),
			'pendidikan' => $this->input->post('pendidikan'),
			'bpjs_tk' => $this->input->post('bpjs_tk'),
			'bpjs_kesehatan' => $this->input->post('bpjs_kesehatan'),
			'npwp' => $this->input->post('npwp'),
			'last_edited_by' => $this->session->csdm,
			'file_pendukung1' => $file_pendukung1,
			'file_pendukung2' => $file_pendukung2,
			'file_pendukung3' => $file_pendukung3,
			'file_pendukung4' => $file_pendukung4
		);

		$data2 = array(
			'status_sdm' => $this->input->post('status_sdm'),
			// 'password' => 'infomedia',
			'name' => $this->input->post('name'),
			'nik_hris' => $this->input->post('nik_hris'),
			'gender' => $this->input->post('gender'),
			'csdm' => $this->input->post('csdm'),
			'agama' => $this->input->post('agama'),
			'batch' => $this->input->post('batch'),
			'perner' => $this->input->post('perner'),
			'nama_online' => $this->input->post('nama_online'),
			'jabatan' => $this->input->post('jabatan'),
			'jabatan_level' => $this->input->post('jabatan_level'),
			'unit' => $this->input->post('unit'),
			'ctp' => $this->input->post('ctp'),
			'skill_layanan' => $this->input->post('skill_layanan'),
			'id_tl' => $id_tl,
			//'tl' => $this->input->post('notes'),
			'id_spv' => $id_spv,
			//'spv' => $this->input->post('notes'),
			'skema_agent' => $this->input->post('skema_agent'),
			'status_menikah' => $this->input->post('status_menikah'),
			'tempat_lahir' => $this->input->post('tempat_lahir'),
			'tgl_lahir' => $this->input->post('tgl_lahir'),
			'tgl_awal_kontrak' => $this->input->post('tgl_awal_kontrak'),
			'join_insani' => $this->input->post('join_insani'),
			'tgl_akhir_kontrak' => $this->input->post('tgl_akhir_kontrak'),
			'ppjp' => $this->input->post('ppjp'),
			'no_hp' => $this->input->post('no_hp'),
			'join_unit' => $this->input->post('join_unit'),
			'email' => $this->input->post('email'),
			'pendidikan' => $this->input->post('pendidikan'),
			'bpjs_tk' => $this->input->post('bpjs_tk'),
			'bpjs_kesehatan' => $this->input->post('bpjs_kesehatan'),
			'npwp' => $this->input->post('npwp'),
			'reason_update' => $this->input->post('reason_update'),
			'notes' => $this->input->post('notes'),
			'last_edited_by' => $this->session->csdm,
			'file_pendukung1' => $file_pendukung1,
			'file_pendukung2' => $file_pendukung2,
			'file_pendukung3' => $file_pendukung3,
			'file_pendukung4' => $file_pendukung4
		);

		$id = $this->input->post('id_sdm');
		
		$result = $this->ms_sdm->do_update_sdm($data, $id);
		
		if($result['success']){

			if ($this->input->post('reason_update') !== '' ){
				$result2 = $this->ms_tb_history_update_sdm->do_insert($data2);

				if($result2['success']){
					// echo json_encode(array('hasil'=>'success', 'url'=>'hr/update_sdm/24'));	
					echo json_encode(array('hasil'=>'success', 'url'=>'hr/content'));
				}else{
					echo json_encode(array('hasil'=>$result['error']['message']));
				}
			}else{
				// echo json_encode(array('hasil'=>'success', 'url'=>'hr/update_sdm/24'));	
				echo json_encode(array('hasil'=>'success', 'url'=>'hr/content'));
			}	
		}else{
			echo json_encode(array('hasil'=>$result['error']['message']));
		}
	}

	public function do_update_sdm2(){
		$this->load->model('ms_tb_history_update_sdm');
		$this->load->model('ms_document');

		//----upload file----//
        $csdm= $this->input->post('csdm');

        $config['upload_path']="./document/$csdm"; //path folder file upload
        $config['allowed_types']='gif|jpg|png|jpeg|bmp|ppt|pptx|xls|xlsx|doc|docx|pdf|rar|zip'; //type file yg bisa di upload
		$config['encrypt_name'] = FAlSE;
        $this->load->library('upload',$config);

        $file_pendukung1=$this->input->post('file_pendukung1');
    	$file_pendukung2=$this->input->post('file_pendukung2');
    	$file_pendukung3=$this->input->post('file_pendukung3');
    	$file_pendukung4=$this->input->post('file_pendukung4');
    	
        for ($i=1; $i <=4 ; $i++) { 	
            if(!empty($_FILES['file_reupload'.$i]['name'])){
            	$pathfile = $_FILES['file_reupload'.$i]['name'];
		        if (is_dir("document/$csdm/")) { //cek if path exist
        			$this->upload->do_upload('file_reupload'.$i); //upload file
        		}else {
		        	mkdir("document/$csdm/"); //create folder
        			$this->upload->do_upload('file_reupload'.$i); //upload file
		        }
		        $namefile = $this->upload->data('file_name');
		        $file_pendukung="file_reupload".$i;
		        
		        if ($file_pendukung == 'file_reupload1' ) {
		        	$file_pendukung1=$namefile;
		        }
		        if ($file_pendukung == 'file_reupload2' ) {
		        	$file_pendukung2=$namefile;
		        }
		        if ($file_pendukung == 'file_reupload3' ) {
		        	$file_pendukung3=$namefile;
		        }
		        if ($file_pendukung == 'file_reupload4' ) {
		        	$file_pendukung4=$namefile;
		        }
            }
        }
        //-----------//

		$id_doc = $this->input->post('id_doc');
		$reason_update = $this->input->post('reason_update');
		$reason_update2 = $this->input->post('reason_update2');
		$status_doc = $this->input->post('status_doc');
		$status_sdm = $this->input->post('status_sdm');

		$x = $this->input->post('id_tl');
		if($x == '0' || $x == ' ' || $x == ''){$id_tl= null;}
		else{$id_tl = $this->input->post('id_tl');}
		
		$y = $this->input->post('id_spv');
		if($y == '0' || $y == ' ' || $y == ''){$id_spv= null;}
		else{$id_spv = $this->input->post('id_spv');}

		$data = array(
			'status_sdm' => $status_sdm,
			'name' => $this->input->post('name'),
			'nik_hris' => $this->input->post('nik_hris'),
			'gender' => $this->input->post('gender'),
			'agama' => $this->input->post('agama'),
			'batch' => $this->input->post('batch'),
			'csdm' => $this->input->post('csdm'),
			'perner' => $this->input->post('perner'),
			'nama_online' => $this->input->post('nama_online'),
			'password' => 'infomedia',
			'jabatan' => $this->input->post('jabatan'),
			'jabatan_level' => $this->input->post('jabatan_level'),
			//'jabatan_level_kode' => $this->input->post('notes'),
			'unit' => $this->input->post('unit'),
			'ctp' => $this->input->post('ctp'),
			'id_tl' => $id_tl,
			//'tl' => $this->input->post('notes'),
			'id_spv' => $id_spv,
			//'spv' => $this->input->post('notes'),
			//'skill' => $this->input->post('notes'),
			'skill_layanan' => $this->input->post('skill_layanan'),
			'skema_agent' => $this->input->post('skema_agent'),
			'tgl_lahir' => $this->input->post('tgl_lahir'),
			'tempat_lahir' => $this->input->post('tempat_lahir'),
			'status_pekerja' => $this->input->post('status_pekerja'),
			'join_insani' => $this->input->post('join_insani'),
			'tgl_awal_kontrak' => $this->input->post('tgl_awal_kontrak'),
			'tgl_akhir_kontrak' => $this->input->post('tgl_akhir_kontrak'),
			'join_unit' => $this->input->post('join_unit'),
			'ppjp' => $this->input->post('ppjp'),
			'pendidikan' => $this->input->post('pendidikan'),
			'no_hp' => $this->input->post('no_hp'),
			'bpjs_tk' => $this->input->post('bpjs_tk'),
			'bpjs_kesehatan' => $this->input->post('bpjs_kesehatan'),
			'npwp' => $this->input->post('npwp'),
			'last_edited_by' => $this->session->csdm,
			'file_pendukung1' => $file_pendukung1,
			'file_pendukung2' => $file_pendukung2,
			'file_pendukung3' => $file_pendukung3,
			'file_pendukung4' => $file_pendukung4
		);

		$data2 = array(
			'status_sdm' => $status_sdm,
			'name' => $this->input->post('name'),
			'nik_hris' => $this->input->post('nik_hris'),
			'gender' => $this->input->post('gender'),
			'agama' => $this->input->post('agama'),
			'batch' => $this->input->post('batch'),
			'csdm' => $this->input->post('csdm'),
			'perner' => $this->input->post('perner'),
			'nama_online' => $this->input->post('nama_online'),
			'password' => 'infomedia',
			'jabatan' => $this->input->post('jabatan'),
			'jabatan_level' => $this->input->post('jabatan_level'),
			'unit' => $this->input->post('unit'),
			'ctp' => $this->input->post('ctp'),
			'id_tl' => $id_tl,
			'id_spv' => $id_spv,
			'skill_layanan' => $this->input->post('skill_layanan'),
			'skema_agent' => $this->input->post('skema_agent'),
			'tgl_lahir' => $this->input->post('tgl_lahir'),
			'tempat_lahir' => $this->input->post('tempat_lahir'),
			'status_pekerja' => $this->input->post('status_pekerja'),
			'join_insani' => $this->input->post('join_insani'),
			'tgl_awal_kontrak' => $this->input->post('tgl_awal_kontrak'),
			'tgl_akhir_kontrak' => $this->input->post('tgl_akhir_kontrak'),
			'join_unit' => $this->input->post('join_unit'),
			'ppjp' => $this->input->post('ppjp'),
			'pendidikan' => $this->input->post('pendidikan'),
			'no_hp' => $this->input->post('no_hp'),
			'bpjs_tk' => $this->input->post('bpjs_tk'),
			'bpjs_kesehatan' => $this->input->post('bpjs_kesehatan'),
			'npwp' => $this->input->post('npwp'),
			'reason_update' => $reason_update2,
			'id_doc' => $id_doc,
			'notes' => $this->input->post('notes'),
			'last_edited_by' => $this->session->csdm,
			'file_pendukung1' => $file_pendukung1,
			'file_pendukung2' => $file_pendukung2,
			'file_pendukung3' => $file_pendukung3,
			'file_pendukung4' => $file_pendukung4
		);
		
		$id = $this->input->post('id_sdm');
		
		$result = $this->ms_sdm->do_update_sdm($data, $id);

		if($result['success']){
			
			$result2 = $this->ms_tb_history_update_sdm->do_insert($data2);
		
			if($result2['success']){
				
				$result3 = $this->ms_document->update_status($id_doc);		
				echo json_encode(array('hasil'=>'success', 'url'=>'hr/content'));
				// echo json_encode(array('hasil'=>'success', 'url'=>'hr/update_sdm2/768'));	
			}else{
				echo json_encode(array('hasil'=>$result['error']['message']));
			}

		}else{
			echo json_encode(array('hasil'=>$result['error']['message']));
		}
	}


	//===update sdm old===//
	// public function do_update_sdm(){
	// 	$id = $this->input->post('id');
	// 	$column = $this->input->post('column');
	// 	$value = $this->input->post('value');

	// 	$hasil = $this->ms_sdm->do_update_sdm_percolumn($id, $column, $value);

	// 	if($hasil['affected_rows'] != '-1'){
	// 		echo json_encode(array('hasil'=> 'success', 'message'=>$column.' Berhasil di Update.'));
	// 	}
	// 	else{
	// 		echo json_encode(array('hasil'=> 'error', 'message'=>$hasil['error']['message']));
	// 	}
	// }

	public function get_xls($date_start, $date_end, $status_sdm, $ctp, $jabatan, $unit, $id_tl, $id_spv)
	{
		$this->load->library("excel");
		$this->excel->load(APPPATH."../document/report_sdm.xlsx");
		$this->excel->setActiveSheetIndex(0);

		$status_sdm = urldecode($status_sdm);
		$ctp = urldecode($ctp);
		$jabatan = urldecode($jabatan);
		$unit = urldecode($unit);
		$id_tl = urldecode($id_tl);
		$id_spv = urldecode($id_spv);

		// $this->excel->getActiveSheet()->SetCellValue('A2', date('M-d-Y', $star).' to '. date('M-d-Y', $end));

		$a = " AND a.status_sdm = '".$status_sdm."'";

		if($date_start !='-'){
			$a.=" AND DATE_FORMAT(a.tgl_awal_kontrak, '%Y-%m-%d') BETWEEN '".$date_start."' AND '".$date_end."'";
		}
		if($ctp!='All'){
			$a.=" AND a.ctp LIKE '".$ctp."'";
		}
		if($jabatan!='All'){
			$a.=" AND a.jabatan LIKE '".$jabatan."'";
		}
		if($unit!='All'){
			$a.=" AND a.unit LIKE '".$unit."'";
		}
		if($id_tl!='All'){
			$a.=" AND a.id_tl = '".$id_tl."'";
		}
		if($id_spv!='All'){
			$a.=" AND a.id_spv = '".$id_spv."'";
		}

		$query=$this->ms_sdm->get_sdm($a);

		$i=1;
		$row=5;
		foreach ($query->result_array() as $myRow) {
			$this->excel->getActiveSheet()->SetCellValue('A'.$row, $i);
			$this->excel->getActiveSheet()->SetCellValue('B'.$row, $myRow['name']);
			$this->excel->getActiveSheet()->SetCellValue('C'.$row, $myRow['nik_hris']);
			$this->excel->getActiveSheet()->SetCellValue('D'.$row, $myRow['gender']);
			$this->excel->getActiveSheet()->SetCellValue('E'.$row, $myRow['csdm']);
			$this->excel->getActiveSheet()->SetCellValue('F'.$row, $myRow['agama']);
			$this->excel->getActiveSheet()->SetCellValue('G'.$row, $myRow['batch']);
			$this->excel->getActiveSheet()->SetCellValue('H'.$row, $myRow['perner']);
			$this->excel->getActiveSheet()->SetCellValue('I'.$row, $myRow['nama_online']);
			$this->excel->getActiveSheet()->SetCellValue('J'.$row, $myRow['jabatan']);
			$this->excel->getActiveSheet()->SetCellValue('K'.$row, $myRow['jabatan_level']);
			$this->excel->getActiveSheet()->SetCellValue('L'.$row, $myRow['unit']);
			$this->excel->getActiveSheet()->SetCellValue('M'.$row, $myRow['ctp']);
			$this->excel->getActiveSheet()->SetCellValue('N'.$row, $myRow['skill_layanan']);
			$this->excel->getActiveSheet()->SetCellValue('O'.$row, $myRow['name_tl']);
			$this->excel->getActiveSheet()->SetCellValue('P'.$row, $myRow['name_spv']);
			$this->excel->getActiveSheet()->SetCellValue('Q'.$row, $myRow['skema_agent']);
			$this->excel->getActiveSheet()->SetCellValue('R'.$row, $myRow['status_pekerja']);
			$this->excel->getActiveSheet()->SetCellValue('S'.$row, $myRow['tempat_lahir']);
			$this->excel->getActiveSheet()->SetCellValue('T'.$row, $myRow['tgl_lahir']);
			$this->excel->getActiveSheet()->SetCellValue('U'.$row, $myRow['tgl_awal_kontrak']);
			$this->excel->getActiveSheet()->SetCellValue('V'.$row, $myRow['join_insani']);
			$this->excel->getActiveSheet()->SetCellValue('W'.$row, $myRow['join_unit']);
			$this->excel->getActiveSheet()->SetCellValue('X'.$row, $myRow['tgl_akhir_kontrak']);
			$this->excel->getActiveSheet()->SetCellValue('Y'.$row, $myRow['ppjp']);
			$this->excel->getActiveSheet()->SetCellValue('Z'.$row, $myRow['no_hp']);
			$this->excel->getActiveSheet()->SetCellValue('AA'.$row, $myRow['pendidikan']);
			$this->excel->getActiveSheet()->SetCellValue('AB'.$row, $myRow['bpjs_tk']);
			$this->excel->getActiveSheet()->SetCellValue('AC'.$row, $myRow['bpjs_kesehatan']);
			$this->excel->getActiveSheet()->SetCellValue('AD'.$row, $myRow['npwp']);
			$this->excel->getActiveSheet()->SetCellValue('AE'.$row, $myRow['createdby']);
			$this->excel->getActiveSheet()->SetCellValue('AF'.$row, $myRow['input_sdm_by']);
			$this->excel->getActiveSheet()->SetCellValue('AG'.$row, $myRow['modief_by']);
			$this->excel->getActiveSheet()->SetCellValue('AH'.$row, $myRow['last_edited_by']);

			$i++;
			$row++;
		}

		$this->excel->getActiveSheet()->getStyle('A5:AH'.($row-1))->applyFromArray(
			array(
			'borders' => array(
					'allborders' => array(
		                'style' => PHPExcel_Style_Border::BORDER_THIN,
		                // 'color' => array('rgb' => 'DDDDDD')
		            )
				)
 			)
		);
		
		//die;
		$this->excel->stream("Report SDM.xlsx");
	}

	public function get_xls_rekomendasi($pathurl)
	{
		$this->load->library("excel");
		$this->excel->load(APPPATH."../document/report_rekomendasi.xlsx");
		$this->excel->setActiveSheetIndex(0);

		$pathurl = urldecode($pathurl);

		// $this->excel->getActiveSheet()->SetCellValue('A2', date('M-d-Y', $star).' to '. date('M-d-Y', $end));

		if($pathurl == '1'){
			// echo $pathurl;die;
			$kategori_file="RESIGN ON THE SPOT";
			$query=$this->ms_sdm->get_rekomendasi_spvtl2($kategori_file);
		}
		else if($pathurl == '2'){
			$kategori_file="RESIGN ONE MONTH NOTICE";
			$query=$this->ms_sdm->get_rekomendasi_spvtl2($kategori_file);
		}
		else if($pathurl == '3'){
			$kategori_file="CONTRACT";
			$query=$this->ms_sdm->get_rekomendasi_spvtl2($kategori_file);
		}
		else if($pathurl == '4'){
			$kategori_file="CUTI MELAHIRKAN";
			$query=$this->ms_sdm->get_rekomendasi_spvtl2($kategori_file);
		}
		else{
			$query=$this->ms_sdm->get_rekomendasi_spvtl();	
		}

		$i=1;
		$row=5;
		foreach ($query->result_array() as $myRow) {
			$this->excel->getActiveSheet()->SetCellValue('A'.$row, $i);
			$this->excel->getActiveSheet()->SetCellValue('B'.$row, $myRow['name']);
			$this->excel->getActiveSheet()->SetCellValue('C'.$row, $myRow['csdm']);
			$this->excel->getActiveSheet()->SetCellValue('D'.$row, $myRow['jabatan']);
			$this->excel->getActiveSheet()->SetCellValue('E'.$row, $myRow['jabatan_level']);
			$this->excel->getActiveSheet()->SetCellValue('F'.$row, $myRow['unit']);
			$this->excel->getActiveSheet()->SetCellValue('G'.$row, $myRow['name_tl']);
			$this->excel->getActiveSheet()->SetCellValue('H'.$row, $myRow['name_spv']);
			$this->excel->getActiveSheet()->SetCellValue('I'.$row, $myRow['tgl_awal_kontrak']);
			$this->excel->getActiveSheet()->SetCellValue('J'.$row, $myRow['tgl_akhir_kontrak']);
			$this->excel->getActiveSheet()->SetCellValue('K'.$row, $myRow['kategori_file']);
			$this->excel->getActiveSheet()->SetCellValue('L'.$row, $myRow['notes2']);
			$this->excel->getActiveSheet()->SetCellValue('M'.$row, $myRow['csdm_uploader']);
			$this->excel->getActiveSheet()->SetCellValue('N'.$row, $myRow['doc_created']);

			$i++;
			$row++;
		}

		$this->excel->getActiveSheet()->getStyle('A5:N'.($row-1))->applyFromArray(
			array(
			'borders' => array(
					'allborders' => array(
		                'style' => PHPExcel_Style_Border::BORDER_THIN,
		                // 'color' => array('rgb' => 'DDDDDD')
		            )
				)
 			)
		);
		
		//die;
		$this->excel->stream("Report Rekomendasi/pengajuan.xlsx");
	}

	public function do_update_password(){

        $password= $this->input->post('password');

        $result = $this->ms_sdm->do_update_password($this->session->id, $password);
        
        if($result == 1){
			echo json_encode(array('hasil'=>"success"));
		}
		else{
			echo json_encode(array('hasil'=>"error"));
		}
	}

	public function do_perpanjang_kontrak(){

		$this->load->model('ms_tb_history_update_sdm');

        $id= $this->input->post('id_sdm');
        $grading= $this->input->post('grading');
        
        $query=$this->ms_sdm->get_sdm_byid($id)->row_array();

        $csdm = $query['csdm'];
        $tgl_akhir_kontrak = $query['tgl_akhir_kontrak'];

        if ($grading == 'A') {
            $tgl_awal_kontrak_baru = date('Y-m-d', strtotime('-1 days', strtotime($tgl_akhir_kontrak)));
	        $tgl_akhir_kontrak_baru = date('Y-m-d', strtotime('+1 year', strtotime($tgl_awal_kontrak_baru)));
	        $tgl_akhir_kontrak_barufix = date('Y-m-d', strtotime('-1 days', strtotime($tgl_akhir_kontrak_baru)));
        } else if ($grading == 'B') {
        	$tgl_awal_kontrak_baru = date('Y-m-d', strtotime('-1 days', strtotime($tgl_akhir_kontrak)));
	        $tgl_akhir_kontrak_baru = date('Y-m-d', strtotime('+10 month', strtotime($tgl_awal_kontrak_baru)));
	        $tgl_akhir_kontrak_barufix = date('Y-m-d', strtotime('-1 days', strtotime($tgl_akhir_kontrak_baru)));
        } else if ($grading == 'C') {
        	$tgl_awal_kontrak_baru = date('Y-m-d', strtotime('-1 days', strtotime($tgl_akhir_kontrak)));
	        $tgl_akhir_kontrak_baru = date('Y-m-d', strtotime('+6 month', strtotime($tgl_awal_kontrak_baru)));
	        $tgl_akhir_kontrak_barufix = date('Y-m-d', strtotime('-1 days', strtotime($tgl_akhir_kontrak_baru)));
        }
        

        // echo $query['tgl_akhir_kontrak']."<br>";
        // echo $tgl_awal_kontrak_baru."<br>";
        // echo $tgl_akhir_kontrak_baru."<br>";
        // echo $tgl_akhir_kontrak_barufix."<br>";
        // echo $id."<br>";
        // echo $csdm."<br>";
        // die;
        
        $data = array(
			'tgl_awal_kontrak' => $tgl_awal_kontrak_baru,
			'tgl_akhir_kontrak' => $tgl_akhir_kontrak_barufix,
			'last_edited_by' => $this->session->csdm
		);

		$data2 = array(
			'csdm' => $csdm,
			'tgl_awal_kontrak' => $tgl_awal_kontrak_baru,
			'tgl_akhir_kontrak' => $tgl_akhir_kontrak_barufix,
			'reason_update' => 'PERPANJANG KONTRAK',
			'last_edited_by' => $this->session->csdm
		);

        $result = $this->ms_sdm->do_update_sdm($data, $id);
		
		if($result['success']){
			
			$result2 = $this->ms_tb_history_update_sdm->do_insert($data2);
			
			if($result2['success']){
				echo json_encode(array('hasil'=>'success'));
			}else{
				echo json_encode(array('hasil'=>$result['error']['message']));
			}
		}else{
			echo json_encode(array('hasil'=>$result['error']['message']));
		}
	}

	public function get_total_home_userhr2()
	{	
		
		$query=$this->ms_sdm->get_total_home_userhr2();
		
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		echo json_encode($data);
	}

}
