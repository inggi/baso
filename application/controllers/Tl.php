<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tl extends CI_Controller {

	function __construct() {
        parent::__construct();
		if ($this->session->logged_in != TRUE || $this->session->jabatan_level !== "TL") {
			redirect('login');
		}
    }

	public function content($page = 'home')
	{
		$this->load->model('ms_sdm');
		$row = $this->ms_sdm->get_total_home_usertl()->row_array();
		$data = array(
			'data' => $row,
			'level' => $this->session->jabatan_level,
			'view' => 'tl/'.$page,
			'page' => $page, 
			'menu' => 'menu_tl');
		$this->load->view('wrapper', $data);
	}

	public function view_sdm($id_sdm){
		$this->load->model('ms_sdm');
		$data_sdm = $this->ms_sdm->get_sdm_byid($id_sdm)->row_array();
		$data = array(
			'data' => $data_sdm,
			'level' => 'tl',
			'view' => 'tl/'.'view_sdm',
			'page' => 'view_sdm', 
			'menu' => 'menu_tl');
		$this->load->view('wrapper', $data);
	}

}
