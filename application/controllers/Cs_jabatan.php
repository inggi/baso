<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cs_jabatan extends CI_Controller {

	function __construct() {
        parent::__construct();
		if ($this->session->logged_in != TRUE) {
			redirect('login');
		}

		$this->load->model('ms_jabatan');
    }

    public function get_jabatan()
	{
		$query=$this->ms_jabatan->get_jabatan();
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i]= $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function do_insert_jabatan()
	{
		
		$data = array(
			'jabatan' => $this->input->post('nama_jabatan_baru'),
			'ctp' => $this->session->ctp
		);

		$result = $this->ms_jabatan->do_insert_jabatan($data);

		if($result['success']){
			echo json_encode(array('hasil'=>'success'));
		}else{
			echo json_encode(array('hasil'=>$result['error']['message']));
		}

	}

	public function do_delete_jabatan()
	{
		
		$data = array(
			'id_jabatan' => $this->input->post('id_jabatan')
		);

		$result = $this->ms_jabatan->do_delete_jabatan($data);

		if($result['success']){
			echo json_encode(array('hasil'=>'success'));
		}else{
			echo json_encode(array('hasil'=>$result['error']['message']));
		}

	}

}
