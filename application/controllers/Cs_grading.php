<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cs_grading extends CI_Controller {

	function __construct() {
        parent::__construct();
		if ($this->session->logged_in != TRUE) {
			redirect('login');
		}

		$this->load->model('ms_grading');
    }

    public function do_import_grading(){
		$this->load->library('Spreadsheet_Excel_Reader');
		
		if(isset($_FILES)){
			$data = new Spreadsheet_Excel_Reader($_FILES['file_upload']['tmp_name']);
			$row = 0;
			$col = 0;
			// print_r($_POST); die;
			for($sheet = 0; $sheet < intval($this->input->post('sheet')); $sheet++){
							// echo json_encode(array('hasil'=>'error'));
				$jumlahbaris = $data->rowcount($sheet_index=$sheet);
				$row = 2;
				while ($row <= $jumlahbaris && $data->val($row,2,$sheet)!='') {
					$result = $this->ms_grading->do_insert_grading($data->val($row,2,$sheet), $data->val($row,3,$sheet), $data->val($row,4,$sheet), $data->val($row,5,$sheet), $data->val($row,6,$sheet), $data->val($row,7,$sheet), $data->val($row,8,$sheet), $data->val($row,9,$sheet), $data->val($row,10,$sheet), $data->val($row,11,$sheet), $data->val($row,12,$sheet), $this->session->csdm );
					if($result != 1) {
						echo json_encode(array('hasil'=>'error'));
					}
					$row ++;
				}
			}

			echo json_encode(array('hasil'=>'success'));
		}
		else {
			echo json_encode(array('hasil'=>'File Kosong.'));
		}
	}

}
