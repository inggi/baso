<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spv extends CI_Controller {

	function __construct() {
        parent::__construct();
		if ($this->session->logged_in != TRUE || $this->session->jabatan_level !== "SPV") {
			redirect('login');
		}
    }

	public function content($page = 'home')
	{
		$this->load->model('ms_sdm');
		$row = $this->ms_sdm->get_total_home_userspv()->row_array();
		$data = array(
			'data' => $row,
			'level' => $this->session->jabatan_level,
			'view' => 'spv/'.$page,
			'page' => $page, 
			'menu' => 'menu_spv');
		$this->load->view('wrapper', $data);
	}

	// public function home()
	// {
	// 	$this->load->model('ms_sdm');
	// 	$row = $this->ms_sdm->get_total_endcontract_userspv()->row_array();
	// 	$data = array(
	// 		'data' => $row,
	// 		'level' => 'spv',
	// 		'view' => 'spv/home',
	// 		'page' => 'home', 
	// 		'menu' => 'menu_spv');
	// 	$this->load->view('wrapper', $data);
	// }

	public function view_sdm($id_sdm){
		$this->load->model('ms_sdm');
		$data_sdm = $this->ms_sdm->get_sdm_byid($id_sdm)->row_array();
		$data = array(
			'data' => $data_sdm,
			'level' => 'spv',
			'view' => 'spv/'.'view_sdm',
			'page' => 'view_sdm', 
			'menu' => 'menu_spv');
		$this->load->view('wrapper', $data);
	}

}
