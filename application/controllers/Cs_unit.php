<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cs_unit extends CI_Controller {

	function __construct() {
        parent::__construct();
		if ($this->session->logged_in != TRUE) {
			redirect('login');
		}

		$this->load->model('ms_unit');
    }

	public function get_unit()
	{
		$query=$this->ms_unit->get_unit();
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i]= $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_unit_byusersession()
	{
		$query=$this->ms_unit->get_unit_byusersession();
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i]= $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_ctp()
	{
		$query=$this->ms_unit->get_ctp();
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i]= $myRow;
			$i++;
		}

		echo json_encode($data);
	}
}
