<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cs_history extends CI_Controller {

	function __construct() {
        parent::__construct();
		if ($this->session->logged_in != TRUE) {
			redirect('login');
		}

		$this->load->model('ms_tb_history_update_sdm');
    }

	public function get_tb_history_update_sdm()
	{
		$unit = $this->input->post('unit');
		$reason_update = $this->input->post('reason_update');
		$date_start = date('Y-m-d', strtotime($this->input->post('date_start')));
		$date_end = date('Y-m-d', strtotime($this->input->post('date_end')));
		$id_tl = $this->input->post('id_tl');
		$id_spv = $this->input->post('id_spv');

		$a = '';
		if($date_start !='1970-01-01'){
			$a.=" AND DATE_FORMAT(a.tgl_awal_kontrak, '%Y-%m-%d') BETWEEN '".$date_start."' AND '".$date_end."'";
		}
		if($reason_update!='All'){
			$a.=" AND a.reason_update LIKE '".$reason_update."'";
		}
		if($this->session->jabatan_level != "TL"){	
			if($id_tl!='All'){
				$a.=" AND a.id_tl = '".$id_tl."'";
			}
		}

		if($this->session->jabatan_level == "TL"){
			$query=$this->ms_tb_history_update_sdm->get_history_usertl($a, $this->session->id);

		}
		if($this->session->jabatan_level == "SPV"){
			$query=$this->ms_tb_history_update_sdm->get_history_userspv($a, $this->session->id);
		}
		else{
			if($unit!='All'){
				$a.=" AND a.unit LIKE '".$unit."'";
			}
			if($id_spv!='All'){
				$a.=" AND a.id_spv = '".$id_spv."'";
			}
			$query=$this->ms_tb_history_update_sdm->get_history($a);
		}

		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_history_usertl()
	{
		$reason_update = $this->input->post('reason_update');
		$date_start = date('Y-m-d', strtotime($this->input->post('date_start')));
		$date_end = date('Y-m-d', strtotime($this->input->post('date_end')));
		
		$a = '';
		if($date_start !='1970-01-01'){
			$a.=" AND DATE_FORMAT(a.createdby, '%Y-%m-%d') BETWEEN '".$date_start."' AND '".$date_end."'";
		}
		if($reason_update!='All'){
			$a.=" AND a.reason_update LIKE '".$reason_update."'";
		}
		
		$query=$this->ms_tb_history_update_sdm->get_history_usertl($a, $this->session->id);
		
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	// public function get_tb_history_update_sdm2()
	// {
	// 	$params = $_REQUEST;
	// 	$date_start = $params['date_start'];
	// 	$date_end = $params['date_end'];
	// 	$a = '';
	// 	if($date_start!='') $a.=" AND a.createdby BETWEEN '".date('Y-m-d',strtotime($date_start))."' AND '".date('Y-m-d',strtotime($date_end))."'";

	// 	$unit = $params['unit'];
	// 	if($unit!='All'){
	// 		$a.=" AND a.unit LIKE '".$unit."'";
	// 	}
	// 	$reason_update = $params['reason_update'];
	// 	if($reason_update!='All'){
	// 		$a.=" AND a.reason_update LIKE '".$reason_update."'";
	// 	}
		
	// 	$columns = array( 
	// 				0 =>'a.csdm',
	// 				1 =>'a.name', 
	// 				2 => 'a.unit',
	// 				3 => 'a.reason_update',
	// 				4 => 'a.notes',
	// 				5 => 'a.createdby'
	// 			);

	// 	$query=$this->ms_tb_history_update_sdm->get_history_severside(
	// 		$columns[$params['order'][0]['column']], 
	// 		$params['order'][0]['dir'], 
	// 		$params['start'], 
	// 		$params['length'],$a);
	// 	$total = $this->ms_tb_history_update_sdm->get_total_history_severside($a)->row();

	// 	$data['data']= array();
	// 	$i=0;
	// 	foreach ($query->result_array() as $myRow) {
	// 		$data['data'][$i] = $myRow;
	// 		$i++;
	// 	}

	// 	$json_data = array(
	// 		"draw"            => intval( $params['draw'] ),   
	// 		"recordsTotal"    => $total->total,  
	// 		"recordsFiltered" => $total->total,
	// 		"data"            => $data['data']   // total data array
	// 		);

	// 	echo json_encode($json_data);
	// }

	public function get_xls($date_start, $date_end, $unit, $reason_update)
	{
		$this->load->library("excel");
		$this->excel->load(APPPATH."../document/report_history.xlsx");
		$this->excel->setActiveSheetIndex(0);

		$unit = urldecode($unit);
		$reason_update = urldecode($reason_update);

		// $this->excel->getActiveSheet()->SetCellValue('A2', date('M-d-Y', $star).' to '. date('M-d-Y', $end));

		$a = '';
		if($date_start!='-'){
			$a.=" AND DATE_FORMAT(a.createdby, '%Y-%m-%d') BETWEEN '".$date_start."' AND '".$date_end."'";
		}
		if($unit!='All'){
			$a.=" AND a.unit LIKE '".$unit."'";
		}
		if($reason_update!='All'){
			$a.=" AND a.reason_update LIKE '".$reason_update."'";
		}

		$query=$this->ms_tb_history_update_sdm->get_history($a);

		$i=1;
		$row=5;
		foreach ($query->result_array() as $myRow) {
			$this->excel->getActiveSheet()->SetCellValue('A'.$row, $i);
			$this->excel->getActiveSheet()->SetCellValue('B'.$row, $myRow['csdm']);
			$this->excel->getActiveSheet()->SetCellValue('C'.$row, $myRow['name']);
			$this->excel->getActiveSheet()->SetCellValue('D'.$row, $myRow['tgl_awal_kontrak']);
			$this->excel->getActiveSheet()->SetCellValue('E'.$row, $myRow['tgl_akhir_kontrak']);
			$this->excel->getActiveSheet()->SetCellValue('F'.$row, $myRow['reason_update']);
			$this->excel->getActiveSheet()->SetCellValue('G'.$row, $myRow['notes']);
			$this->excel->getActiveSheet()->SetCellValue('H'.$row, $myRow['unit']);
			$this->excel->getActiveSheet()->SetCellValue('I'.$row, $myRow['jabatan']);
			$this->excel->getActiveSheet()->SetCellValue('J'.$row, $myRow['createdby']);
			$this->excel->getActiveSheet()->SetCellValue('K'.$row, $myRow['name_created']);

			$i++;
			$row++;
		}

		$this->excel->getActiveSheet()->getStyle('A5:K'.($row-1))->applyFromArray(
			array(
			'borders' => array(
					'allborders' => array(
		                'style' => PHPExcel_Style_Border::BORDER_THIN,
		                // 'color' => array('rgb' => 'DDDDDD')
		            )
				)
 			)
		);
		
		//die;
		$this->excel->stream("Report History.xlsx");
	}

}
