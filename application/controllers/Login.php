<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$data = array(
			'level' => '',
			'view' => '',
			'page' => '', 
			'menu' => '');
		$this->load->view('login', $data);
	}

	public function do_sign_in(){
		$this->load->model('ms_sdm');

		$data = array();
		$query = $this->ms_sdm->get_user($this->input->post('username'), $this->input->post('password'));
		// echo 'makan';
		$row = $query->row();
		if(isset($row)){
			if ($row->jabatan=='ADMIN SUPPORT' || $row->jabatan=='HR SUPPORT') {
				$newdata = array(
				        'id'  			=> $row->id,
				        'csdm'  		=> $row->csdm,
				        'name'     		=> $row->name,
				        'jabatan'		=> $row->jabatan,
				        'jabatan_level'	=> $row->jabatan_level,
				        'ctp'			=> $row->ctp,
				        'ceklogin'		=> 'user_hr',
				        'logged_in' 	=> TRUE
				);
				
				$this->session->set_userdata($newdata);
				$data['url'] = 'hr/content';
				$data['hasil'] = 'success';
			}
			else if (strpos($row->jabatan_level,'SPV') !== false) {
				$newdata = array(
				        'id'  			=> $row->id,
				        'csdm'  		=> $row->csdm,
				        'name'     		=> $row->name,
				        'jabatan'		=> $row->jabatan,
				        'jabatan_level'	=> $row->jabatan_level,
				        'unit'			=> $row->unit,
				        'ctp'			=> $row->ctp,
				        'ceklogin'		=> 'user_spv',
				        'logged_in' 	=> TRUE
				);
				
				$this->session->set_userdata($newdata);
				$data['url'] = 'spv/content';
				$data['hasil'] = 'success';
			}
			else if (strpos($row->jabatan_level,'TL') !== false) {
				$newdata = array(
				        'id'  			=> $row->id,
				        'csdm'  		=> $row->csdm,
				        'name'     		=> $row->name,
				        'jabatan'		=> $row->jabatan,
				        'jabatan_level'	=> $row->jabatan_level,
				        'unit'			=> $row->unit,
				        'ctp'			=> $row->ctp,
				        'ceklogin'		=> 'user_tl',
				        'logged_in' 	=> TRUE
				);
				
				$this->session->set_userdata($newdata);
				$data['url'] = 'tl/content';
				$data['hasil'] = 'success';
			}
			// else if ($row->csdm==$this->input->post('username') && $row->password!==$this->input->post('password')) {

			// 	$data['hasil'] = 'Username atau Password Anda Salah';
			// }
		}
		else{
			$data['hasil'] = 'Username atau Password Anda Salah';
		}

		
		echo json_encode($data);
	}

	public function do_log_out(){
		session_destroy();
		redirect('login');
	}
}
