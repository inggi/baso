<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ma_sdm extends CI_Model {
	
	var $db1;

	function __construct() {
        parent::__construct();
		$this->db1 = $this->load->database('absensi', TRUE);
    }

    

	function do_insert_sdm($data){
		$result2 = $this->db1->insert('sdm', $data);
		return array('success' => $result2, 
					'error' => $this->db1->error());
	}

	function do_update_sdm($data, $id){
		//$this->db1->set($data);
		$this->db1->where('id', $id);
		// $this->db1->update('tb_sdm', $data);
		$result2 = $this->db1->update('sdm', $data);
		// return $result;
		return array('success' => $result2, 
					'error' => $this->db1->error());

		// return array('affected_rows' => $this->db1->affected_rows(), 
		// 			'error' => $this->db1->error());
		//return $this->db1->insert_id();
	}
}
?>