<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ms_unit extends CI_Model {
	
	var $db1;

	function __construct() {
        parent::__construct();
		$this->db1 = $this->load->database('sdminfomedia', TRUE);
    }


	function get_unit(){
		$sql = "SELECT *
				FROM tb_unit a";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_unit_byusersession(){

		$sql = "SELECT *
				FROM tb_unit a WHERE ctp=?";
		$query = $this->db1->query($sql,array($this->session->ctp));
		return $query;
	}

	function get_ctp(){
		$sql = "SELECT *
				FROM tb_unit a GROUP BY ctp";
		$query = $this->db1->query($sql,array());
		return $query;
	}
	
}
?>