<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ms_jabatan extends CI_Model {
	
	var $db1;

	function __construct() {
        parent::__construct();
		$this->db1 = $this->load->database('sdminfomedia', TRUE);
    }


	function get_jabatan(){
		$sql = "SELECT *
				FROM tb_jabatan 
				WHERE ctp=? ORDER BY jabatan";
		$query = $this->db1->query($sql,array($this->session->ctp));
		return $query;
	}

	function do_insert_jabatan($data){
		$result = $this->db1->insert('tb_jabatan', $data);
		return array('success' => $result, 
					'error' => $this->db1->error());
	}

	function do_delete_jabatan($data){
		$result = $this->db1->delete('tb_jabatan', $data);
		return array('success' => $result, 
					'error' => $this->db1->error());
	}

}
?>