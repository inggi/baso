<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ms_document extends CI_Model {
	
	var $db1;

	function __construct() {
        parent::__construct();
		$this->db1 = $this->load->database('sdminfomedia', TRUE);
    }


	function insert_document($id_user='', $csdm='', $namefile='', $pathfile='', $namefile2='', $pathfile2='', $namefile3='', $pathfile3='', $notes='', $kategori_file){
		$sql = "INSERT INTO tb_document (id_user, csdm, namefile, pathfile, namefile2, pathfile2, namefile3, pathfile3, notes2, kategori_file, csdm_uploader) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		$query = $this->db1->query($sql,array($id_user, $csdm, $namefile, $pathfile, $namefile2, $pathfile2, $namefile3, $pathfile3, $notes, $kategori_file, $this->session->csdm));
		return $query;
	}

	function reupload_document($id_doc='', $namefile='', $pathfile='', $notes='', $kategori_file){
		$sql = "UPDATE tb_document SET notes2 = ?, namefile = ?, pathfile = ? , kategori_file = ? WHERE id = ? ";
		$query = $this->db1->query($sql,array($notes, $namefile, $pathfile, $kategori_file, $id_doc));
		return $query;
	}

	function update_note_document($id_doc='', $notes='', $kategori_file){
		$sql = "UPDATE tb_document SET notes2 = ?, kategori_file = ? WHERE id = ? ";
		$query = $this->db1->query($sql,array($notes, $kategori_file, $id_doc));
		return $query;
	}

	function update_status($id_doc=''){
		$sql = "UPDATE tb_document SET status = '1' WHERE id = ? ";
		$query = $this->db1->query($sql,array($id_doc));
		return $query;
	}

	function get_dokumenbycsdm($csdm){
		$sql = "SELECT a.*, b.name
				FROM tb_document a
				LEFT JOIN tb_sdm b ON b.csdm = a.csdm
				WHERE a.csdm = ?";
		$query = $this->db1->query($sql,array($csdm));
		return $query;
	}
}
?>