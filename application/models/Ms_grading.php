<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ms_grading extends CI_Model {
	
	var $db1;

	function __construct() {
        parent::__construct();
		$this->db1 = $this->load->database('sdminfomedia', TRUE);
    }


	function do_insert_grading($csdm, $grading, $qa_score, $ces, $fcr, $tnps, $propper, $aht, $held_call, $attendance, $effective_time, $uploader){
		$sql = "INSERT INTO grading (csdm,grading,qa_score,ces,fcr,tnps,propper,aht,held_call,attendance,effective_time,uploader) 
				VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
		$query = $this->db1->query($sql,array($csdm, $grading, $qa_score, $ces, $fcr, $tnps, $propper, $aht, $held_call, $attendance, $effective_time, $uploader));
		return $query;
	}
	
}
?>