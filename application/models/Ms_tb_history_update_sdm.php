<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ms_tb_history_update_sdm extends CI_Model {
	
	var $db1;

	function __construct() {
        parent::__construct();
		$this->db1 = $this->load->database('sdminfomedia', TRUE);
    }


	function do_insert($data){
		$result2 = $this->db1->insert('tb_history_update_sdm', $data);
		return array('success' => $result2, 
					'error' => $this->db1->error());
	}

	function get_history_severside($order='',$direction='',$start_limit='',$end_limit='',$search=''){
		$sqlrec =  " ORDER BY ". $order."   ".$direction."  LIMIT ".$start_limit." , ".$end_limit." ";

		$sql = "SELECT a.*
 				FROM tb_history_update_sdm a
				WHERE 1 ".$search.$sqlrec;
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_total_history_severside($search=''){
		$sql = "SELECT count(*) as total
				FROM tb_history_update_sdm a
				WHERE 1 ".$search;
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_history($search=''){

		$sql = "SELECT a.*, 
					b.name as name_created,
					COALESCE(c.name,'') as 'name_tl',
					COALESCE(d.name,'') as 'name_spv'
 				FROM tb_history_update_sdm a
 				LEFT JOIN tb_sdm b ON a.last_edited_by = b.csdm
 				LEFT JOIN tb_sdm c ON c.id = a.id_tl
				LEFT JOIN tb_sdm d ON d.id = a.id_spv
				WHERE 1 ".$search;
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_history_userspv($search='', $id_spv){

		$sql = "SELECT a.*, 
					b.name as name_created,
					COALESCE(c.name,'') as 'name_tl',
					COALESCE(d.name,'') as 'name_spv'
 				FROM tb_history_update_sdm a
 				LEFT JOIN tb_sdm b ON a.last_edited_by = b.csdm
 				LEFT JOIN tb_sdm c ON c.id = a.id_tl
				LEFT JOIN tb_sdm d ON d.id = a.id_spv
				WHERE a.id_spv=? ".$search;
		$query = $this->db1->query($sql,array($id_spv));
		return $query;
	}

	function get_history_usertl($search='', $id_tl){

		$sql = "SELECT a.*, 
					b.name as name_created,
					COALESCE(c.name,'') as 'name_tl',
					COALESCE(d.name,'') as 'name_spv'
 				FROM tb_history_update_sdm a
 				LEFT JOIN tb_sdm b ON a.last_edited_by = b.csdm
 				LEFT JOIN tb_sdm c ON c.id = a.id_tl
				LEFT JOIN tb_sdm d ON d.id = a.id_spv
				WHERE a.id_tl=? ".$search;
		$query = $this->db1->query($sql,array($id_tl));
		return $query;
	}
	
	
}
?>