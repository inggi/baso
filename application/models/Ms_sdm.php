<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ms_sdm extends CI_Model {
	
	var $db1;

	function __construct() {
        parent::__construct();
		$this->db1 = $this->load->database('sdminfomedia', TRUE);
    }

    function get_user($username='',$password=''){
		$sql = "SELECT *
				FROM tb_sdm a
				WHERE a.csdm = ? AND a.password = ?";
		$query = $this->db1->query($sql,array($username, $password));
		return $query;
		// return array('success' => $query, 
		// 			'error' => $this->db1->error());
	}
	
	function get_sdm($search=''){
		$sql = "SELECT a.*, 
					COALESCE(b.name,'') as 'name_tl',
					COALESCE(c.name,'') as 'name_spv'
 				FROM tb_sdm a 
				LEFT JOIN tb_sdm b ON b.id = a.id_tl
				LEFT JOIN tb_sdm c ON c.id = a.id_spv
				WHERE a.ctp IN ('ECARE','IBC','OBC','ITCC','OTHER')
				".$search;
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_sdm_byspv(){
		$sql = "SELECT a.*, 
					COALESCE(b.name,'') as 'name_tl',
					COALESCE(c.name,'') as 'name_spv',
					d.status, d.namefile, d.pathfile, d.namefile2, d.pathfile2, d.namefile3, d.pathfile3, d.notes, d.notes2, d.id as id_doc, d.kategori_file
 				FROM tb_sdm a 
				LEFT JOIN tb_sdm b ON b.id = a.id_tl AND b.status_sdm = 1
				LEFT JOIN tb_sdm c ON c.id = a.id_spv AND c.status_sdm = 1
				LEFT JOIN tb_document d ON d.csdm = a.csdm AND d.status=0
				WHERE a.status_sdm = 1
				AND a.id_spv = ? OR a.id = ? ORDER BY a.name";
		$query = $this->db1->query($sql,array($this->session->id,$this->session->id));
		return $query;
	}

	function get_sdm_bytl(){
		$sql = "SELECT a.*, 
					COALESCE(b.name,'') as 'name_tl',
					COALESCE(c.name,'') as 'name_spv',
					d.status, d.namefile, d.pathfile, d.namefile2, d.pathfile2, d.namefile3, d.pathfile3, d.notes, d.notes2, d.id as id_doc, d.kategori_file
 				FROM tb_sdm a 
				LEFT JOIN tb_sdm b ON b.id = a.id_tl AND b.status_sdm = 1
				LEFT JOIN tb_sdm c ON c.id = a.id_spv AND c.status_sdm = 1
				LEFT JOIN tb_document d ON d.csdm = a.csdm AND d.status=0
				WHERE a.status_sdm = 1
				AND a.id_tl = ? OR a.id = ? ORDER BY a.name";
		$query = $this->db1->query($sql,array($this->session->id,$this->session->id));
		return $query;
	}

	function get_sdm_byid($id){
		$sql = "SELECT a.*, 
					CURDATE() as update_los_today,
					COALESCE(b.name,'') as 'name_tl',
					COALESCE(c.name,'') as 'name_spv',
					(CASE 
						WHEN a.jabatan_level = 'ADMIN' THEN 'Admin'
						WHEN a.jabatan_level = 'ROSTER' THEN 'Roster'
						WHEN a.jabatan_level = 'SPV' THEN 'Supervisor'
						WHEN a.jabatan_level = 'TL' THEN 'Team Leader'
						WHEN a.jabatan_level = 'STAFF' THEN 'Staff'
						ELSE a.jabatan_level END)
					AS 'jabatan_level2',
					d.status, d.namefile, d.pathfile, d.namefile2, d.pathfile2, d.namefile3, d.pathfile3, d.notes, d.notes2, d.id as id_doc, d.csdm_uploader, d.kategori_file,
					d.create_by as doc_created,
					COALESCE(e.name,'') AS 'name_uploader',
					(CASE WHEN a.tgl_lahir != 0000-00-00 THEN (YEAR(CURDATE()) - YEAR(a.tgl_lahir)) ELSE 0 END) as usia,
					(CASE WHEN a.status_sdm = 1 THEN 'AKTIF' 
						  WHEN a.status_sdm = 0 THEN 'NON AKTIF'
						  ELSE a.status_sdm END) 
					as status_sdm2,
					(DATEDIFF(curdate(), a.join_insani))/30 as los,
					(CASE WHEN (DATEDIFF(curdate(), a.join_insani)) <= 90 THEN 'A' 
						WHEN (DATEDIFF(curdate(), a.join_insani)) <= 180 THEN 'B'
						WHEN (DATEDIFF(curdate(), a.join_insani)) <= 356 THEN 'C'
						WHEN (DATEDIFF(curdate(), a.join_insani)) <= 712 THEN 'D'
						ELSE 'E' END)
					as kode_los,
					(DATEDIFF(curdate(), a.join_unit))/30 as los_unit
 				FROM tb_sdm a 
				LEFT JOIN tb_sdm b ON b.id = a.id_tl
				LEFT JOIN tb_sdm c ON c.id = a.id_spv
				LEFT JOIN tb_document d ON d.csdm = a.csdm AND d.status=0
				LEFT JOIN (SELECT a.name,e.csdm_uploader FROM tb_sdm a  LEFT JOIN tb_document e ON e.csdm_uploader = a.csdm) e ON e.csdm_uploader = a.csdm
				WHERE a.id = ?";
		$query = $this->db1->query($sql,array($id));
		return $query;
	}

	function get_tl(){
		$sql = "SELECT a.*
 				FROM tb_sdm a
				WHERE a.status_sdm = 1 AND a.jabatan_level = 'TL' ORDER BY a.name";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_tl_userspv($id){
		$sql = "SELECT a.*
 				FROM tb_sdm a
				WHERE a.status_sdm = 1 AND a.jabatan_level = 'TL' AND a.id_spv=? ORDER BY a.name";
		$query = $this->db1->query($sql,array($id));
		return $query;
	}

	function get_home_totalkaryawanbyjabatan(){
		$sql = "SELECT count(a.id) as total , jabatan
				FROM tb_sdm a
				WHERE a.status_sdm = 1 and ctp=? GROUP BY a.jabatan ORDER BY jabatan";
		$query = $this->db1->query($sql,array($this->session->ctp));
		return $query;
	}

	function get_home_totalkaryawan_userspv($id){
		$sql = "SELECT count(a.id_tl) as total, a.id_tl, b.id, b.status_sdm, b.jabatan_level,
					COALESCE(b.name,'') as 'name_tl',
					COALESCE(c.name,'') as 'name_spv'
				FROM tb_sdm a
				LEFT JOIN tb_sdm b ON b.id = a.id_tl
				LEFT JOIN tb_sdm c ON c.id = a.id_spv
				WHERE a.status_sdm = 1 AND a.id_spv=? AND a.id_tl is not null GROUP BY a.id_tl ORDER BY name_tl";
		$query = $this->db1->query($sql,array($id));
		return $query;
	}

	function get_spv(){
		$sql = "SELECT a.*,
					COALESCE(b.name,'') as 'name_tl',
					COALESCE(c.name,'') as 'name_spv'
 				FROM tb_sdm a
 				LEFT JOIN tb_sdm b ON b.id = a.id_tl
				LEFT JOIN tb_sdm c ON c.id = a.id_spv
				WHERE a.status_sdm = 1 AND a.jabatan_level = 'SPV' ORDER BY a.name";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_jabatan(){
		$sql = "SELECT *
				FROM tb_sdm a GROUP BY jabatan";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_selectspvbytl($id_tl){
		$sql = "SELECT * FROM tb_sdm a WHERE id=?";
		$query = $this->db1->query($sql,array($id_tl));
		return $query;
	}

	function get_total_home_userhr(){
		$sql = "SELECT 
					(CASE WHEN a.end_kontrak > 0 THEN a.end_kontrak ELSE 0 END) as end_kontrak, 
					(CASE WHEN b.end_kontrak60 > 0 THEN b.end_kontrak60 ELSE 0 END) as end_kontrak60, 
					(CASE WHEN c.outofkontrak > 0 THEN c.outofkontrak ELSE 0 END) as outofkontrak, 
					(CASE WHEN d.outofdateeit > 0 THEN d.outofdateeit ELSE 0 END) as outofdateeit, 
					(CASE WHEN e.rekomendasi_tlspv > 0 THEN e.rekomendasi_tlspv ELSE 0 END) as rekomendasi_tlspv, 
					(CASE WHEN f.onthespot > 0 THEN f.onthespot ELSE 0 END) as onthespot,
					(CASE WHEN g.onemonthnotice > 0 THEN g.onemonthnotice ELSE 0 END) as onemonthnotice,
					(CASE WHEN h.cuti_m > 0 THEN h.cuti_m ELSE 0 END) as cuti_m,
					(CASE WHEN i.aproval_resign > 0 THEN i.aproval_resign ELSE 0 END) as aproval_resign,
					(CASE WHEN j.total_karyawan > 0 THEN j.total_karyawan ELSE 0 END) as total_karyawan
				FROM
				(SELECT COUNT(*) as end_kontrak FROM tb_sdm a 
					WHERE a.tgl_akhir_kontrak BETWEEN CURDATE() AND CURDATE() + INTERVAL 30 DAY AND a.status_sdm = 1) a,
				(SELECT COUNT(*) as end_kontrak60 FROM tb_sdm a 
					WHERE a.tgl_akhir_kontrak BETWEEN CURDATE() AND CURDATE() + INTERVAL 60 DAY 
						AND a.status_sdm = 1 AND a.ctp=?) b,
				(SELECT COUNT(*) as outofkontrak FROM tb_sdm a 
					WHERE a.tgl_akhir_kontrak < CURDATE() AND a.ctp=? AND a.status_sdm = 1) c,
				(SELECT COUNT(*) as outofdateeit FROM tb_sdm a 
					WHERE a.tgl_akhir_kontrak < CURDATE() AND a.ctp='ITCC' AND a.status_sdm = 1) d,
				(SELECT COUNT(*) as rekomendasi_tlspv FROM tb_sdm a 
						LEFT JOIN tb_document b ON b.csdm = a.csdm
						WHERE b.status=0 and b.kategori_file='CONTRACT' AND a.ctp=?) e,
				(SELECT COUNT(*) as onthespot FROM tb_sdm a 
						LEFT JOIN tb_document b ON b.csdm = a.csdm
						WHERE b.status=0 and b.kategori_file='RESIGN ON THE SPOT' AND a.ctp=?) f,
				(SELECT COUNT(*) as onemonthnotice FROM tb_sdm a 
						LEFT JOIN tb_document b ON b.csdm = a.csdm
						WHERE b.status=0 and b.kategori_file='RESIGN ONE MONTH NOTICE' AND a.ctp=?) g,
				(SELECT COUNT(*) as cuti_m FROM tb_sdm a 
						LEFT JOIN tb_document b ON b.csdm = a.csdm
						WHERE b.status=0 and b.kategori_file='CUTI MELAHIRKAN' AND a.ctp=?) h,
				(SELECT COUNT(*) as aproval_resign FROM tb_sdm a 
						LEFT JOIN tb_document b ON b.csdm = a.csdm
						WHERE b.status=0 and (b.kategori_file='RESIGN ONE MONTH NOTICE' OR b.kategori_file='RESIGN ON THE SPOT') ) i,
				(SELECT COUNT(*) as total_karyawan FROM tb_sdm a 
						WHERE a.ctp = ? AND a.status_sdm=1) j
				";
		$query = $this->db1->query($sql,array($this->session->ctp,$this->session->ctp,$this->session->ctp,$this->session->ctp,$this->session->ctp,$this->session->ctp,$this->session->ctp));
		return $query;
	}

	function get_total_home_userhr2(){
		$sql = "SELECT a.*, 
					CURDATE() as update_los_today,
					COALESCE(b.name,'') as 'name_tl',
					COALESCE(c.name,'') as 'name_spv',
					(CASE WHEN a.tgl_lahir != 0000-00-00 THEN (YEAR(CURDATE()) - YEAR(a.tgl_lahir)) ELSE 0 END) as usia,
					-- (CASE WHEN a.status_sdm = 1 THEN 'AKTIF' 
					-- 	  WHEN a.status_sdm = 0 THEN 'NON AKTIF'
					-- 	  ELSE a.status_sdm END) 
					-- as status_sdm2,
					(DATEDIFF(curdate(), a.join_insani))/30 as los_rumus,
					(CASE WHEN (DATEDIFF(curdate(), a.join_insani)) <= 90 THEN 'A' 
						WHEN (DATEDIFF(curdate(), a.join_insani)) <= 180 THEN 'B'
						WHEN (DATEDIFF(curdate(), a.join_insani)) <= 356 THEN 'C'
						WHEN (DATEDIFF(curdate(), a.join_insani)) <= 712 THEN 'D'
						ELSE 'E' END)
					as kode_los_rumus,
					(DATEDIFF(curdate(), a.join_unit))/30 as los_unit
 				FROM tb_sdm a 
				LEFT JOIN tb_sdm b ON b.id = a.id_tl
				LEFT JOIN tb_sdm c ON c.id = a.id_spv
				WHERE a.ctp = ?
				AND a.status_sdm=1
				";
		$query = $this->db1->query($sql,array($this->session->ctp));
		return $query;
	}

	function get_total_home_userspv(){
		$sql = "SELECT 
					(CASE WHEN a.end_kontrak > 0 THEN a.end_kontrak ELSE 0 END) as end_kontrak, 
					(CASE WHEN g.outofdate > 0 THEN g.outofdate ELSE 0 END) as outofdate, 
					(CASE WHEN b.perpanjang_kontrak > 0 THEN b.perpanjang_kontrak ELSE 0 END) as perpanjang_kontrak, 
					(CASE WHEN c.waiting_aproval_hr > 0 THEN c.waiting_aproval_hr ELSE 0 END) as waiting_aproval_hr,
					(CASE WHEN d.onthespot > 0 THEN d.onthespot ELSE 0 END) as onthespot,
					(CASE WHEN e.onemonthnotice > 0 THEN e.onemonthnotice ELSE 0 END) as onemonthnotice,
					(CASE WHEN f.aproval_resign > 0 THEN f.aproval_resign ELSE 0 END) as aproval_resign,
					(CASE WHEN h.total_underteam > 0 THEN h.total_underteam ELSE 0 END) as total_underteam
				FROM
				(SELECT COUNT(*) as end_kontrak FROM tb_sdm a 
						WHERE a.tgl_akhir_kontrak BETWEEN CURDATE() AND CURDATE() + INTERVAL 60 DAY 
						and (a.id_spv = ? OR a.id = ?) AND a.status_sdm = 1) a,
				(SELECT COUNT(*) as outofdate FROM tb_sdm a 
						WHERE a.tgl_akhir_kontrak < CURDATE() AND a.id_spv = ? AND a.status_sdm = 1) g,
				(SELECT count(*) as perpanjang_kontrak FROM tb_history_update_sdm a
						WHERE a.id_spv=? and reason_update='PERPANJANG KONTRAK') b,
				(SELECT COUNT(*) as waiting_aproval_hr FROM tb_sdm a 
						LEFT JOIN tb_document b ON b.csdm = a.csdm
						WHERE b.status=0 and b.kategori_file='CONTRACT' and a.id_spv = ?) c,
				(SELECT COUNT(*) as onthespot FROM tb_sdm a 
						LEFT JOIN tb_document b ON b.csdm = a.csdm
						WHERE b.status=0 and b.kategori_file='RESIGN ON THE SPOT' and a.id_spv = ?) d,
				(SELECT COUNT(*) as onemonthnotice FROM tb_sdm a 
						LEFT JOIN tb_document b ON b.csdm = a.csdm
						WHERE b.status=0 and b.kategori_file='RESIGN ONE MONTH NOTICE' and a.id_spv = ?) e,
				(SELECT COUNT(*) as aproval_resign FROM tb_sdm a 
						LEFT JOIN tb_document b ON b.csdm = a.csdm
						WHERE b.status=0 and (b.kategori_file='RESIGN ONE MONTH NOTICE' OR b.kategori_file='RESIGN ON THE SPOT') and a.id_spv = ?) f,
				(SELECT COUNT(*) as total_underteam FROM tb_sdm a 
						WHERE a.id_spv = ? AND a.status_sdm=1) h
				";
		$query = $this->db1->query($sql,array($this->session->id,$this->session->id,$this->session->id,$this->session->id,$this->session->id,$this->session->id,$this->session->id,$this->session->id,$this->session->id));
		return $query;
	}

	function get_total_home_usertl(){
		$sql = "SELECT 
					(CASE WHEN a.end_kontrak > 0 THEN a.end_kontrak ELSE 0 END) as end_kontrak, 
					(CASE WHEN h.outofdate > 0 THEN h.outofdate ELSE 0 END) as outofdate, 
					(CASE WHEN b.perpanjang_kontrak > 0 THEN b.perpanjang_kontrak ELSE 0 END) as perpanjang_kontrak, 
					(CASE WHEN c.waiting_aproval_hr > 0 THEN c.waiting_aproval_hr ELSE 0 END) as waiting_aproval_hr,
					(CASE WHEN d.onthespot > 0 THEN d.onthespot ELSE 0 END) as onthespot,
					(CASE WHEN e.onemonthnotice > 0 THEN e.onemonthnotice ELSE 0 END) as onemonthnotice,
					(CASE WHEN f.aproval_resign > 0 THEN f.aproval_resign ELSE 0 END) as aproval_resign,
					(CASE WHEN g.total_underteam > 0 THEN g.total_underteam ELSE 0 END) as total_underteam
				FROM
				(SELECT COUNT(*) as end_kontrak FROM tb_sdm a 
					WHERE a.tgl_akhir_kontrak BETWEEN CURDATE() AND CURDATE() + INTERVAL 60 DAY 
					and (a.id_tl = ? OR a.id = ?) AND a.status_sdm = 1) a,
				(SELECT COUNT(*) as outofdate FROM tb_sdm a 
						WHERE a.tgl_akhir_kontrak < CURDATE() AND a.id_tl = ? AND a.status_sdm = 1) h,
				(SELECT count(*) as perpanjang_kontrak FROM tb_history_update_sdm a
						WHERE a.id_tl=? and reason_update='PERPANJANG KONTRAK') b,
				(SELECT COUNT(*) as waiting_aproval_hr FROM tb_sdm a 
						LEFT JOIN tb_document b ON b.csdm = a.csdm
						WHERE b.status=0 and b.kategori_file='CONTRACT' and a.id_tl = ?) c,
				(SELECT COUNT(*) as onthespot FROM tb_sdm a 
						LEFT JOIN tb_document b ON b.csdm = a.csdm
						WHERE b.status=0 and b.kategori_file='RESIGN ON THE SPOT' and a.id_tl = ?) d,
				(SELECT COUNT(*) as onemonthnotice FROM tb_sdm a 
						LEFT JOIN tb_document b ON b.csdm = a.csdm
						WHERE b.status=0 and b.kategori_file='RESIGN ONE MONTH NOTICE' and a.id_tl = ?) e,
				(SELECT COUNT(*) as aproval_resign FROM tb_sdm a 
						LEFT JOIN tb_document b ON b.csdm = a.csdm
						WHERE b.status=0 and (b.kategori_file='RESIGN ONE MONTH NOTICE' OR b.kategori_file='RESIGN ON THE SPOT') 
						and a.id_tl = ?) f,
				(SELECT COUNT(*) as total_underteam FROM tb_sdm a 
						WHERE a.id_tl = ? AND a.status_sdm=1) g
				";
		$query = $this->db1->query($sql,array($this->session->id,$this->session->id,$this->session->id,$this->session->id,$this->session->id,$this->session->id,$this->session->id,$this->session->id,$this->session->id));
		return $query;
	}

	function get_endcontract_userhr(){
		$sql = "SELECT a.id, a.csdm, a.name, a.jabatan, a.tgl_awal_kontrak, a.tgl_akhir_kontrak,
					COALESCE(c.grading, '-') as 'grading',
					COALESCE(c.created, '-') as 'grade_upload'
				FROM tb_sdm a 
				LEFT JOIN grading c ON c.csdm = a.csdm 
					AND c.no = (SELECT MAX(no) FROM grading d WHERE d.csdm = a.csdm)
				WHERE a.tgl_akhir_kontrak BETWEEN CURDATE() AND CURDATE() + INTERVAL 60 DAY 
					AND a.status_sdm = 1 AND a.ctp=?
					ORDER BY tgl_akhir_kontrak
				";
		$query = $this->db1->query($sql,array($this->session->ctp));
		return $query;
	}

	function get_endcontract_userspv(){
		$sql = "SELECT a.*, b.status, b.namefile, b.pathfile, b.namefile2, b.pathfile2, b.namefile3, b.pathfile3, b.notes, b.notes2, b.id as id_doc, b.kategori_file FROM tb_sdm a 
				LEFT JOIN tb_document b ON b.csdm = a.csdm AND b.status=0
				WHERE a.tgl_akhir_kontrak BETWEEN CURDATE() AND CURDATE() + INTERVAL 60 DAY 
				and (a.id_spv = ? OR a.id = ?) AND a.status_sdm = 1
				";
		$query = $this->db1->query($sql,array($this->session->id,$this->session->id));
		return $query;
	}

	function get_endcontract_usertl(){
		$sql = "SELECT a.*, b.status, b.namefile, b.pathfile, b.namefile2, b.pathfile2, b.namefile3, b.pathfile3, b.notes, b.notes2, b.id as id_doc, b.kategori_file FROM tb_sdm a 
				LEFT JOIN tb_document b ON b.csdm = a.csdm AND b.status=0
				WHERE a.tgl_akhir_kontrak BETWEEN CURDATE() AND CURDATE() + INTERVAL 60 DAY 
				and (a.id_tl = ? OR a.id = ?) AND a.status_sdm = 1
				";
		$query = $this->db1->query($sql,array($this->session->id,$this->session->id));
		return $query;
	}

	function get_outofcontract_userhr(){
		$sql = "SELECT a.id, a.csdm, a.name, a.jabatan, a.tgl_awal_kontrak, a.tgl_akhir_kontrak,
					COALESCE(c.grading, '-') as 'grading',
					COALESCE(c.created, '-') as 'grade_upload'
				FROM tb_sdm a 
				LEFT JOIN grading c ON c.csdm = a.csdm 
					AND c.no = (SELECT MAX(no) FROM grading d WHERE d.csdm = a.csdm)
				WHERE a.tgl_akhir_kontrak < CURDATE() AND a.ctp=? AND a.status_sdm = 1
					ORDER BY tgl_akhir_kontrak
				";
		$query = $this->db1->query($sql,array($this->session->ctp));
		return $query;
	}

	function get_outofcontract_userhr_old(){
		$sql = "SELECT a.*, b.status, b.namefile, b.pathfile, b.notes, b.notes2, b.id as id_doc FROM tb_sdm a
				LEFT JOIN tb_document b ON b.csdm = a.csdm AND b.status=0
				WHERE a.tgl_akhir_kontrak < CURDATE() AND a.ctp=? AND a.status_sdm = 1
				";
		$query = $this->db1->query($sql,array($this->session->ctp));
		return $query;
	}

	function get_outofcontract_userspv(){
		$sql = "SELECT a.*, b.status, b.namefile, b.pathfile, b.namefile2, b.pathfile2, b.namefile3, b.pathfile3, b.notes, b.notes2, b.id as id_doc, b.kategori_file FROM tb_sdm a
				LEFT JOIN tb_document b ON b.csdm = a.csdm AND b.status=0
				WHERE a.tgl_akhir_kontrak < CURDATE() and (a.id_spv = ? OR a.id = ?) AND a.status_sdm = 1
				";
		$query = $this->db1->query($sql,array($this->session->id,$this->session->id));
		return $query;
	}

	function get_outofcontract_usertl(){
		$sql = "SELECT a.*, b.status, b.namefile, b.pathfile, b.namefile2, b.pathfile2, b.namefile3, b.pathfile3, b.notes, b.notes2, b.id as id_doc, b.kategori_file FROM tb_sdm a
				LEFT JOIN tb_document b ON b.csdm = a.csdm AND b.status=0
				WHERE a.tgl_akhir_kontrak < CURDATE() and (a.id_tl = ? OR a.id = ?) AND a.status_sdm = 1
				";
		$query = $this->db1->query($sql,array($this->session->id,$this->session->id));
		return $query;
	}

	function get_rekomendasi_spvtl(){
		$sql = "SELECT a.*, d.status, d.namefile, d.pathfile, d.notes, d.notes2, d.id as id_doc, d.kategori_file,
					COALESCE(b.name,'') as 'name_tl',
					COALESCE(c.name,'') as 'name_spv'
 				FROM tb_sdm a 
				LEFT JOIN tb_sdm b ON b.id = a.id_tl AND b.status_sdm = 1
				LEFT JOIN tb_sdm c ON c.id = a.id_spv AND c.status_sdm = 1
				LEFT JOIN tb_document d ON d.csdm = a.csdm AND d.status=0
				WHERE d.status=0 
				";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_rekomendasi_spvtl2($kategori_file){
		$sql = "SELECT a.*, d.status, d.namefile, d.pathfile, d.namefile2, d.pathfile2, d.namefile3, d.pathfile3, d.notes, d.notes2, d.id as id_doc, d.csdm_uploader, d.create_by as doc_created, d.kategori_file,
					COALESCE(b.name,'') as 'name_tl',
					COALESCE(c.name,'') as 'name_spv'
 				FROM tb_sdm a 
				LEFT JOIN tb_sdm b ON b.id = a.id_tl AND b.status_sdm = 1
				LEFT JOIN tb_sdm c ON c.id = a.id_spv AND c.status_sdm = 1
				LEFT JOIN tb_document d ON d.csdm = a.csdm AND d.status=0
				WHERE d.status=0 AND d.kategori_file=? AND a.ctp=?
				";
		$query = $this->db1->query($sql,array($kategori_file,$this->session->ctp));
		return $query;
	}

	function get_endcontract_userspv_old($order='',$direction='',$start_limit='',$end_limit=''){
		$sqlrec =  " ORDER BY ". $order."   ".$direction."  LIMIT ".$start_limit." , ".$end_limit." ";

		$sql = "SELECT a.*, b.status, b.namefile, b.pathfile, b.notes, b.id as id_doc FROM tb_sdm a 
				LEFT JOIN tb_document b ON b.csdm = a.csdm AND b.status=0
				WHERE a.tgl_akhir_kontrak BETWEEN CURDATE() AND CURDATE() + INTERVAL 30 DAY 
				and a.id_spv = ?".$sqlrec;
		$query = $this->db1->query($sql,array($this->session->id));
		return $query;
	}

	function get_total_endcontract_userspv_severside(){
		$sql = "SELECT count(*) as total FROM tb_sdm a 
				LEFT JOIN tb_document b ON b.csdm = a.csdm AND b.status=0
				WHERE a.tgl_akhir_kontrak BETWEEN CURDATE() AND CURDATE() + INTERVAL 30 DAY 
				and a.id_spv = ?";
		$query = $this->db1->query($sql,array($this->session->id));
		return $query;
	}

	function do_update_tl_spv_ctp(){
		$sql = "UPDATE tb_sdm a
				LEFT JOIN tb_sdm b ON b.name = a.tl AND b.status_sdm = 1
				LEFT JOIN tb_sdm c ON c.name = a.spv AND c.status_sdm = 1
				SET a.id_tl = b.id, a.id_spv = c.id
				WHERE a.status_sdm = 1 
					AND ((a.id_tl IS NULL AND a.tl IS NOT NULL) OR (a.id_spv IS NULL AND a.spv IS NOT NULL))";
		$this->db1->query($sql,array());
		return array('affected_rows' => $this->db1->affected_rows(), 
					'error' => $this->db1->error());
	}

	function do_insert_sdm_bacth($array){
		$this->db1->insert_batch('tb_sdm', $array);
		return array('affected_rows' => $this->db1->affected_rows(), 
					'error' => $this->db1->error());
	}

	function do_insert_sdm($data){
		$result = $this->db1->insert('tb_sdm', $data);
		// return $result;
		return array('success' => $result, 
					'error' => $this->db1->error());
	}

	function do_update_sdm_percolumn($id, $column, $value){
		$this->db1->set($column, $value);
		$this->db1->where('id', $id);
		$this->db1->update('tb_sdm');

		return array('affected_rows' => $this->db1->affected_rows(), 
					'error' => $this->db1->error());
	}

	function do_update_sdm($data, $id){
		//$this->db1->set($data);
		$this->db1->where('id', $id);
		// $this->db1->update('tb_sdm', $data);
		$result = $this->db1->update('tb_sdm', $data);
		// return $result;
		return array('success' => $result, 
					'error' => $this->db1->error());

		// return array('affected_rows' => $this->db1->affected_rows(), 
		// 			'error' => $this->db1->error());
		//return $this->db1->insert_id();
	}

	function do_update_password($id='',$password=''){
		$sql = "UPDATE tb_sdm a
				SET a.password = ?
				WHERE a.id = ? ";
		$query = $this->db1->query($sql,array($password, $id));
		return $query;
	}
}
?>